main(List<String> args) {
  final liste = [1, 2, 3];
  final cp = liste;
  cp[0] = 0;
  print(liste != cp);

  var cp1 = [...liste];
  cp1[0] = 42;
  print(liste != cp1);

  var cp2 = [
    for (var item in liste) item,
  ];
  cp2[0] = 1234;
  print(liste != cp2);

  final liste2 = [
    1,
    2,
    [3, 4],
  ];
  print(liste2);

  var cp4 = [...liste2];
  (cp4[2] as List)[1] = 42;
  print(liste2);

  var cp5 = List.from(liste2);
  (cp5[2] as List)[1] = 42;
  print(liste2 == cp5);
  print(cp5);
}
