main(List<String> args) {
  //initialisation
  // Les trois façons sont toutes de type Set<String>
  Set<String> pays = {"France", "USA", "Inde", "Japon"};
  //var pays = <String>{"France", "USA", "Inde", "Japon"};
  //var pays = {"France", "USA", "Inde", "Japon"};

  // accès
  var premier = pays.elementAt(0);
  print("Accès au premier élément du set : $premier.");

  // ajout d'une valeur
  if (pays.add("Islande")) {
    print("Islande a été rajouté sans problème");
  } else {
    print("ce message ne sera jamais affiché");
  }

  // ajout d'un doublon
  if (pays.add("France")) {
    print("ce message ne sera jamais affiché");
  } else {
    print("Erreur : la valeur 'France' est déjà présente.");
  }

  // suppression d'une donnée
  if (pays.remove("Inde")) {
    print("Inde a été supprimée du Set.");
  }
  print(pays);

  // autres méthodes utiles sur les sets
  print("Le set a pour premier élément : ${pays.first}.");
  print("Le set a pour dernier élément : ${pays.last}.");
  print("Le set a une longueur de ${pays.length} éléments.");
  print("Le set contient-il le pays Italie ? ${pays.contains("Italie")}.");

  // opérations
  //// union
  var pays2 = {"USA", "Italie", "Norvège"};
  var unionPays = pays.union(pays2);
  print("Union = $unionPays");

  //// intersection
  unionPays = pays.intersection(pays2);
  print("Intersection = $unionPays");

  //// différence
  unionPays = pays.difference(pays2);
  print("Différence = $unionPays");
}
