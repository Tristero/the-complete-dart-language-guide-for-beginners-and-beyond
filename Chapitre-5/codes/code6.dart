main(List<String> args) {
  final couleurs = ["rouge", "vert"];
  const ajouteBleu = true;
  const ajouteMarron = false;

  if (ajouteBleu) {
    couleurs.add("bleu");
  }

  if (ajouteMarron) {
    couleurs.add("marron");
  }

  final couleurs2 = [
    "rouge",
    "vert",
    // if (ajouteBleu) {"bleu"},
    if (ajouteBleu) "bleu",
    //if (ajouteMarron) {"marron"},
    if (ajouteMarron) "marron",
  ];

  assert(couleurs == couleurs2);
}
