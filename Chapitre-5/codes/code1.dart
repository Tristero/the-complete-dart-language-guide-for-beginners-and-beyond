main(List<String> args) {
  var villes = ["Paris", "Londres", "Bordeaux", "Rome"];

  // pour afficher l'ensemble de la liste
  print(villes); // affichera [Paris, Londres, Bordeaux, Rome]

  // pour accéder à une valeur du tableau
  print(villes[0]);

  // affectation d'une nouvelle valeur
  villes[2] = "Toulouse";
  print(villes);

  // boucle FOR pour itérer simplement sur une liste
  for (var ville in villes) {
    print(ville);
  }

  // boucle FOR avec l'écriture traditionnelle
  for (var i = 0; i < villes.length; i++) {
    print(villes[i]);
  }
}
