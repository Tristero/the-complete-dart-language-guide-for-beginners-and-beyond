main(List<String> args) {
  const a = {1, 3};
  const b = {3, 5};

  // doit afficher {1, 5}
  var c = a.union(b);
  c.remove(3);
  // alternative plus complexe et illisible
  final d = a.union(b).difference(a.intersection(b));

  print(c);
  print(d);

  //calcul de la somme
  var somme = 0;
  for (var elem in d) {
    somme += elem;
  }
  print(somme);

  somme = 0;
  d.forEach((element) {
    somme += element;
  });
  print(somme);

}
