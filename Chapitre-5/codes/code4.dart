main(List<String> args) {
  // Le plus simple :

  // var id = {
  //   "nom": "Doe",
  //   "prenom": "John",
  //   "age": 42,
  //   "taille": 1.80,
  // };

  // Instance de Map :
  // var id = Map();
  // id["nom"] = "Doe";
  // id["prenom"] = "John";
  // id["age"] = 42;
  // id["taille"] = 1.80;

  // Le typage
  //// Première possibilité :
  Map<String, dynamic> id = {
    "nom": "Doe",
    "prenom": "John",
    "age": 42,
    "taille": 1.80,
  };

  print("Affichage Map : $id");

  print("Accès à une valeur : le nom est ${id["nom"]}");

  print("Affichage de toutes les valeurs : ${id.values}");

  print("Affichage de toutes les clés : ${id.keys}");

  // accès à une valeur inexistante
  var inexistante = id['truc'];
  print("Valeur inexistante = ${inexistante}");
  print(id);

  // modification d'une valeur
  id['age'] = 24;
  print("'age' a été modifié : ${id["age"]}");

  // type à l'extraction
  // var prenom = id['prenom'];
  // print(prenom.runtimeType.toString());
  // le type n'est pas déterminé à l'exécution donc la solution est de caster :
  var prenom = id['prenom'] as String;
  print(prenom);

  // Boucle for-in
  //// accès aux clés
  for (var cle in id.keys) {
    print("clé = $cle");
  }

  //// accès aux valeurs
  for (var valeur in id.values) {
    print("valeur = $valeur");
  }

  //// MapEntry
  for (MapEntry me in id.entries) {
    print("clé (${me.key}) = valeur (${me.value})");
  }

  // Utilisation du forEach()
  id.entries.forEach((e) => print("clé (${e.key}) = valeur (${e.value})"));

  id.keys.forEach((element) {
    var c = element;
    var v = id[element];
    print("clé ($c) = valeur ($v)");
  });

  id.values.forEach((element) {
    print("Valeur = $element");
  });
}
