main(List<String> args) {
  const nouvellesCouleurs = ["jaune", "violet"];

  final couleurs = [
    "rouge",
    "vert",
    for (var couleur in nouvellesCouleurs) couleur,
    "indigo",
  ];

  final couleurs2 = [
    "rouge",
    "vert",
    "indigo",
  ];
  couleurs2.addAll(nouvellesCouleurs);

  print("Résultat avec collection-for :\n$couleurs");
  print("\nRésultat avec addAll() :\n$couleurs2");
}
