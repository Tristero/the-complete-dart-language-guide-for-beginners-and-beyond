main(List<String> args) {
  const pizzaPrices = {
    'margherita': 5.5,
    'pepperoni': 7.5,
    'vegetarian': 6.5,
  };

  // affichera "Total: 13€"
  // si une pizza n'est pas dans le menu alors le prog doit afficher "Cette XXX n'est pas au menu."

  const order = ["margherita", "pepperoni", ""];
  double total = 0.0;
  for (String pizza in order) {
    final price = pizzaPrices[pizza];
    if (price == null) {
      print("Cette pizza ($pizza) n'est pas au menu.");
      continue;
    }
    total += price;
  }
  print("Total : $total€");
}
