main(List<String> args) {
  var lst = [1, 3, 5, 7, 9];

  // affiche la somme de toutes le valeurs
  int somme = 0;
  for (int val in lst) {
    somme += val;
  }
  print("La somme de $lst est égale à $somme.");
}
