main(List<String> args) {
  const nouvellesCouleurs = ["jaune", "violet"];

  final couleurs = [
    "rouge",
    "vert",
    for (var couleur in nouvellesCouleurs) couleur,
  ];

  // alternative :
  //couleurs.addAll(nouvellesCouleurs);
  print(couleurs);
}
