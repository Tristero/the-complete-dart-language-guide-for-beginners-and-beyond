main(List<String> args) {
  var restaurants = [
    {
      "nom": "Chez Mario",
      "cuisine": "italienne",
      "notes": <double>[5.0, 3.5, 4.5],
    },
    {
      "nom": "A la bonne franquette",
      "cuisine": "française",
      "notes": [5.0, 4.5, 4.0]
    },
    {
      "nom": "New Delhi",
      "cuisine": "indienne",
      "notes": [4.0, 4.5, 4.0],
    },
  ];

// calcul de la note moyenne par resto
// rajouter le résultat à la map
  for (var m in restaurants) {
    var total = 0.0;

    var notes = (m["notes"]) as List<double>;
    for (double note in notes) {
      total += note;
    }
    m["moyNotes"] = (total / notes.length);
  }
  print(restaurants);
}
