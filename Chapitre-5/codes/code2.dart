main(List<String> args) {
  var villes = ["Paris", "Londres", "Bordeaux", "Rome"];

  print("La liste est-elle vide ? ${villes.isEmpty}");

  print("La liste est-elle remplie ? ${villes.isNotEmpty}");

  print("Dernier élément de la liste = ${villes.last}");

  print("Premier élément de la liste = ${villes.first}");

  villes.add("Tokyo");
  print(villes);

  villes.insert(2, "Berlin");
  print(villes);

  var villeRetiree = villes.removeAt(2);
  print("Ville retirée : $villeRetiree");
  print(villes);

  print(
      "Londres est-elle présente dans la liste ? ${villes.contains('Londres')}");
  print("Lima est-elle présente dans la liste ? ${villes.contains('Lima')}");

  print("L'index de Bordeaux est ${villes.indexOf("Bordeaux")}");
  print("L'index de Madrid est ${villes.indexOf("Madrid")}");

  villes.clear();
  print("Vidange de la liste : $villes");
}
