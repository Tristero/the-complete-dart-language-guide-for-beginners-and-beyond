# Collections

Il existe plusieurs types de collections dites itérables en Dart :

- Listes
- Sets
- Maps

Nous verrons leur utilisation, l'intéraction avec le système de type, puis nous verrons comme utiliser les conditions, les boucles et le *spreading*.

## 1-Listes

Les listes en Dart sont des tableaux donc manipulables avec des index.

### 1-Syntaxe, accès, mutation et itération

#### 1-Syntaxe d'une liste

```dart
var nomListe = [valeur1, valeur2, ..., valeurN];
```

**NB** : avec cette syntaxe chaque valeur peut être de n'importe quel type. Il est possible de forcer un type unique pour les valeurs de la liste (cf. [chapitre 3-Typage des listes](#3-typage-des-listes)).

#### 2-Accès à une valeur

Pour accéder à une valeur, on utilise un index :

```dart
var valeur = nomListe[index];
```

Si on utilise un index trop grand une exception est levée dont le message est suffisamment précis pour corriger l'erreur :

```txt
RangeError (index): Invalid value: Not in inclusive range 0..3: 15
```

#### 3-Modification d'une valeur

On peut affecter une nouvelle valeur à une position de la liste :

```dart
nomListe[0] = nouvelleValeur;
```

La liste est une structure de données mutable.

#### 4-Itération

Pour itérer sur une liste on utilisera for avec la syntaxe suivante (proche de Python ou de Go) : on nomme cette syntaxe boucle ```for-in```.

```dart
for (var element in nomListe){
    ...
}
```

Il s'agit d'une alternative à l'écriture traditionnelle de la boucle for. Mais rien n'empêche d'utiliser l'écriture traditionnelle surtout si l'on a la nécessité d'utiliser les index :

```dart
for (var i = 0; i < nomListe.length ; i++){
    ...
}
```

Quelques remarques sur la syntaxe ci-dessus : tous les éléments (condition, incrémentation et initialisation) sont configurables selon les besoins (selon l'algorithme rien n'empêche d'initialiser avec la longueur de la liste et de décrémenter). La variable ```length``` retourne la longueur de la liste.

[Code](codes/code1.dart)

### 2-Méthodes de liste

Les listes sont des objets et, donc, disposent de méthodes.

- ```isEmpty``` retourne un booléen : true si la liste est vide, false sinon
- ```isNotEmpty``` est le corrolaire de la précédente
- ```first``` retourn le premier élément de la liste. Une erreur est levée si la liste est vide.
- ```last``` retourne le dernier élément de la liste. Une erreur est levée si la liste est vide.
- ```add()``` prend une valeur en argument et ajoute en fin de liste la nouvelle valeur.
- ```insert()``` prend deux arguments : l'index positionnel dans la liste et une valeur.
- ```removeAt()``` prend un argument : un index pour la position dans la liste et retourne la valeur qui a été retirée.  
- ```clear()``` efface le contenu de la liste
- ```contains()``` prend un argument (de type identique à ceux de la liste) et retourne un booléen
- ```indexOf()``` retourne l'index pour une valeur donnée (cette valeur est passé en argument). Si la valeur passée n'existe pas alors la valeur ```-1``` est retournée.

Il existe d'autres méthodes plus ou moins spécifiques : comme l'extraction de sous-listes,...

[Code](codes/code2.dart)

### 3-Typage des listes

Nous savons qu'une liste peut se composer de types différents mais il est possible d'expliciter le type de la collection. Une liste est de type ```List``` et l'on peut préciser le type des valeurs contenues dans la liste :

```dart
List<TypeValeur> nomListe = [valeur1, ..., valeurN];
```

Un exmple :

```dart
List<String> villes = ["New-York", "Tokyo", "New Delhi"];
```

À partir de maintenant, toute introduction d'un type différent de ```String``` entrainera une erreur.

Une syntaxe avec ```var``` est possible :

```dart
var nomListe = <TypeValeur>[valeur1, ..., valeurN];
```

Avec notre exemple :

```dart
var villes = <String>["New-York", "Tokyo", "New Delhi"];
```

### 4-final et const

#### 1-final

Avec ```final```, une liste est figée en longueur. Ainsi le code suivant génère une erreur :

```dart
final villes = ["Paris", "Londres", "Berlin"];
villes = ["Bayonne"];
```

En revanche, le code suivant est possible car on ne modifie pas la longueur de la liste. Les éléments d'une liste ```final``` restent, quant à eux, **modifiables** :

```dart
final villes = ["Paris", "Londres", "Berlin"];
villes[1] = "Rome";
```

#### 2-const

```dart
const villes = ["Paris", "Londres", "Berlin"];
```

Dans ce cas, la liste n'est accessible qu'en lecture seule : la affectation d'une nouvelle valeur comme dans ```final``` est **impossible**. Il en va de même pour la taille de la liste : celle-ci est, comme pour ```final```, figée.

**Attention** : l'erreur ne sera lévée qu'à l'exécution mais pas à la compilation...

## 2-Sets

Les sets sont des listes dont les doublons ne peuvent co-exister : toute valeur dans un set est **unique**.

### 1-Syntaxes pour un Set

```dart
var nomSet = {valeur1, valeur2, ..., valeurN};
```

Bien évidemment comme pour une liste, on peut figer le type des valeurs en indiquant le type :

```dart
  Set<String> pays = {"France", "USA", "Inde", "Japon"};
  var pays2 = <String>{"France", "USA", "Inde", "Japon"};
```

### 2-Accès

Contrairement à une liste, le set n'a pas un accès de type ```monSet[X]```. Nous devons procéder comme suit en appelant sur l'instance du Set, la méthode ```elementAt()```.

```dart
monSet.elementAt(index);
```

L'argument est un entier positif partant de 0, comme tout index.

### 3-Ajout d'une nouvelle donnée

Il faut appeler la méthode ```add()``` qui prend en argument une valeur à ajouter au Set et retourne un booléen : ```true``` si l'opération s'est bien déroulé, ```false``` sinon :

```dart
bool resultat = monSet.add(valeur);
```

### 4-Suppression d'une donnée

Cela s'effectue avec l'appel de la méthode ```remove()``` qui prend un argument une valeur et retourne un booléen (si l'opération s'est bien déroulé alors ```true```, sinon ```false```):

```dart
var resultat = monSet.remove(valeur);
```

### 5-Méthodes utiles

Sur un objet Set, on peut appeler les méthodes suivantes :

- ```first``` retourne le premier élément du Set
- ```last``` retourne le dernier élément
- ```length``` retourne le nombre d'éléments du Set
- ```contains(valeur)``` retourne ```true``` si la valeur est présente dans le Set, sinon ```false```

### 6-Opérations sur les Sets

Il existe des opérations très utiles sur deux collections :

- l'union
- l'intersection
- la différence

#### 1-L'union

On appelle sur une collection, la méthode ```union()``` qui prend en paramètre un Set et retourne un objet Set faisant la fusion, l'union des deux collections :

```dart
var nouveauSet = set1.union(set2);
```

Le nouveau Set sera composé des éléments du Set appelant ```union()``` suivi des éléments du Set passés en argument de la méthode. **Rappel** : tous les doublons sont supprimés.

```dart
pays = {"France", "USA", "Inde", "Japon"};
var pays2 = {"USA", "Italie", "Norvège"};
var unionPays = pays.union(pays2);
print("Union = $unionPays");

// donnera le résultat suivant:
// Union = {France, USA, Japon, Islande, Italie, Norvège}
// la valeur USA n'apparaît qu'une seule fois.
```

#### 2-Intersection

C'est l'opération inverse de l'union : seules les valeurs doublons sont retournées par la méthode ```intersection()``` :

```dart
var nouveauSet = set1.intersection(set2);
```

Exemple :

```dart
var pays = {"France", "USA", "Inde", "Japon"};
var pays2 = {"USA", "Italie", "Norvège"};
var unionPays = pays.intersection(pays2);
print("Intersection = $unionPays");

// le résultat affiché sera :
// Intersection = {USA}
```

#### 3-Différence

C'est la soustraction des valeurs du set appelant la méthode ```difference()``` par les valeurs du Set passé en argument. Autrement dit, le Set retourné par la méthode ```difference()``` sera composé des éléments du Set appelant la méthode réduit de tous les éléments identiques au Set passé en argument :

```dart
var nouveauSet = set1.difference(set2);
```

Exemple :

```dart
var pays = {"France", "USA", "Inde", "Japon"};
var pays2 = {"USA", "Italie", "Norvège"};
var unionPays = pays.difference(pays2);
print("Différence = $unionPays");

// Le résultat d'unionPays sera :
// {France, Japon, Islande}
// USA est le doublon donc la valeur est retranché du Set pays
```

### 7-Boucles

Comme pour les listes, on utilise un boucle FOR-IN :

```dart
for (var element in monSet) {
    ...
}
```

Il existe d'autres méthodes : telles que ```fold()``` ou encore ```forEach()``` qui permettent de faire des opérations sur un set en les itérants. Avec ```forEach()``` :

```dart
var somme = 0;
var d = {1, 5};
// forEach demande une fonction anonyme :
d.forEach((element) {
  somme += element;
});
print(somme);
// affichera 6 !
```

Et cet exemple à exact identique à l'opération avec for-in :

```dart
var somme = 0;
var d = {1, 5};

for (var elem in d) {
  somme += elem;
}
print(somme);
```

[Code](codes/code3.dart)

## 3-Maps

En Python, on parlera de dictionnaire. Le principe des Maps est identique à celui de GO et consort.

On dispose d'une collection non ordonnée de couple clé-valeur. Les clés sont uniques et les valeurs peuvent être multiples.

### 1-Syntaxes pour un Map

#### 1-La plus simple

```dart
var m = {
  cle : valeur,
  ...
  cleN : valeur,
}
```

Les clés doivent être **toujours** de même type. Mais les valeurs peuvent être de types différents :

```dart
var id = {
  "nom": "Doe",
  "prenom": "John",
  "age": 42,
  "taille": 1.80,
};
```

#### 2-Instance de Map()

Autre façon, où l'on explicite le type ```Map```:

```dart
// instanciation
var m = Map();

// affectation clés-valeurs:
m[cle1] = valeur1;
...
m[cleN] = valeurN;
```

Un exemple :

```dart
var id = Map();
id["nom"] = "Doe";
id["prenom"] = "John";
id["age"] = 42;
id["taille"] = 1.80;
```

#### 3-Annotation de type

```dart
Map<Type1, Type2> m = {
  cle1 : valeur1,
  ...
  cleN: valeurN,
};

// ou encore l'instanciation d'abord
Map<Type1, Type2> m = Map();
// puis affectation clés-valeurs:
m[cle1] = valeur1;
...
m[cleN] = valeurN;
```

Un exemple : dans le cas d'utilisation de types multiples pour les valeurs, il faudra recourir à ```dynamic``` !

```dart
Map<String, dynamic> id = {
  "nom": "Doe",
  "prenom": "John",
  "age": 42,
  "taille": 1.80,
};
```

#### 4-Seconde forme d'annotation de type

```dart
var m = <Type1, Type2>{
  cle1 : valeur1,
  ...
  cleN : valeurN
}
```

Dans l'exemple précédent, on peut écrire :

```dart
var id = <String, dynamic>{
  "nom": "Doe",
  "prenom": "John",
  "age": 42,
  "taille": 1.80,
}
```

**ATTENTION** : il faut que les clés **soient** uniques

#### Quelques remarques importantes

- les clés doivent toujours être de même type et uniques,
- les annotations ne sont pas obligatoires mais rendent le code plus sécure.
- le type des valeurs n'est pas restreint à un seul type : pour cela il faut utiliser ```dynamic```

#### 5-var vs final vs const

On peut utiliser aussi bien ```final```, que ```const``` que ```var``` pour créer une instance de Map.

### 2-Accès aux valeurs et clés

#### 1-Accès à une valeur

Accéder à une valeur s'effectue comme pour une liste sauf que l'index est remplacé par le nom de la clé :

```dart
var m = {cle1 : valeur1};
m[cle1]
```

Pour tous les exemples, nous utiliserons notre Map ```id``` :

```dart
Map<String, dynamic> id = {
  "nom": "Doe",
  "prenom": "John",
  "age": 42,
  "taille": 1.80,
};

print("Accès à une valeur : le nom est ${id["nom"]}");
// Affichera : Accès à une valeur : le nom est Doe
```

#### 2-Accès à toutes les valeurs

Sytaxe : il faut appeler la propriété ```values```

```dart
m.values
```

Exemple :

```dart
print("Affichage de toutes les valeurs : ${id.values}");

// Résultat : Affichage de toutes les valeurs : (Doe, John, 42, 1.8)
```

L'objet retourné est itérable.

#### 3-Accès à toutes les clés

Syntaxe : il faut appeler la propriété ```keys``` qui retourne un objet itérable

```dart
m.keys;
```

Exemple :

```dart
print("Affichage de toutes les clés : ${id.keys}");

// Résultat : Affichage de toutes les clés : (nom, prenom, age, taille)
```

#### 4-Accès à une valeur inexistante

Que se passe-t-il si l'on accède à une clé qui n'existe pas ? Quelle valeur sera retournée ?

L'accès à une valeur depuis une clé inexistante retourne une valeur ```null```...

```dart
var inexistante = id['truc'];
print("Valeur inexistante = ${inexistante}");
// Résultat : Valeur inexistante = null
```

... et la structure de la map n'est pas modifiée : pas d'ajout de nouvelle clé avec une valeur ```null```.

```dart
print(id);
// Résultat : {nom: Doe, prenom: John, age: 42, taille: 1.8}
```

### 3-Modifications

#### 1-Modifier une valeur

Modifier une valeur s'effectue en utilisant la clé :

```dart
m[cleExistante] = nouvelleValeur;
```

Exemple :

```dart
id['age'] = 24;
print("'age' a été modifié : ${id["age"]}");

// Résultat : 'age' a été modifié : 24
```

#### 2-Modifier avec une clé inexistante

Modifier une map avec une clé qui n'est pas présente revient à génèrer une nouvelle entrée dans la map.

```dart
id["likesPizzas"] = true;

// génère une nouvelle entrée
```

### 4-Type dynamic sur les valeurs et casting

#### 1-Problème de typage à l'extraction d'une valeur

Quel type est retourné lors d'un accès à valeur ?

```dart
var prenom = id['prenom'];

// Quel est le type de prenom ? dynamic ? String ?
```

La réflexion va nous aider. On utilisera la propriété ```runtimeType``` comme suit :

```dart
print(prenom.runtimeType.toString());

// Résultat String
```

**Problème** : c'est que le type n'est déterminer qu'à l'exécution et non à la compilation : on ne peut pas accéder aux méthodes propres au type.

#### 2-Casting avec l'opérateur as

Pour régler ce problème, on peut préciser le type avec l'opérateur ```as```.

Syntaxe :

```dart
var v = m[cle] as String;
```

Exemple :

```dart
var prenom = id['prenom'] as String;
print(prenom.toUpperCase());
```

Avec le cast via l'opérateur ```as```, on peut accéder aux méthodes et propriétés de la variable ```prenom```.

#### 3-Casting sans as

Il est possible de caster sans as :

```dart
Type v = m[cle];
```

Avec notre exemple précédent, on peut écrire :

```dart
String prenom = id['prenom'];
```

#### 4-Absence de garantie du casting

**Attention** dans tous les cas, le cast ne garantit pas la sécurisation du code avant compilation :

```dart
var prenom = id['prenom'] as int;
print(prenom);
```

Une exception sera levée qu'**à la compilation** :

```txt
Unhandled exception:
type 'String' is not a subtype of type 'int' in type cast
```

#### 5-Quelle écriture privilégier

Il faut considérer l'utilisation de l'opérateur ```as``` comme référence d'écriture. Il sera utile ultérieurement.

### 5-Itérations

Comme pour les Sets, il existe plusieurs façons pour itérer une Map.

#### 1-Boucle for-in

##### 1-Itération sur les clés

Syntaxe : on utilise la propriété ```keys```

```dart
for (var cle in m.keys) {
  ...
}
```

Exemple :

```dart
for (var cle in id.keys) {
  print("clé = $cle");
}
```

##### 2-Itération sur les valeurs

On a deux façons de procéder. Soit en utilisant une itération avec ```keys``` puis dans la boucle on accède à la valeur :

```dart
for (var cle in id.keys) {
  print("clé = $id[cle]");
}
```

Soit plus rapide en utilisant la propriété ```values``` de l'instance de Map.

Syntaxe :

```dart
for (var valeur in m.values) {
  ...
}
```

Exemple :

```dart
for (var valeur in id.values) {
  print("valeur = $valeur");
}
```

#### 2-MapEntries : itérer sur les clés ET les valeurs

Au lieu d'écrire un code :

```dart
for (var cle in id.cle) {
  print("$cle = ${id[cle]}");
}
```

Dart nous fournit une écriture alternative en utilisant le type ```MapEntry```. Chaque élément d'une Map est appelée *entry* ou entrée : c'est le couple clé-valeurs. Dart fournit un type ```MapEntry``` qui nous permet d'extraire la valeur et la clé. Mais il faut recourir à la propriété ```entries``` de l'instance de la Map !

Syntaxe :

```dart
for (MapEntry me in m.entries) {
  // accès à la valeur :
  var v = me.value;

  // accès à la clé
  var k = me.key;
}
```

Exemple :

```dart
for (MapEntry me in id.entries) {
  print("clé (${me.key}) = valeur (${me.value})");
}

// Résultat :
//clé (nom) = valeur (Doe)
//clé (prenom) = valeur (John)
//clé (age) = valeur (24)
//clé (taille) = valeur (1.8)
```

#### 3-forEach()

```forEach()``` est une méthode appelée sur l'objet Map. Elle requiert une fonction anonyme en paramètre.

Elle fonctionne sur les propriétés :

- ```keys```
- ```values```
- ```entries```

Syntaxe :

```dart
// s'il n'y a qu'une seule expression, on pourra
// utiliser la forme condensée
m.entries.forEach((argument) => expression );

// Alternative et qui sera à utiliser avec
// plusieurs expressions
m.values.forEach((argument) {
  expression1;
  expression2;
  ...

});
```

Exemples :

```dart
id.entries.forEach((e) => print("clé (${e.key}) = valeur (${e.value})"));

id.keys.forEach((element) {
  var c = element;
  var v = id[element];
  print("clé ($c) = valeur ($v)");
});

// Dans les deux cas on aura le même résultat

id.values.forEach((element) {
  print("Valeur = $element");
});

// on pourra privilégier une ecriture plus condensée
//comme dans le premier exemple :

id.values.forEach((element) => print("Valeur = $element"));
```

[Code](codes/code4.dart)

## 4-Collections imbriquées

Comment représenter des collections imbriquées dans des collections ?

```txt
nom : Chez Mario
cuisine : italienne
notes : [5.0, 3.5, 4.0]

nom : A la bonne franquette
cuisine : française
notes : [5.0, 4.5, 4.5]

nom : New Delhi
cuisine : indienne
notes : [4.0, 4.5, 4.0]
```

Nous avons une liste de maps. Et chaque map possède un champ de type liste :

```dart
var restaurants = [
  {
    "nom": "Chez Mario",
    "cuisine": "italienne",
    "notes": [5.0, 3.5, 4.0],
  },
  {
    "nom": "A la bonne franquette",
    "cuisine": "française",
    "notes": [5.0, 4.5, 4.5]
  },
  {
    "nom": "New Delhi",
    "cuisine": "indienne",
    "notes": [4.0, 4.5, 4.0],
  },
];
```

Bien évidemment il est possible d'itérer sur de pareilles collections complexes.

[Code](codes/code5.dart)

## 5-Collection-if

Le *collection-if* est une condition évaluée qui retournera une valeur ou non dans la collection.

Au lieu d'écrire un code Dart conditionnel :

```dart
main(List<String> args) {
  final couleurs = ["rouge", "vert"];
  const ajouteBleu = true;
  const ajouteMarron = false;

  if (ajouteBleu) {
    couleurs.add("bleu");
  }

  if (ajouteMarron) {
    couleurs.add("marron");
  }
  
  print(couleurs);

}
```

Dart dispose d'un IF pour les collections. On peut incorporer un if **dans** une collection :

```dart
const ajouteBleu = true;
const ajouteMarron = false;

final couleurs = [
  "rouge",
  "vert",
  if (ajouteBleu) {"bleu"},
  if (ajouteMarron) {"marron"},
];
print(couleurs);
```

On peut même supprimer les accolades :

```dart
final couleurs = [
  "rouge",
  "vert",
  if (ajouteBleu) "bleu",
  if (ajouteMarron) "marron",
];
```

[Code](codes/code6.dart)

## 6-Collection-for

Permet d'ajouter des valeurs à une collection depuis une autre collection :

```dart
const nouvellesCouleurs = ["jaune", "violet"];

final couleurs = [
  "rouge",
  "vert",
  for (var couleur in nouvellesCouleurs) couleur,
];
```

[Code](codes/code7.dart)

### Alternative : addAll()

On peut utiliser sur une collection la méthode ```addAll()``` qui modifie *in-place* (dans la collection) en ajoutant une collection passée en argument (toute collection implémentant l'interface Iterable) :

```dart
couleurs.addAll(nouvellesCouleurs);
```

Les résultats sont identiques. Attention toutefois, le placement de la boucle *collection-for* est plus flexible car ```addAll()``` ajoute **toujours en fin de collection**. La boucle *collection-for* peut se placer n'importe dans la collection :

```dart
const nouvellesCouleurs = ["jaune", "violet"];

final couleurs = [
  "rouge",
  "vert",
  for (var couleur in nouvellesCouleurs) couleur,
  "indigo",
];

final couleurs2 = [
  "rouge",
  "vert",
  "indigo",
];
couleurs2.addAll(nouvellesCouleurs);

print("Résultat avec collection-for :\n$couleurs");
print("\nRésultat avec addAll() :\n$couleurs2");
```

Et le résultat diffèrera :

```txt
Résultat avec collection-for :
[rouge, vert, jaune, violet, indigo]

Résultat avec addAll() :
[rouge, vert, indigo, jaune, violet]
```

[Code](codes/code8.dart)

## 7-Opérateur spread

L'ajout de valeurs peut se passer de la boucle *collection-for*, comme avec JS ou Go, il existe un opérateur *spread* : ```...``` préposée à la nouvelle collection :

```dart
final couleurs = [
  "rouge",
  "vert",
  "indigo",
  ...["bleu", "jaune"],
];
```

Le résultat est :

```dart
[rouge, vert, indigo, bleu, jaune]
```

*Collection-if*, *collection-for* et l'opérateur *spread* sont "composables" : on peut les architecturer les uns avec les autres afin de moduler un comportement (exemple : [code](codes/exo4.dart)).

[Code](codes/code9.dart)

## 8-Copie de référence, shallow-copy et deep-copy

### 1-Copie

Une copie d'une collection du style :

```dart
final l = [1,2,3];
final cp = l;
cp[0] = 0;
print(l != cp);
```

Le résultat sera ```false``` car on a une copie de références : si on copie une liste vers une autre, il ne s'agit pas de deux listes différentes. ```cp``` pointe sur la même liste que la variable ```l``` et donc toute modification de ```cp``` entre *de facto* une modification de la liste pointée par ```l```. Il s'agit pas d'une copie mais d'un partage de référence en mémoire.

### 2-Shallow copy

Une solution serait :

```dart
cp1 = [...l];
// ou encore
cp2 = [
  for (var elem in l) elem,
]
```

Sauf que cette façon de faire ne marchera pas pour les collections imbriquées :

```dart
  var cp3 = [...l];
  (cp3[2] as List)[1] = 42;
  print(l);
```

La liste ```l``` sera modifiée aussi ! Nous avons affaire à une *shallow-copy* ou copie de surface.

### 3-Deep copy

Pour une copie profonde, Dart propose la méthode ```List.from()``` qui réalise l'opération à notre place :

```dart
var deepCopy = List.from(l);
(deepCopy[2] as List)[1] = 42;
print(deepCopy == l);
```

Il n'y a plus de "contagion" : la réponse est ```false```. Nous avons modifié la copie sans engendrer de modification de la liste originale.

[Code](codes/code10.dart)

## 9-Allez plus loin

Voici une liste de liens vers la documentation officielle de Dart et les collections itérables :

- [Documentation sur les Listes](https://api.dart.dev/stable/2.13.4/dart-core/List-class.html)
- [Documentation sur les Sets](https://api.dart.dev/stable/2.13.4/dart-core/Set-class.html)
- [Documentation sur les Maps](https://api.dart.dev/stable/2.13.4/dart-core/Map-class.html)
- [Codelab sur les collections itérables](https://dart.dev/codelabs/iterables)
