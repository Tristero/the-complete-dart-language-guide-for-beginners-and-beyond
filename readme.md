# The Complete Dart Guide for Beginners and Beyond

Notes et traduction.

Nous n'utiliserons pas DartPad mais VSCode avec le package [Dart](https://marketplace.visualstudio.com/items?itemName=Dart-Code.dart-code) qui va nous permettre dans VSCode de tester directement notre code sans avoir recours à une connexion Internet.

## Sommaire

- [Chapitre 1 - Bases](Chapitre-1/chapitre1.md)
- [Chapitre 2 - Système de type en Dart](Chapitre-2/chapitre2.md)
- [Chapitre 3 - Structures de contrôle](Chapitre-3/chapitre3.md)
- [Chapitre 4 - CLI (TP)](Chapitre-4/chapitre4.md)
- [Chapitre 5 - Collections](Chapitre-5/chapitre5.md)
- [Chapitre 6 - Traitement de données (TP)](Chapitre-6/chapitre6.md)
- [Chapitre 7 - Null safety](Chapitre-7/chapitre7.md)
- [Chapitre 8 - Fonctions (bases)](Chapitre-8/chapitre8.md)
- [Chapitre 9 - Fonctions (notions avancées et opérateurs fonctionnels)](Chapitre-9/chapitre9.md)
- [Chapitre 10 - POO (bases)](Chapitre-10/chapitre10.md)
- [Chapitre 11 - POO (notions avancées)](Chapitre-11/chapitre11.md)
- [Chapitre 12 - Application eCommerce simple (TP)](Chapitre-12/chapitre12.md)
- [Chapitre 13 - Mixins et extensions](Chapitre-13/chapitre13.md)
- [Chapitre 14 - Gestion des erreurs et exceptions](Chapitre-14/chapitre14.md)
- [Chapitre 15 - Programmation asynchrone](Chapitre-15/chapitre15.md)
