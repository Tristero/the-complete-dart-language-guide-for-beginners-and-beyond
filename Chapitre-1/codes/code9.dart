void main(List<String> args) {
  // Convertion d'un int vers String
  int age = 36;
  String strAge = age.toString();
  print(strAge);
  print(42.toString());

  // convertion d'un double en String
  double d = 42.toDouble();
  print(d.toString());

  // convertion d'un double en entier puis en chaîne
  d.toInt();
  print(d.toInt());

  // convertion d'une chaîne en double
  String str = '4.2';
  double dbl = double.parse(str) + 1.0;
  print(dbl);

  // convertion d'un double vers int
  int i = dbl.toInt();
  print(i.toString());
  print("avec round() = ${(4.2).round()}");
  print("avec ceil() = ${(4.2).ceil()}");
  print("avec truncate() = ${(4.2).truncate()}");
  print("avec floor() = ${(4.2).floor()}");
}
