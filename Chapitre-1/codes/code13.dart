import 'dart:math';

main(List<String> args) {
  //int age = 42;
  Random r = Random.secure();
  int age = r.nextInt(100);

  String msg = age < 18
      ? "Vous êtes mineur car vous avez $age ans."
      : "Vous avez $age ans et vous êtes un adulte.";
  print(msg);
}
