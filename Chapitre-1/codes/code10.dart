main(List<String> args) {
  print(10 - 2 * 3); // donne bien 4
  print((10 - 2) * 3); // donne bien 24

  print(7 / 3); // donne le résultat 2.3333333333333335
  print("Résultat de la division euclidienne 7 ~/ 3 = ${7 ~/ 3}");
}
