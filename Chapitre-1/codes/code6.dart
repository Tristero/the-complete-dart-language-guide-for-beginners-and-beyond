void main(List<String> args) {
  print("Concaténations");
  String s1 = "John";
  String s2 = "Doe";
  print(s1 + " " + s2);

  String s3 = "John " "Doe";
  print(s3);

  print("\nInterpolation");
  int age = 42;
  double taille = 1.80;

  print("Je m'appelle $s1 $s2. J'ai ${age} ans et je mesure ${taille}");

  double fahrenheit = 451.0;
  print(
      "\nLe papier brûle à ${fahrenheit}°F soit ${(fahrenheit - 32.0) * (5.0 / 9.0)}°C");
}
