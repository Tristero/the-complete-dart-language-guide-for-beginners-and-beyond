main(List<String> args) {
  int x = 11;
  int y = ++x;
  // réinitialisation de x à sa première valeur
  x = 11;
  int z = x++;
  print("x=$x || y=$y || z=$z || x=$x");
}
