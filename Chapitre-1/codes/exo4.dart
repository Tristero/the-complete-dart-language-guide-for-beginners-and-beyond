main(List<String> args) {
  double tempFahrenheit = 90;
  double celsius = (tempFahrenheit - 32) / 1.8;
  print("${tempFahrenheit}F = ${celsius.toStringAsFixed(1)}C");
}
