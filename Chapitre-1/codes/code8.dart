main(List<String> args) {
  String msg = "hello, world.";
  String low = msg.toLowerCase();
  String up = msg.toUpperCase();

  print(up);
  print(low);

  print("HeLlO, wOrLd !".toLowerCase());
  print("HeLlO, wOrLd !".toUpperCase());

  // vérification de la présence d'un motif

  String pizza = "I love pizzas";
  print(pizza.contains("love"));
  print(pizza.contains("love", 3));

  String pastas = pizza.replaceAll("pizzas", "pastas");
  print(pastas);
  // remplacement
}
