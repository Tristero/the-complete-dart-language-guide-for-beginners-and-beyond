main(List<String> args) {
  print(0xA);

  // opérateurs bitwise
  int x = 0xF0; // binaire = 11110000
  int y = 0x0F; // binaire = 00001111

  print((x | y).toRadixString(2)); // binaire = 11111111

  print((x & y).toRadixString(2)); // binaire = 0

  print((x ^ y).toRadixString(2)); // binaire = 11111111

  print((~x).toRadixString(2)); // binaire = 11111111111111111111111111110000

  int z = 4; // binaire = 100
  print(
      "Shifting à droite : z >> 1 = ${(z >> 1).toRadixString(2)} (ce qui équivant à une division par 2 : ${(z >> 1).toRadixString(10)})");
  print(
      "Shifting à gauche : z << 1 = ${(z << 1).toRadixString(2)} (ce qui équivant à une multiplication par 2 : z*2=${(z << 1).toRadixString(10)})");
}
