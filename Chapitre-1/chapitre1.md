# Bases de Dart

La plupart des concepts abordés ci-dessous ne sont pas propres à Dart et on les retrouve partagés dans bon nombre de langages objet.

## 1-Méthode main()

Point d'entrée de Dart, cette méthode ne retourne rien (```void``` préfixe le nom de la méthode mais c'est *optionnel* comme nous le verrons par la suite) :

```dart
void main(){}
```

La version complète pour la prise en compte de paramètres :

```dart
main(List<String> args) {
  
}
```

[Code](codes/code1.dart)

Tout programme Dart doit comporter cette méthode. Et ce code est compilable :

```sh
dart code1.dart
```

Et le résultat à la console n'aura aucun effet visible.

## 2-String et affichage de texte

Le fameux "Hello, world". Pour afficher du texte à la console, tout comme en Python, on utilisera la méthode ```print()```.

```dart
void main() {
  print("Hello, world.");
}
```

À noter que ```print()``` peut prendre des guillemets simples. Ainsi les deux déclarations suivantes sont équivalentes :

```dart
print("hello world")
print('hello world')
```

### Point-virgule

**ATTENTION** contrairement à Python, Go, F#, ... chaque déclaration est séparée par un ```;``` !!! Donc son absence peut générer une erreur :

```dart
void main() {
  print("Hello, world.")
}
```

Le résultat est une erreur :

```txt
code2.dart:2:24: Error: Expected ';' after this.
  print("Hello, world.")
                       ^
```

[Code](codes/code2.dart)

### Indentation du code

Contrairement à Go ou Python, l'indentation n'est pas une obligation mais par défaut le formateur de texte Dart ajoutera les indentations là où c'est nécessaire.

## 3-Variables : déclaration et initialisation

Dart est fortement typé et la création d'une variable passe par son typage selon la syntaxe suivante :

```dart
Type nomVariable;
```

L'initialisation peut se faire *a posteriori* :

```dart
String str;
str = "Ma valeur";
```

Ou en une ligne :

```dart
String str = "Ma valeur";
```

### Nomenclature

Pour écrire une variable on privilégie comme en Go, le camelCase.

**Attention** le premier caractère d'une variable ne peut pas être un nombre ! De même Dart est sensible à la casse !

L'ensemble des règles d'écriture se trouve la page [Effective Dart: Style](https://dart.dev/guides/language/effective-dart/style).

[Code](codes/code3.dart)

## 4-Types de base

### int

Entier de 64 bits sur les plateformes x86-64 mais pas pour le web (longueur plus faible) : ce sera la même longueur qu'en JS.

```dart
int x = 42;
```

### double

Nombre à virgule flottante de 64 bits.

```dart
double y = 4.2;
```

### bool

Booléen ne possédant que deux valeurs ```true``` vs ```false``` :

```dart
bool b = false;
```

Ces trois types sont prédéfinis dans le langage.

[Code](codes/code4.dart)

## 5-Concaténation et interpolation

### Multiligne

Il est possible de faire du texte sur plusieurs lignes :

```dart
String txt1 = """Ceci
est un
texte
sur plusieurs
lignes.""";
print(txt1);

String str2 = '''Ceci est
un autre texte
sur plusieurs
ligne en utilisant
les simples guillemets''';
print(str2);
```

[Code](codes/code5.dart)

### Concaténation

On pourra utiliser ```+``` pour concaténer deux ```String``` ensemble :

```dart
String s1 = "John";
String s2 = "Doe";

print(s1 + " " + s2);
```

On obtient la chaîne suivante : ```John Doe```

Il est possible de concaténer **sans** le symbole ```+``` plusieurs **littéraux** :

```dart
String s3 = "John " "Doe";
print(s3);
```

### Interpolation ${}

Dart supporte l'interpolation à l'aide de l'opérateur ```${}```.

```dart
String nom = "John";
String prenom = "Doe";
int age = 42;
double height = 1.80;

print("Je m'appelle ${prenom} ${nom}. J'ai ${age} ans et je mesure ${taille});
```

**NB** lorsqu'on utilise une simple variable on peut se passer des accolades. Ainsi :

```dart
print("Je m'appelle ${prenom} ${nom}. J'ai ${age} ans et je mesure ${taille});
```

est équivalent à :

```dart
print("Je m'appelle $prenom $nom. J'ai $age ans et je mesure $taille);
```

L'interpolation peut évaluer des expressions à l'intérieur de ```${}``` :

```dart
  double fahrenheit = 451.0;
  print(
      "Le papier brûle à ${fahrenheit}°F soit ${(fahrenheit - 32.0) * (5.0 / 9.0)}°C");
```

[Code](codes/code6.dart)

## 6-Echappement et caractères

Le problème avec les chaînes concerne le jonglement entre les ```"``` et ```'``` : on utilisera ```\``` pour gérer les possibles interférences et erreurs.

```txt
'....\'....'; // -----> Correct
'....'.....'; // -----> Erreur
"...."....."; // -----> Erreur
"...\"....."; // -----> Correct
"....'....."; // -----> Correct
```

De même l'utilisation du backslash dans une chaîne devra être précédé par un backslsah : ```\\```

```dart
String cheminCorrect = "C:\\Windows\\system32";
```

Dart interprète un certain nombre de caractères échappés :

```txt
\n -> newline
\t -> tabulation
```

### r = raw

Précéder une chaîne avec le caractère ```r``` indique que tout caractère à l'intérieur de la chaîne ne sera pas interprété (aucune interpolation) :

```dart
String str =
    r"Les éléments comme ${5} apparaitront normalement sans être interpolé. De même de \ \n ou encore \t";
```

Le résultat apparaîtra :

```txt
Les éléments comme ${5} apparaitront normalement sans être interpolé. De même de \ \n ou encore \t
```

**NB** : on peut écrire ```r'....'``` ou ```r"....."```.

[Code](codes/code7.dart)

## 7-Opérations sur les chaînes

Ces opérations ne modifient pas les chaînes.

### Majuscules

Dart fournit tout un ensemble d'opérations sur les chaînes de caractères. Un String est un objet et donc on peut appeler ses méthodes à l'aide du traditionnel ```.```:

```dart
void main() {
  String msg = "hello, world.";
  String up = msg.toUpperCase();
}
```

Cette méthode retournera le texte en majuscule.

### Minuscules

L'opération inverse est proposée par la méthode ```toLowerCase()```.

```dart
String low = msg.toLowerCase();
print(low);
```

Cette méthode comme la précédente fonctionne sur les littéraux :

```dart
print("HeLlO, wOrLd !".toLowerCase());
print("HeLlO, wOrLd !".toUpperCase());
```

### Sous-chaînes

Dart dispose de la méthode ```contains()``` qui prend en paramètre une chaîne de caractères et retourne un booléen. Il existe un paramètre optionnel : un index. Cet index indique la position dans la chaîne où l'on souhaite démarrer la recherche :

```dart
String pizza = "I love pizzas";
print(pizza.contains("love"));
```

Et le résultat retourné sera ```true```.

Avec un index de position de recherche modifié (par exemple 3) :

```dart
print(pizza.contains("love", 3));
```

nous obtenons un résultat à ```false```.

### Remplacement

Dart fournit non pas une mais plusieurs  méthodes  ```replaceAll()``` (qui remplace toutes les occurences du motif passé en argument par un autre motif en second argument), ```replaceFirst()``` qui remplace la première occurence rencontrée par le second argument passé. Il en existe d'autres notamment ```replaceRange()``` qui fonctionne comme ```replaceAll()``` mais demande un troisième argument : un index positionnel. Et ces méthodes retournent un type ```String``` :

```dart
String pizza = "I love pizzas";
String pastas = pizza.replaceAll("pizzas", "pastas");
print(pastas);
```

Nous obtenons la chaîne : ```I love pastas```.

[Code](codes/code8.dart)

## 8-conversion de types

### int vers String

Il suffit d'appeler une méthode ```toString()``` sur l'entier :

```dart
print(42.toString());
```

### int vers double

Le type entier dispose de méthode de conversion : ```toDouble()``` :

```dart
double d = 42.toDouble();
```

### double vers int

Et comme pour int, double dispose de méthodes pour convertir en un entier ```toInt()``` et vers d'autres types.

La conversion d'un double vers int avec la méthode ```toInt()```signifie que le double retourné sera tronqué de sa partie décimale :

```dart
// dbl vaut 5.2 : suite du code précédent
int i = dbl.toInt();
print(i);
```

retournera ```5```.

Et bien évidemment, nous disposons de la méthode ```toString()``` pour effectuer la conversion vers le type String.

Il existe d'autres méthodes de conversion vers le type int :

- ```floor()```
- ```round()```
- ```truncate()```
- ```ceil()```

### String vers double

le type double possède une méthode nommée ```parse()``` qui prend en argument une chaîne et retourne un double :

```dart
String str = '4.2';
double dbl = double.parse(str) + 1.0;
print(dbl);
```

#### Le cas des littéraux

On peut écrire avec Dart : ```double d = 42;```. Dart effectue la *promotion* automatique du type int vers double.

En revanche, l'expression ```int i = 4.2;``` est **illégale** et nécessite l'appel de la méthode ```toDouble()``` (avec les littéraux, il faudra utiliser les parenthèses : ```(4.2).toDouble()```), ou encore ```floor()```, ```round()```, ```ceil()``` ou encore ```truncate()```.

[Code](codes/code9.dart)

## 9-Opérations arithmétiques

La fonction ```print``` peut prendre des expressions mathématiques :

```dart
print(10 - 2 * 3); // donne 4
```

Et il y a le respect des priorités arithmétiques :

```dart
  print((10 - 2) * 3); // donne bien 24

```

### 1-Principaux opérateurs

Dart utilise les opérateurs mathématiques suivants :

- ```+```
- ```-```
- ```*```
- ```/```
- ```~/``` : division euclidienne (e.g si 7/3 donne environ 2.33..., le résultat de la division euclidienne donne 2)
- ```%``` le modulo

[Code](codes/code10.dart)

### 2-Et leur équivalent pour assigner

Comme pour la plupart des langages, Dart utilise des opérateurs qui permettent de réassigner à une variable la valeur modifiée par une autre valeur

- ```+=```
- ```-=```
- ```*=```
- ```/=```
- ```~/=```
- ```%=```

## 10-Incrémentation et décrémentation

Comme en Java ou en C/C++, Dart permet une incrémentation et une décrémentation postfixe **ET** préfixe.

- ```x++```
- ```++x```
- ```x--```
- ```--x```

La différence concerne l'affectation :

```dart
int x = 11;
int y = x++;
// réinitialisation de x à 11
x = 11;
int z = ++x;
print("x=$x || y=$y || z=$z");
```

Nous obtenons le résultat : ```x=12 || y=12 || z=11```

[Code](codes/code11.dart)

Donc *postfixé*, l'incrément (ou le décrément) signifie on assigne x à y **puis** l'incrémentation (ou la décrémentation) a lieu.

*Préfixé*, l'opération d'incrémentation est réalisée puis x est assigné à z.

Donc il faudra faire très attention avec les incrémentations/décrémentations postfixées vs préfixées !!

## 11-Opérateurs booléens et logiques

### 1-Opérateurs booléens

- ```==```
- ```!=```
- ```>=```
- ```>```
- ```<=```
- ```<```

On peut comparer un entier avec un double mais pas un entier avec une chaîne.

### 2-Opérateurs logiques

- ```||``` OU logique
- ```&&``` ET logique
- ```!``` NOT

La précédence ou priorité de ces opérateurs est supérieure aux autres opérateurs booléens.

### 3-Méthodes particulières

Certains objets disposent de méthodes particulières pour effectuer des vérifications et retourner un booléen : par exemple les chaînes de caractères disposent de ```isEmpty()``` et ```isNotEmpty()```, ou encore de ```contains()``` qui vont retourner un booléen.

```dart
String email = "truce@machin.com";
print(email.isNotEmpty && email.contains("@"));
```

Ce code affichera ```true```.

[Code](codes/code12.dart)

## 12-Opérateur ternaire

Comme pour JS, Dart dispose d'un opérateur utile pour éviter les conditions IF simples, c'est l'opérateur ```expr1 ? expr2 : expr3``` qui peut se traduire par *si expr1 est vraie alors expr2 exécutée sinon expr3* :

```dart
int age = 42;
String msg = age > 18 ? "Vous êtes majeur." : "Vous êtes un mineur.";
print(msg);
```

Le message affiché sera ```Vous êtes majeur.```.

Remarque : les types des expressions 2 et 3 doivent être les mêmes (on ne peut mélanger les types de retour). Ici nous avons le type String dans le deux cas. Et la première expression doit **toujours** être une condition.

[Code](codes/code13.dart)

## 13-Format hexadécimal, opérateurs bitwise et shifting

### 1-Ecriture

Les hexadécimaux s'écrivent en les préfixant avec : ```0x``` (zéro suivi de X en minuscule)

```dart
print(0xA); // retourne le chiffre 10
```

### 2-Opérateurs bitwise

#### 1-Opérateurs binaires

- ```|``` opérateur OU LOGIQUE
- ```^``` opérateur OU EXCLUSIF
- ```&``` opérateur ET

On peut réaffecter la nouvelle valeur dans l'ancienne variable avec les opérateurs suivants :

- ```|=``` remplace ```x = x | 0x0F``` par ```x |= 0x0F```
- ```^=``` remplace ```x = x ^ 0x0F``` par ```x ^= 0x0F```
- ```&=``` remplace ```x = x & 0x0F``` par ```x &= 0x0F```

#### 2-Opérateur unaire

- ```~``` opérateur NOT

Il existe son équivalent pour réaffecter la valeur modifiée dans la même variable :

- ```~=``` remplace ```x = x ~ 0x0F``` par ```x ~= 0x0F```

### 3-Méthodes disponibles

À partir d'un hexadécimal, on peut changer le rendu de la base : ```toRadixString(int radix)```. C'est le *radix* qui représente la base à employée : 2 pour le binaire, 10 pour le système décimal ou encore 16 pour le format hexadécimal. Il existe des dizaines d'autres méthodes utilisables comme ```toInt()``` qui retourne la répresentation de l'hexadécimal sous forme d'un entier.

### 4-Opérateurs de shifting

- ```>>``` shift vers la droite
- ```<<``` shift vers la gauche

On peut réassigner la nouvelle directement dans l'ancienne variable

- ```>>=``` : remplace ```x = x >> 1;``` par ```x >>= 1;```
- ```<<=``` : remplace ```x = x << 1;``` par ```x <<= 1;```

[Code](codes/code14.dart)

## 14-Commentaires

Comme dans les langages issus du C, on dispose de deux types de commentaires traditionnel plus un troisième pour générer de la documentation :

- de ligne : ```// ...ici commentaire sur une seule ligne```
- de bloc : ```/* commentaire sur plusieurs lignes */```
- de documentation : ```/// ... commentaire pour la documentation sur une ligne```

De même ```// TODO: ...texte``` permet d'avoir un pense-bête pour faire quelque chose.
