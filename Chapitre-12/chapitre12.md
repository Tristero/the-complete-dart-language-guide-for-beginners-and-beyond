# TP : application simple d'eCommerce

Un site d'eCommerce repose sur un motif simple : nous disposons d'une liste de produits, l'utilisateur sélectionne un item et le place dans un panier. Puis il passe au paiement. Une fois le paiement réalisé la commande est complète.

Ici nous n'allons pas nous intéresser à la création d'un site web mais d'une application console.

Nous aurons trois entités :

- Produit avec ces propriétés : un id unique, un nom et son prix
- Item : le produit, la quantité et le prix
- Panier : la liste des items et le total

Ces trois entités seront des classes.

## 1-Création du projet

```bash
$ dart create ./simple_ecommerce
```

Puis on se place dans le nouveau répertoire créé pour lancer VSCode.

Nous allons créer nos classes dans un seul fichier : contrairement à C# ou Java, il est conseillé de regrouper autant que possible les classes dans un même fichier que nous nommerons ```gestion.dart``` et il sera stocké dans ```./bin```.

## 2-Création des classes

Trois classes vont représenter les étapes de notre pseudo-application : une classe pour un produit, une classe pour un produit et sa quantité (on calculera son prix) et une classe représentant le panier (ici se posera la question du type de collection à employer)

### 1-Classe Product

Cette classe est simple.

```dart
class Product {
  final int id;
  final String name;
  final double price;

  const Product({
    required this.id,
    required this.name,
    required this.price,
  });

  @override
  String toString() => '($letter)${name.substring(1)}: $price €';

  // extraction de la première lettre du nom du produit
  String get letter => name[0];
}
```

Elle ne récupère que des données concernant un seul produit. On n'a redéfini la méthode ```toString()```. Et nous voulons utiliser un getter qui retournera la première lettre du produit (cette première lettre sera très utile pour que l'usager fasse un choix non en tapant le produit en entier mais juste la première lettre du produit).

### 2-Classe Item

Nous allons récupérer un produit de type ```Product``` et une quantité (par défaut sa valeur sera 1). Puis on se crée un getter ```price``` qui va calculer le prix en fonction de la quantité voulu pour un produit :

```dart
class Item {
  final Product product;
  final int quantity;

  const Item({
    required this.product,
    this.quantity = 1, // par défaut pour l'utilisateur
  });

  double get price => product.price * quantity;

  @override
  String toString() => '$quantity x ${product.name}: $price €';
}
```

### 3-Classe Cart

Représentant le panier et donc l'ensemble de son contenu pose la question : quelle collection employer ?

Si on ajoute un produit on a deux cas : soit le produit existe et il faut incrémenter sa quantité soit il n'exite pas et il faut lui affecter une nouvelle quantité. Pas de doublons de produits ? alors nous devons utiliser une Map! Cette Map aura pour clé l'id du produit, et la valeur sera le ```Product```.

Nous créerons trois méthodes :

- une qui ajoutera un produit s'il existe il incrémentera la quantité de 1 sinon il créera une nouvelle entrée dans la map
- une qui calculera le montant total du panier
- une qui redéfinira la méthode ```toString()```

```dart
class Cart {
  final Map<int, Item> _items = {};

  void addProduct(Product p) {
    final item = _items[p.id];

    if (item == null) {
      _items[p.id] = Item(product: p);
    } else {
      _items[p.id] = Item(product: p, quantity: item.quantity + 1);
    }
  }

  double total() => _items.values
      .map((item) => item.price)
      .reduce((value, element) => value + element);

  @override
  String toString() {
    if (_items.isEmpty) return '------\nPanier vide';

    final lst = _items.values.map((item) => item.toString()).join('\n');

    return '------\n$lst\n------\nTotal : ${total()} €\n------';
  }
}
```

## 3-Main

Maintenant que nous avons deux des trois classes, nous allons définir un pseudo-code d'interaction utilisateur-interface CLI

### 1-Pseudo-code

Ce que nous voulons faire peut se décrire comme suit :

```txt
Dans une boucle infinie :

- afficher un message : voir le panier / ajouter un item / payer
- si ajout d'item :
  - choisir un produit
  - ajouter le produit au panier
  - afficher le panier
- si voir le panier :
  - afficher le panier
- si payer :
  - effectuer le paiement
  - sortie de la boucle

Fin de la boucle
```

### 2-Mise en place du code

Pour afficher un message à l'écran :

```dart
import 'dart:io';

stdout.write('une texte à afficher.');
```

```write()``` est une fonction qui crée pas de retour à la ligne contrairement à ```writeln()```.

Pour lire une entrée :

```dart
import 'dart:io';

final lectureEntree = stdin.readLineSync();
```

Nous obtenons notre premier code :

```dart
import 'dart:io';

void main(List<String> arguments) {
  while (true) {
    stdout.write(
        'Que souhaitez-vous faire ? (v)oir vos choix, (a)jouter un produit, (p)ayer :');

    final choixUtilisateur = stdin.readLineSync();

    switch (choixUtilisateur) {
      case 'a':
        break;
      case 'v':
        break;
      case 'p':
        break;
      default:
    }
  }
}
```

### 3-Création d'un catalogue

Nous allons pouvoir créer un catalogue de produits. Il s'agira d'une liste de type ```List<Product>```.

```dart
const List<Product> catalogue=[
  Product(id: 1, name: 'pommes', price: 1.6),
  Product(id: 2, name: 'bananes', price: 0.7),
  Product(id: 3, name: 'courgettes', price: 1.0),
  Product(id: 4, name: 'raisin', price: 2),
  Product(id: 5, name: 'oignon', price: 0.8),
  Product(id: 6, name: 'avocat', price: 1.5),
];
```

### 3-Fonction helper : choixProduit()

Pour une meilleure gestion

#### 1-choixProduit()

Cette fonction va permettre à l'utilisateur de pouvoir choisir parmi la liste de produits disponibles. Cette fonction retournera un type ```Product?```.

Mais le choix se fera en tapant la première lettre du produit :

```dart
Product? choixProduit() {
  final lstProduits = catalogue.map((produit) => produit.toString()).join('\n');
  stdout.writeln('Liste des produits disponibles\n$lstProduits');
  stdout.write('Votre choix : ');
  final choix = stdin.readLineSync();

  for (var product in catalogue) {
    if (choix == product.letter) {
      return product;
    }
  }

  // si la lettre employée n'est pas trouvée
  print('Produit inconnu.');
  return null;
}
```

#### 2-Premier test

Modifions notre méthode Main pour lui adjoindre notre fonction helper :

```dart
void main(List<String> arguments) {
  while (true) {
    stdout.write(
        'Que souhaitez-vous faire ? (v)oir vos choix, (a)jouter un produit, (p)ayer :');

    final choixUtilisateur = stdin.readLineSync();

    switch (choixUtilisateur) {
      case 'a':
        final prod = choixProduit();
        if (prod != null) {
          print(prod.toString());
        }
        break;
      case 'v':
        break;
      case 'p':
        break;
      default:
    }
  }
}
```

Nous pouvons tester notre code avec ```dart run simple_ecommerce.dart```.

Pour quitter : CTRL + C

#### 3-Modification du main et second test

Nous allons introduire le panier en créant une variable hors boucle puis appeler la méthode ```addProduct()``` dans le cas ```'a'``` et afficher le panier. De même nous pouvons aussi gérer le cas ```'v'``` puisqu'il s'agit seulement de voir le contenu du panier :

```dart
void main(List<String> arguments) {
  final panier = Cart();

  while (true) {
    stdout.write(
        'Que souhaitez-vous faire ? (v)oir vos choix, (a)jouter un produit, (p)ayer :');

    final choixUtilisateur = stdin.readLineSync();

    switch (choixUtilisateur) {
      case 'a':
        final prod = choixProduit();
        if (prod != null) {
          panier.addProduct(prod);
          print(panier);
        }
        break;
      case 'v':
        print(panier);
        break;
      case 'p':
        break;
      default:
    }
  }
}
```

À nouveau, nous pouvons tester notre code.

## 4-Paiement

La fonction qui va se charger du traitement de la finalisation de la commande retournera un booléen (indiquant si les opérations ce sont bien déroulées) et on lui passera en argument un type ```Cart```.

Première chose à vérifier : si le panier est vide. Nous allons rajouter un getter à ```Cart```, ```isEmpty()``` retournant un booléen : ```bool get isEmpty => _items.isEmpty;```

Avec ce getter nous allons pouvoir gérer le cas où le panier est vide. Il ne se passe rien.

Dans les autres cas, on affichera le total et le paiement. L'utilisateur devra indiquer le montant. Il faudra gérer d'abord les cas où rien n'est passé.

Puis il faudra parser le montant en valeur numérique avec ```double.tryParse()``` qui retourne soit une valeur soit un ```null```. Il faudra gérer ```null```. Puis si le montant passé par l'utilisateur est plus grand ou égal au montant, il faudra calculer la différence.

```dart
bool paiement(Cart c) {
  if (c.isEmpty) {
    print('Le panier est vide');
    return false;
  }

  final total = c.total();
  print('Total : $total €');

  // paiement
  stdout.write('Paiement liquide : ');
  final input = stdin.readLineSync();
  if ((input == null) || (input.isEmpty)) {
    return false;
  }

  final valeur = double.tryParse(input);
  if (valeur == null) return false;
  if (valeur >= total) {
    final diff = valeur - total;
    print('Rendu au client = ${diff.toStringAsFixed(2)} €');
    return true;
  } else {
    print("Il manque de l'argent.");
    return false;
  }
}
```

On va pouvoir retourner dans la méthode ```main``` et ajouter la nouvelle fonction dans le cas ```'p'``` de notre ```switch``` :

```dart
void main(List<String> arguments) {
  final panier = Cart();

  loop:
  while (true) {
    
    ...

    switch (choixUtilisateur) {
      
      ...

      case 'p':
        if (paiement(panier)) {
          break;
        }
        break;
    }
  }
}
```

Problème : la boucle continuera de fonctionner même si le paiement est réalisé ! On a deux choix. Soit transformer le while en créant une variable booléenne externe :

```dart
void main(List<String> arguments) {
  final panier = Cart();

  var b = true;
  while (b) {
    
    ...

    switch (choixUtilisateur) {
      
      ...

      case 'p':
        if (paiement(panier)) {
          b = false;
        }
        break;
    }
  }
```

Mais il existe une solution alternative : le **label**. En plaçant un nom quelconque juste avant la boucle, nous pouvons casser la boucle dans le corps de celle-ci en appelant le label.

Syntaxe :

```dart
monLabel:
while(true){

  ...

  break monLabel;
}
```

Ce qui donne (en résumé) :

```dart
  loop:
  while (true) {
    
    ...

    switch (choixUtilisateur) {
      
      ...

      case 'p':
        if (paiement(panier)) {
          break loop;
        }
        break;
    }
  }
```

**NB** le nom ici est loop mais cela peut être n'importe quel nom.

Le code est complet !

## 5-Structure d'un projet

Si en C# ou en Java, on choisit au mieux une classe par fichier, en Dart, ce choix n'est pas toujours pertinent notamment avec les membres privés d'une classe. Parfois ces membres doivent être accessibles à d'autres classes (ce que l'on nommerait ```protected``` dans d'autres langages) donc il faut bien réfléchir à la structure de ces fichiers. Ici nous aurions pu tout autant créer de fichiers que de classe finalement. De même nous pouvons placer ces classes dans un répertoire différent (package). Mais il faudra bien importer les différents packages et différentes classes dans notre point d'entrée. VSCode est suffisamment "intelligent" pour aider le développeur à récupérer les noms des packages et classes.

[Code simple_ecommerce](./codes/simple_ecommerce/bin/simple_ecommerce.dart)

[Code gestion.dart](codes/simple_ecommerce/bin/gestion.dart)