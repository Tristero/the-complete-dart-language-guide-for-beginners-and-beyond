import 'dart:io';

class Product {
  final int id;
  final String name;
  final double price;

  const Product({
    required this.id,
    required this.name,
    required this.price,
  });

  @override
  String toString() => '($letter)${name.substring(1)}: $price €';

  // extraction de la première lettre du nom du produit
  String get letter => name[0];
}

class Item {
  final Product product;
  final int quantity;

  const Item({
    required this.product,
    this.quantity = 1, // par défaut pour l'utilisateur
  });

  double get price => product.price * quantity;

  @override
  String toString() => '$quantity x ${product.name}: $price €';
}

class Cart {
  final Map<int, Item> _items = {};

  void addProduct(Product p) {
    final item = _items[p.id];

    if (item == null) {
      _items[p.id] = Item(product: p, quantity: 1);
    } else {
      _items[p.id] = Item(product: p, quantity: item.quantity + 1);
    }
  }

  bool get isEmpty => _items.isEmpty;

  double total() => _items.values
      .map((item) => item.price)
      .reduce((value, element) => value + element);

  @override
  String toString() {
    if (_items.isEmpty) return '------\nPanier vide';

    final lst = _items.values.map((item) => item.toString()).join('\n');

    return '------\n$lst\n------\nTotal : ${total()} €\n------';
  }
}

bool paiement(Cart c) {
  if (c.isEmpty) {
    print('Le panier est vide');
    return false;
  }

  final total = c.total();
  print('Total : $total €');

  // paiement
  stdout.write('Paiement liquide : ');
  final input = stdin.readLineSync();
  if ((input == null) || (input.isEmpty)) {
    return false;
  }

  final valeur = double.tryParse(input);
  if (valeur == null) return false;
  if (valeur >= total) {
    final diff = valeur - total;
    print('Rendu au client = ${diff.toStringAsFixed(2)} €');
    return true;
  } else {
    print("Il manque de l'argent.");
    return false;
  }
}
