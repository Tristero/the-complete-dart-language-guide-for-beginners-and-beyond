import 'dart:io';

import 'gestion.dart';

const List<Product> catalogue = [
  Product(id: 1, name: 'pommes', price: 1.6),
  Product(id: 2, name: 'bananes', price: 0.7),
  Product(id: 3, name: 'courgettes', price: 1.0),
  Product(id: 4, name: 'raisin', price: 2),
  Product(id: 5, name: 'girolles', price: 0.8),
  Product(id: 6, name: 'pommes de terre', price: 1.5),
];

void main(List<String> arguments) {
  final panier = Cart();

  loop:
  while (true) {
    stdout.write(
        'Que souhaitez-vous faire ? (v)oir vos choix, (a)jouter un produit, (p)ayer :');

    final choixUtilisateur = stdin.readLineSync();

    switch (choixUtilisateur) {
      case 'a':
        final prod = choixProduit();
        if (prod != null) {
          panier.addProduct(prod);
          print(panier);
        }
        break;
      case 'v':
        print(panier);
        break;
      case 'p':
        if (paiement(panier)) {
          break loop;
        }
        break;
    }
  }
}

Product? choixProduit() {
  final lstProduits = catalogue.map((produit) => produit.toString()).join('\n');
  stdout.writeln('Liste des produits disponibles\n$lstProduits');
  stdout.write('Votre choix : ');
  final choix = stdin.readLineSync();

  for (var product in catalogue) {
    if (choix == product.letter) {
      return product;
    }
  }

  // si la lettre employée n'est pas trouvée
  print('Produit inconnu.');
  return null;
}
