import 'dart:io';
import 'dart:math';

Stream<int> compteurStream(int n) async* {
  for (var i = 1; i <= n; i++) {
    final r = Random();
    final delay = r.nextInt(5);
    sleep(Duration(seconds: delay));
    yield i;
  }
}

// calcul de la somme
Future<int> sum(Stream<int> s) async {
  return s.reduce((previous, element) => previous + element);
}

main(List<String> args) async {
  Stream<int> s = compteurStream(4);
  final rslt = await sum(s);
  print(rslt);
}
