Future<String> recuperationCommande() => Future.delayed(
      Duration(seconds: 2),
      () => 'Cappuccino',
    );

Future<String> recuperationCommande2() => Future.value('Espresso');

main() async {
  print("Démarrage du programme");
  try {
    final cmd = await recuperationCommande();
    print(cmd);

    final cmd2 = await recuperationCommande2();
    print(cmd2);
  } catch (e, st) {
    print(e);
    print(st);
  }
  print("Fin de programme");
}
