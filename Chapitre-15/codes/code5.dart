Future<String> recuperationCommande3() =>
    Future.error(Exception("Il n'y a plus de lait"), StackTrace.current);

main() async {
  print("Démarrage du programme");
  try {
    final cmd3 = await recuperationCommande3();
    print(cmd3);
  } catch (e, st) {
    print(e);
    print(st);
  }
  print("Fin de programme");
}
