Future<String> recuperationCommande() => Future.delayed(
      Duration(seconds: 2),
      () => 'Cappuccino',
    );

/*
Future<String> recuperationCommande() => Future.delayed(
      Duration(seconds: 2),
      () => throw Exception('Plus de lait en stock'),
    );
*/

main(List<String> args) {
  print("Démarrage du programme");
  recuperationCommande()
      .then((value) => print("Commande $value est prête !"))
      .catchError((error, stackTrace) => print('$error\n$stackTrace'))
      .whenComplete(() => print("Fin de programme"));
}
