void fizzBuzz() {
  for (var i = 0; i <= 15; i++) {
    if (i % 3 == 0 && i % 5 == 0) {
      print("fizz buzz");
    } else if (i % 3 == 0)
      print("fizz");
    else if (i % 5 == 0)
      print('buzz');
    else
      print(i);
  }
}

// Convertir cet algo en Stream  et les print en yield
// introduire aussi un délai de 500 millisec

Stream<String> fizzBuzzAsync(int n) async* {
  for (var i = 1; i <= n; i++) {
    await Future.delayed(Duration(milliseconds: 500));
    if (i % 3 == 0 && i % 5 == 0) {
      yield 'fizz buzz';
    } else if (i % 3 == 0)
      yield 'fizz';
    else if (i % 5 == 0)
      yield 'buzz';
    else
      yield "$i";
  }
}

// Dans main : itérer les valeurs du flux
main(List<String> args) async {
  await for (var item in fizzBuzzAsync(15)) {
    print(item);
  }
}
