Future<String> recuperationCommande() => Future.delayed(
      Duration(seconds: 2),
      () => throw Exception('Plus de lait en stock'),
    );

main() async {
  print("Démarrage du programme");
  try {
    final rslt = await recuperationCommande();
    print(rslt);
  } catch (e, st) {
    print(e);
    print(st);
  }
  print("Fin de programme");
}
