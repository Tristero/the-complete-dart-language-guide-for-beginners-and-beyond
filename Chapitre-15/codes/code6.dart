Future<int> sum(Stream<int> s) async {
  return s.reduce((previous, element) => previous + element);
}

Future<int> sumWithFor(Stream<int> s) async {
  int sum = 0;
  await for (var elem in s) {
    sum += elem;
  }
  return sum;
}

Future<void> main(List<String> args) async {
  final s = Stream<int>.fromIterable([1, 2, 3, 4]);

  int rslt = await sum(s);
  print(rslt);

  rslt = await sumWithFor(s);
  print(rslt);
}
