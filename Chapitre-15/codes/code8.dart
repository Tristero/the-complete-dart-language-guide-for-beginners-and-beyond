import 'dart:math';

Stream<int> compteurStream(int n) async* {
  for (var i = 1; i <= n; i++) {
    final r = Random();
    final delay = r.nextInt(5);
    // deux possibilités soit avec sleep
    //sleep(Duration(seconds: delay));
    // soit avec Future et await !
    await Future.delayed(Duration(seconds: delay));
    yield i;
  }
}

main(List<String> args) async {
  Stream<int> s = compteurStream(4);
  await for (var item in s) {
    print(item);
  }
}
