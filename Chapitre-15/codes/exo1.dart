Future<void> countdown(int n) async {
  for (int i = n; i >= 0; i--) {
    await Future.delayed(
      Duration(seconds: 1),
      () => print(i),
    );
  }
}

// les mots-clés async et await sont nécessaires pour
// que les événements se déroulent séquentiellement
main(List<String> args) async {
  await countdown(5);
  print("Done");
}
