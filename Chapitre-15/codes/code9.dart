main(List<String> args) async {
  final s = Stream.fromIterable([1, 2, 3]);

  // pour tester le code : il suffit de décommenter la partie à tester
/*
  final first = await s.first;
  print('Premier élément = $first');
*/

/*
  await s.forEach((element) {
    print(element);
  });
*/

  final carres =
      await s.map((event) => event * 2).where((event) => event % 2 == 0);
  print(carres); // affichera Instance of '_WhereStream<int>'
  await carres.forEach(print);
}
