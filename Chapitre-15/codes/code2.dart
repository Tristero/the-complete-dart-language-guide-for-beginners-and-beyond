Future<String> recuperationCommande() => Future.delayed(
      Duration(seconds: 2),
      () => 'Cappuccino',
    );

Future<void> main(List<String> args) async {
  print("Démarrage du programme");

  final rslt = await recuperationCommande();
  print(rslt);

  print("Fin de programme");
}
