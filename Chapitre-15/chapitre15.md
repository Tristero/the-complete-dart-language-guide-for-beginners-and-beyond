# Programmation asynchrone

## 0-Référence

L'article de référence sur la programmation asynchrone en Dart est publié sur le site officiel à l'adresse suivante : [https://dart.dev/tutorials/language/streams](https://dart.dev/tutorials/language/streams)

Ce type de programmation est essentiel pour de nombreux cas :

- récupérer des données sur le réseau
- écrire dans une base de données
- lire un fichier

Tout ceci afin de ne pas bloquer le programme, d'assurer la disponibilité du programme en attendant que les opérations asynchrones s'achèvent.

En Dart, nous disposons de ```Future``` (représentant une computation qui n'est pas entièrement complète, réalisée immédiatement) ainsi que les ```Stream```s (séquence d'événements asynchrones délivrés dans le temps). Les ```Stream```s sont utilisés dans le cadre le programmation dite *réactive* (utile pour modifier l'UI quand des données arrivent à l'application) et Flutter utilise ce type de programmation.

Trois grandes sections seront abordées :

- Les ```Future```s avec ```async``` et ```await```
- Les ```Stream```s avec ```async*``` et ```yield```
- Les méthodes permettant de manipuler les flux ou streams.

## 1-Future

### 1-Une première approche

Les ```Future```s représentent le résultat d'une opération asynchrone. Ces ```Future```s seront très utiles pour récupérer des données depuis le réseau, depuis une base de données... Les ```Future```s ont deux états : complet ou incomplet. Le ```Future``` est dans un état incomplet tant que les opérations ne se sont pas achevées. L'état complet disposera soit du résultat des opérations soit d'une erreur. Et dans le code il faudra veiller à bien gérer, en cas de complétude, aussi le succès que l'échec.

Utilisons un scénario de commande d'un café. Nous allons simuler un délai de 2 secondes entre le moment de la commande et la réalisation des opérations.

Exemple :

```dart
Future<String> recuperationCommande() => Future.delayed(
      Duration(seconds: 2),
      () => 'Cappuccino',
    );
```

Notre fonction va exécuter une opération (retourner une String) au bout de deux secondes. La méthode ```delayed``` est un constructeur factory. Cette fonction retourne non pas une String mais une Future ! Donc il faut que l'on gère ce type.

Comment utiliser cette fonction ?

Nous avons dit qu'il fallait gérer le cas de succès et celui d'échec. Pour cela, nous disposons de deux méthodes : ```then()``` qui prend une lambda et ```catchError()``` qui prend aussi une lambda :

- Avec ```then()``` nous allons récupérer la valeur String et la traiter
- Avec ```catchError()``` nous allons récupérer deux variables une pour l'erreur et l'autre pour la stack trace

Ce qui donne :

```dart
main(List<String> args) {
  print("Démarrage du programme");
  recuperationCommande()
      .then((value) => print("Commande $value est prête !"))
      .onError((error, stackTrace) => print(error));
}
```

Si l'on souhaite vérifier comment est gérée l'exception, il suffit de modifier notre fonction ```recuperationCommande()``` en levant une exception :

```dart
Future<String> recuperationCommande() => Future.delayed(
      Duration(seconds: 2),
      () => throw Exception('Plus de lait en stock'),
    );

main(List<String> args) {
  print("Démarrage du programme");
  recuperationCommande()
      .then((value) => print("Commande $value est prête !"))
      .catchError((error, stackTrace) => print('$error\n$stackTrace'));
}
```

Et comme c'est une opération asynchrone, Dart n'attend pas le résultat pour effectuer d'autres opérations. Si on rajoute en fin de programme un ```print("Fin de programme")``` celui-ci sera affiché **avant** le traitement de notre ```Future``` :

```dart
main(List<String> args) {
  print("Démarrage du programme");
  recuperationCommande()
      .then((value) => print("Commande $value est prête !"))
      .catchError((error, stackTrace) => print('$error\n$stackTrace'));

  print("Fin de programme");
}
```

Et le résultat est :

```txt
Démarrage du programme
Fin de programme
Commande Cappuccino est prête !
```

Pour gérer ceci, les ```Future```s disposent d'une méhode ```whenComplete()``` qui prend une lambda sans argument :

```dart
main(List<String> args) {
  print("Démarrage du programme");
  recuperationCommande()
      .then((value) => print("Commande $value est prête !"))
      .catchError((error, stackTrace) => print('$error\n$stackTrace'))
      .whenComplete(() => print("Fin de programme"));
}
```

Et le résultat devient "normal" :

```txt
Démarrage du programme
Commande Cappuccino est prête !
Fin de programme
```

Ce code n'est pas le plus pratique ni même recommandé pour gérer les ```Future```s

[Code](codes/code1.dart)

### 2-async et await

Il existe une alternative d'écriture à l'utilisation des ```Future```s. Si l'on souhaite gérer plusieurs ```Future```s séquentiellement, cette façon de gérer devient très difficile. Dans la méthode ```whenComplete()``` il faudra appeler une autre ```Future``` et gérer le cas succès le cas échec et ainsi de suite. Donc on risque d'avoir une imbrication d'appels très lourde à gérer, à maintenir.

#### 1-Syntaxe

Avec ```async/await```, on gère cette problématique.

Comme ```recuperationCommande()``` retourne une ```Future<String>```, on l'assigne à une variable mais en précédant l'appel de la fonction avec ```await```. Mais il faut **aussi** modifier la fonction qui utilise un type ```Future``` dans son code, en lui adjoignant le *modificateur* ```async```. Ici la fonction qui manipule un type ```Future``` c'est ```main``` :

```dart
main(List<String> args) async {
  print("Démarrage du programme");

  final rslt = await recuperationCommande();
}
```

**ATTENTION** une méthode/fonction qui est modifiée par la présence d'```async``` retourne un type ```Future```. Ici notre fonction ```main``` retourne non un void mais une ```Future<void>``` :

```dart
Future<void> main(List<String> args) async {
    // code omis
}
```

Maintenant le type de ```rslt``` n'est plus un ```Future<String>``` mais une ```String``` !

```dart
main(List<String> args) async {
  print("Démarrage du programme");

  final rslt = await recuperationCommande();
  print(rslt);

  print("Fin de programme");
}
```

Et on obtient un résultat :

```txt
Démarrage du programme
Cappuccino
Fin de programme
```

#### 2-Exceptions

Comment gérer les exceptions ?

Reprenons le code suivant :

```dart
Future<String> recuperationCommande() => Future.delayed(
      Duration(seconds: 2),
      () => throw Exception('Plus de lait en stock'),
    );
```

Nous obtenons l'erreur :

```txt
Démarrage du programme
Unhandled exception:
Exception: Plus de lait en stock
...
```

Nous pouvons dorénavant embarquer notre code dans un bloc ```try/catch[/finally]``` :

```dart
Future<void> main(List<String> args) async {
  print("Démarrage du programme");
  try {
    final rslt = await recuperationCommande();
    print(rslt);
  } catch (e, st) {
    print(e);
    print(st);
  }
  print("Fin de programme");
}
```

Et finalement notre code affiche non seulement l'erreur, la stack trace mais aussi notre message "Fin de programme" :

```txt
Démarrage du programme
Exception: Plus de lait en stock
...

Fin de programme
```

[Code](codes/code3.dart)

### 3-Future.value et Future.error

Le type ```Future``` dispose d'autres constructeurs : ```value```, ```error```, ```sync``` ou encore ```microtask```.

En plus de ```delayed```, ce seront préférentiellement les deux autres premiers constructeurs listés ci-dessus que l'on utilisera les plus fréquemment.

### 4-Future.value

On utilisera ce constructeur pour créer un futur complet avec une valeur (possiblement null) :

```dart
Future<String> recuperationCommande2() => Future.value('Espresso');
```

Ce constructeur sera utilisé si l'on souhaite que la ```Future``` soit immédiatement complète.

Si l'on teste le code suivant :

```dart
main() async {
  print("Démarrage du programme");
  try {
    final cmd = await recuperationCommande();
    print(cmd);
    final cmd2 = await recuperationCommande2();
    print(cmd2);
  } catch (e, st) {
    print(e);
    print(st);
  }
  print("Fin de programme");
}
```

Nous obtenons le résultat suivant :

```txt
Démarrage du programme
Cappuccino  <------- attendu après 2 secondes
Espresso    <------- aucune attente le résultat est retourné immédiatement
Fin de programme
```

[Code](codes/code4.dart)

### 5-Future.error

Ce constructeur prend au moins un argument : un type ```Exception```. Quant au second argument, il s'agit de la pile de traçage.

```dart
Future<String> recuperationCommande3() =>
    Future.error(Exception("Il n'y a plus de lait"), StackTrace.current);
```

Ce code retournera un message d'erreur ainsi qu'une stack trace.

[Code](codes/code5.dart)

### 6-Intérêt

Pourquoi de tels constructeurs ? Quel sont leur intérêt ?

Cela permet de modéliser une API, de pouvoir prévenir toute utilisation d'une API non encore implémentée avec ```UnimplementedError()``` par exemple.

De même, ces deux constructeurs seront trs pour écrire des tests pour tout code asynchrone.

### 7-Exemple : le décompte

Nous souhaitons créer une fonction countdown qui prend en paramètre un entier. A partir de cet entier on effectuera un décompte toutes les secondes. Dans le ```main```, on affichera, une fois le décompte achevé un message ```Done```.

Donc ici nous allons créer une fonction ```countdown``` qui sera ```async```. Elle retournera un type ```Future<void>``` :

```dart
Future<void> countdown(int n) async {}
```

Pour le décompte, nous utiliserons une boucle ```for``` traditionnelle dans laquelle on appellera la classe ```Future``` et son constructeur ```delayed```. Au bout d'une seconde on affichera la valeur de la variable de ```for```. Attention aux termes ```async``` et ```await``` car on doit attendre le résultat :

```dart
Future<void> countdown(int n) async {
  for (int i = n; i >= 0; i--) {
    await Future.delayed(
      Duration(seconds: 1),
      () => print(i),
    );
  }
}
```

Comment utiliser ce code ?

Si on appelle directement notre code :

```dart
main(List<String> args) async {
  countdown(5);
  print("Done");
}
```

On aura bien un appel du décompte avec son temps de latence mais on verra afficher en premier le message ```Done``` **puis** le décompte. Or nous voulons l'inverse : d'aobrd le décompte puis ```Done```. Donc à nouveau, nous devons préciser que ```main``` est ```async``` car nous souhaitons *attendre* (```await```) que ```countdown``` ait réalisé toutes ses opérations :

```dart
main(List<String> args) async {
  // on attend la fin 
  await countdown(5);
  print("Done");
}
```

[Code](codes/exo1.dart)

## 2-Streams

### 1-Définition et utilisation

Un stream est un flux d'événements asynchrones itérables.

Pour utiliser un ```Stream``` nous avons trois possibilités :

- ```Stream.fromIterable```
- Les générateurs de ```Stream```s avec ```async*``` et ```yield```
- ```StreamController```

#### 1-fromIterable

Ce constructeur demande en argument une structure itérable (e.g. listes) :

```dart
final s = Stream<int>.fromIterable([1, 2, 3, 4]);
```

Ceci nous retourne une instance de ```Stream<int>```.

#### 2-Utilisation du stream

Nous souhaitons effectuer la somme des entiers du flux. Et comme nous sommes avec un flux **asynchrone** il faut gérer ce traitement avec les ```Future```s. Notre fonction de sommation prendra en argument le flux et retournera un résultat asynchrone entier donc un ```Future<int>``` :

```dart
Future<int> sum(Stream<int> s) async {
  return s.reduce((previous, element) => previous + element);
}
```

**ATTENTION** à bien préciser que cette fonction est **```async```** !

Comme ```Stream``` est itérable on peut faire appel aux méthodes telles que ```reduce```.

#### 3-Problème avec for

Si on veut réécrire ce code avec une boucle ```for```, il va falloir préciser ```await``` :

```dart
Future<int> sum(Stream<int> s) async {
  var sum = 0;
  await for (var elem in s) {
    sum += elem;
  }
  return sum;
}
```

#### 4-Application

Son application est dorénavant très simple puisqu'elle suit ce que nous avons déjà vu :

```dart
Future<void> main() async {
  final s = Stream<int>.fromIterable([1, 2, 3, 4]);
  final rslt = await sum(s);
  print(rslt);
}
```

[Code](codes/code6.dart)

### 2-Les générateurs async* et yield

#### 1-Création du flux

Pour générer un flux de données nous nous devons de créer un type ```Stream```.

Dans les exemples précédents nous avons codé en dur un ```Iterable``` (une liste). Mais nous pouvons créer un *vrai* type ```Stream``` itérable.

Pour cela nous allons créer, une fonction qui retournera un type ```Stream<int>``` mais contrairement à précédemment où l'on prenait en argument un ```Stream```, ici nous utiliserons un entier.

Pour marquer l'asynchronicité nous devons indiquer que notre générateur de flux est asynchrone mais nous ne pouvons pas utiliser le mot-clé ```async``` mais **```async*```** :

```dart
Stream<int> compteurStream(int n) async* {}
```

Le résultat des opérations sera retourné par le mot-clé ```yield``` et non ```return``` ou ```await``` !

Nous souhaitons donc ici généré des nombres de 1 à n :

```dart
Stream<int> compteurStream(int n) async* {
  for (var i = 1; i <= n; i++) {
    yield i;
  }
}
```

Syntaxiquement,

- on retourne un ```Stream```
- le mot-clé pour signifier l'asynchronicité est ```async*``` (et non ```async```)
- pour générer un retour c'est ```yield``` (et non ```return``` ou ```await```)

```yield``` ajoute le résultat des opérations dans le flux.

#### 2-Utilisation du flux

Pour cela nous allons pouvoir réutiliser nos codes précédent pour calculer la somme :

```dart
Future<int> sum(Stream<int> s) async {
  return s.reduce((previous, element) => previous + element);
}
```

Et dans notre ```main```, on pourra réutiliser notre code sans problème (en n'oublia pas aucun ```async/await```)

```dart
main(List<String> args) async {
  Stream<int> s = compteurStream(4);
  final rslt = await sum(s);
  print(rslt);
}
```

[Code](codes/code7.dart)

Simulons un affichage avec des temps de latence différents. Pour cela nous avons deux possibilités :

- soit appeler ```sleep```

```dart
Stream<int> compteurStream(int n) async* {
  for (var i = 1; i <= n; i++) {
    final r = Random();
    final delay = r.nextInt(5);
    sleep(Duration(seconds: delay));
    yield i;
  }
}
```

- soit utiliser ```Future``` mais **attention à ne pas oublier ```await``` !** :

```dart
Stream<int> compteurStream(int n) async* {
  for (var i = 1; i <= n; i++) {
    final r = Random();
    final delay = r.nextInt(5);
    // deux possibilités soit avec sleep
    //sleep(Duration(seconds: delay));
    // soit avec Future et await !
    await Future.delayed(Duration(seconds: delay));
    yield i;
  }
}
```

[Code](codes/code8.dart)

[Autre exemple: FizzBuzz en stream](codes/exo2.dart)

Les ```Stream```s sont des ```Iterable```s avec pour seule distinction : les flux sont asynchrones et les ```Iterable```s sont synchrones.

### 3-Constructeurs

La classe ```Stream``` dispose de plusieurs constructeurs pur créer des flux.

#### 1-fromIterable()

Comme pour les ```Future```s on peut utiliser toute structure de type ```Iterable```.

#### 2-fromValue()

Contrairement au constructeur précédent, on n'utilisera qu'une seule valeur.

#### 3-error()

Comme avec ```Future```, nous pouvons passer une erreur dans le flux, à des fins soit de débogage, de test ou parce que l'API n'a pas été encore implémentée. On passera un type ```Exception(messageString)```. **Attentio** un ```Stream``` émet une erreur alors que ```Future``` lève une exception.

#### 4-empty()

Permet de générer un flux ... vide. Ce flux ne fait rien excepté l'envoi d'un événement achevé, fini quand le flux est écouté.

#### 5-fromFuture()

Permet d'utiliser un ```Future``` dans le flux. Par exemple :

```dart
Stream.fromFuture(Future.delayed(Duration(seconds: 1)), () => 42);
```

Dans ce dernier exemple, nous appliquons un délai d'une seconde, puis au bout de ce délai, une fonction anonyme est exécutée en retournant simplement 42.

#### 6-periodic()

Permet d'émettre des résultats selon un certain intervalle de temps. Le premier argument passé est une durée de type ```Duration``` et une fonction anonyme. Exemple :

```dart
Stream.periodic(
  Duration(seconds: 1),
  (index) => index
);
```

Le flux retournera toutes les secondes une valeur débutant à 0

#### 7-autres constructeurs

Il existe d'autres constructeurs mais ils ne sont pas utilisés massivement : on en trouvera liste complète dans la documentation [Dart](https://api.dart.dev/stable/2.13.4/dart-async/Stream-class.html#constructors)

## 3-Méthodes traitant les flux

### 1-Méthodes de flux

Il existe un grand nombre de méthodes qui peuvent traiter, opérer sur des flux. Donc toute instance de ```Stream``` dispose des méthodes suivantes :

```dart
Future<T> get first;
Future<bool> get isEmpty;
Future<T> get last;
Future<int> get length;
Future<T> get single;
Future<bool> any(bool Function(T element) test);
Future<bool> contains(Object? needle);
Future<E> drain<E>([E? futureValue]);
Future<T> elementAt(int index);
Future<bool> every(bool Function(T element) test);
Future<T> firstWhere(bool Function(T element) test, {T Function()? orElse});
Future<S> fold<S>(S initialValue, S Function(S previous, T element) combine);
Future forEach(void Function(T element) action);
Future<String> join([String separator = '']);
Future<T> lastWhere(bool Function(T element) test, {T Function()? orElse});
Future pipe(StreamConsumer<T> streamConsumer);
Future<T> reduce(T Function(T previous, T element) combine);
Future<T> singleWhere(bool Function(T element) test, {T Function()? orElse});
Future<List<T>> toList();
Future<Set<T>> toSet();
```

Et toutes ces méthodes retournent un type ```Future```.

### 2-Méthodes modifiant les flux

```dart
Stream<R> cast<R>();
Stream<S> expand<S>(Iterable<S> Function(T element) convert);
Stream<S> map<S>(S Function(T event) convert);
Stream<T> skip(int count);
Stream<T> skipWhile(bool Function(T element) test);
Stream<T> take(int count);
Stream<T> takeWhile(bool Function(T element) test);
Stream<T> where(bool Function(T event) test);
```

Ces méthodes retournent un nouveau flux et sont similaires aux méthodes de la classe ```Iterable``` : elles transforment un flux en un nouveau flux retourné.

### 3-Quelques exemples

À partir d'un simple flux :

```dart
final s = Stream.fromIterable([1, 2, 3]);
```

Nous allons pouvoir appliquer les différentes méthodes d'accès et de modifications du flux.

#### 1-first

**RAPPEL** : nous ne devons pas oublier qu'avec l'utilisation d'```await```, dans ```main```, nous devons utiliser le mot-clé ```async``` (et optionnellement, retourné le type ```Future<void>```).

```dart
main(List<String> args) async {
  final s = Stream.fromIterable([1, 2, 3]);

  final first = await s.first;
  print('Premier élément = $first');
}
```

Si nous devons faire appel à une autre méthode, nous aurons une exception :

```txt
Unhandled exception:
Bad state: Stream has already been listened to.
```

Le flux ayant été épuisé, nous ne pouvons plus le réemployé.

#### 2-forEach()

```dart
await s.forEach((element) {
  print(element);
}
```

**RAPPEL** ne pas oublier d'utiliser ```await``` avant l'appel de ```for```.

#### 3-map() et cumul de méthodes

Bien évidemment, nous pouvons aggréger des méthodes les unes à la suite des autres.

```ATTENTION``` : l'utilisation de ```map``` retourne un ```Stream<T>```. Donc il va falloir gérer ce problème

```dart
final carres = await s.map((event) => event * 2).where((event) => event % 2 == 0);
print(carres); // affichera Instance of '_WhereStream<int>'
await carres.forEach(print);
```

[Code](codes/code9.dart)

## 4-Souscriptions

Dans le sous-chapitre ["Quelques exemples"](3-quelques-exemples), nous avons rencontré un problème : une fois le flux parcouru ce dernier étant épuisé, vidé, nous ne pouvions plus l'utiliser. Ceci est dû à l'utilisation d'une double utilisation sur le même flux. En Dart, il y a deux types de flux :

- les flux à souscription unique (*single subscription*)
- les flux de diffusion (*broadcast streams*)

Et jusqu'ici nous n'avons utilisé que des flux à usage unique, à souscription unique.

De même toutes les méthodes vues jusqu'ici ont créé un ```StreamSubscription``` qui utilise un flux à souscription unique.

Pour aller plus loin sur la question :

- Codelab sur la programmation asynchrone avec ```async``` et ```await``` : [codelab](https://dart.dev/codelabs/async-await).
- Créer des ```Stream```s avec Dart : [article](https://dart.dev/articles/libraries/creating-streams).
