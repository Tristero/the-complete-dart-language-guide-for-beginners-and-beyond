class Point {
  final int X;
  final int Y;

  Point(this.X, this.Y);

  @override
  String toString() => "Point (x: $X, y: $Y)";

  @override
  bool operator ==(covariant Point p) => ((p.X == this.X) && (p.Y == this.Y));

  Point operator *(int i) {
    return Point(this.X * i, this.Y * i);
  }
}

main(List<String> args) {
  print(Point(0, 0) == Point(0, 0));

  // le code suivant n'est plus possible :
  //print(Point(0, 0) == 0);

  print(Point(2, 3) * 10);
}
