abstract class Forme {
  double get aire;
}

class Carre extends Forme {
  double long;
  Carre({required this.long});

  @override
  double get aire => this.long * this.long;
}

void PrintAire(Forme f) {
  print(f.aire);
}

main(List<String> args) {
  final carre = Carre(long: 12);
  print(carre.aire);

  // En utilisant notre fonction alternative
  PrintAire(carre);
}
