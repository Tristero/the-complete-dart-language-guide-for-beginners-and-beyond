import 'dart:math';

abstract class Shape {
  double get area;
  double get perimeter;

  void printValues() {
    print("Area = $area - Perimeter = $perimeter");
  }
}

class Square extends Shape {
  Square(this.side);
  final double side;

  @override
  double get area => side * side;

  @override
  double get perimeter => side * 4;
}

class Circle extends Shape {
  Circle(this.radius);
  final double radius;

  @override
  double get area => pi * (radius * radius);

  @override
  double get perimeter => 2 * pi * radius;
}

void main(List<String> args) {
  [Square(2), Square(3), Circle(2), Circle(3)]
      .forEach((elem) => elem.printValues());
}
