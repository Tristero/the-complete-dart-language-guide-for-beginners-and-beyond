class Point {
  final int X;
  final int Y;

  Point(this.X, this.Y);

  @override
  String toString() => "Point (x: $X, y: $Y)";

  @override
  bool operator ==(Object o) {
    if (o is Point) return ((o.X == this.X) && (o.Y == this.Y));
    return false;
  }
}

main(List<String> args) {
  print(Point(0, 0) == Point(0, 0));

  print(Point(0, 0) == 0);
}
