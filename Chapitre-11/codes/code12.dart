class Credentials {
  final String email;
  final String mdp;

  const Credentials({
    this.email = '',
    this.mdp = '',
  });

  Credentials copie({
    String? email,
    String? mdp,
  }) {
    return Credentials(email: email ?? this.email, mdp: mdp ?? this.mdp);
  }

  @override
  String toString() {
    return "Credentials($email, $mdp)";
  }
}

main(List<String> args) {
  final c = new Credentials(email: 'mon-mail@mail.com', mdp: '1234');

  print(c.toString());

  final copie = c.copie(mdp: 'azerty');

  print(copie.toString());
}
