class Mere {
  final int age;
  Mere({required this.age});

  void Print() {
    print(this.age);
  }
}

class Fille extends Mere {
  final String name;
  Fille({
    required int age,
    required String this.name,
  }) : super(age: age);

  @override
  void Print() {
    // affichage seulement du nom
    print(name);

    // appel du code de Print de la classe mère
    super.Print();

    // affichage customisé
    print("$name a $age ans.");
  }
}

main(List<String> args) {
  final f = Fille(
    age: 12,
    name: "Alice",
  );
  f.Print();
}
