class Stack<T> {
  final List<T> _lst = [];

  void push(T item) {
    _lst.add(item);
  }

  T pop() => _lst.removeLast();
}

main(List<String> args) {
  final stack = Stack<int>();

  stack.push(1);
  stack.push(2);
  stack.push(3);

  print(stack.pop());

  final stack2 = Stack<String>();

  stack2.push("un");
  stack2.push("deux");
  stack2.push("trois");

  print(stack2.pop());
}
