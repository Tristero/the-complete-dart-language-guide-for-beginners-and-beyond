class Point {
  final int X;
  final int Y;

  Point(this.X, this.Y);

  @override
  String toString() => "Point (x: $X, y: $Y)";

  @override
  bool operator ==(covariant Point p) => ((p.X == this.X) && (p.Y == this.Y));

  Point operator *(int i) {
    return Point(this.X * i, this.Y * i);
  }
}

class Chemin {
  List<Point> _points = [];

  void departDe(Point p) {
    _points = [p];
  }

  void traceLigneVers(Point p) {
    _points.add(p);
  }
}

main(List<String> args) {
  Chemin()
    ..departDe(Point(0, 0))
    ..traceLigneVers(Point(2, 0))
    ..traceLigneVers(Point(2, 2))
    ..traceLigneVers(Point(0, 2))
    ..traceLigneVers(Point(0, 0));
}
