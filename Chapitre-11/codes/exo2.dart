class Person {
  final String name;
  final int age;

  Person({
    required this.name,
    required this.age,
  });

  factory Person.fromJson(Map<String, Object> json) {
    final n = json['name'];
    final a = json['age'];
    if ((n is String) && (a is int)) {
      return Person(name: n, age: a);
    }
    throw StateError("Les valeurs name et age ont été incorrectement créées");
  }

  Map<String, Object> toJson() {
    return {'name': name, 'age': age};
  }
}

main(List<String> args) {
  final person = Person.fromJson({
    'name': 'Andrea',
    'age': 36,
  });

  // toJson convert to Map<String, Object>
  final json = person.toJson();
  print(json);
}
