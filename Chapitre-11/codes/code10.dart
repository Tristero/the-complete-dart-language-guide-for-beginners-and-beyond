import 'dart:math';

abstract class Forme {
  const Forme();

  double get aire;

  factory Forme.fromJson(Map<String, Object> json) {
    final type = json['type'];
    switch (type) {
      case 'carre':
        final long = json['cote'];
        // cast et vérification que long est double
        if (long is double) {
          return Carre(long: long);
        }
        // sinon il faut retourner quelque chose
        // on ne peut pas retourner une Forme
        // ni un null donc la seule possibilité
        // et de lever une erreur
        // ce sera étudié plus tard
        throw UnsupportedError("propriété cote est manquante ou non valide");
      case 'cercle':
        final rayon = json['rayon'];
        if (rayon is double) {
          return Cercle(rayon: rayon);
        }
        throw UnsupportedError("Le rayon est manquant ou non-valide");
      default:
        throw UnimplementedError("Forme inconnue");
    }
  }
}

class Carre extends Forme {
  double long;
  Carre({required this.long});

  @override
  double get aire => this.long * this.long;
}

class Cercle extends Forme {
  double rayon;
  Cercle({required this.rayon});

  @override
  double get aire => pi * (rayon * rayon);
}

void PrintAire(Forme f) {
  print(f.aire);
}

main(List<String> args) {
  final formes = [
    {
      'type': 'carre',
      'cote': 10.0,
    },
    {
      'type': 'cercle',
      'rayon': 10.0,
    },
    {
      'type': 'triangle',
    }
  ];

  // affichage d'une seule valeur
  final f = Forme.fromJson(formes[0]);
  PrintAire(f);

  // affichage de tous les calculs
  formes.map((forme) => Forme.fromJson(forme)).forEach(PrintAire);
}
