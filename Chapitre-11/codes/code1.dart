class Mere {
  final int age;
  Mere({required this.age});

  void Print() {
    print(this.age);
  }
}

class Fille extends Mere {
  final String name;
  Fille({
    required int age,
    required String this.name,
  }) : super(age: age);
}

main(List<String> args) {
  final f = Fille(
    age: 12,
    name: "Alice",
  );
  f.Print();
}
