abstract class Widget {}

class Text extends Widget {
  final String text;
  Text(this.text);
}

class Button extends Widget {
  final Widget child;
  final void Function()? onPressed;

  Button({
    required this.child,
    this.onPressed,
  });
}

main(List<String> args) {
  String txt = "hello, world";
  final btn = Button(
    child: Text(txt),
    onPressed: () => print(txt),
  );
  btn.onPressed?.call();
}
