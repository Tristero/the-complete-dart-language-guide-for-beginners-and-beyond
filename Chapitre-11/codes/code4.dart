class Point {
  final int X;
  final int Y;

  Point(this.X, this.Y);

  @override
  String toString() => "Point (x: $X, y: $Y)";
}

main(List<String> args) {
  print(Point(12, 24));
}
