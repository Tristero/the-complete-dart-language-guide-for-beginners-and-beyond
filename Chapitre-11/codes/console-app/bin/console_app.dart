import 'package:equatable/equatable.dart';

class Point extends Equatable {
  final int X;
  final int Y;

  const Point(this.X, this.Y);

  @override
  List<Object?> get props => [X, Y];

  @override
  bool? get stringify => true;
}

void main(List<String> arguments) {
  print(Point(1, 2));
  print(Point(12, 42) == Point(12, 42));
}
