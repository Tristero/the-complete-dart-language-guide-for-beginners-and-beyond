# POO : notions avancées

Ce qui va être couvert dans ce chapitre :

- héritage
- constructeur et super
- surcharge de méthode
- classes abstraites
- ```implements``` et ```extends```
- classe ```Object```
- méthodes ```ToString()```, ```hashCode``` et l'opérateur ```==```
- le package ```equatable```
- classes et génériques
- composition vs héritage
- constructeurs Factory
- copie d'objet avec ```copyWith```
- l'opérateur cascade ```..```

## 1-Héritage

Une classe fille peut hériter d'une classe mère en utilisant le mot-clé ```extends```.

Syntaxe :

```dart
class Mere {
    // code
}

class Fille extends Mere {
    // code
} 
```

La classe fille accèdera aux méthodes de la classe mère. Et l'inverse n'est, bien évidemment, pas possible.

L'héritage est **simple** (comme en Java). Pour simuler un héritage multiple il faudra recourir aux interfaces.

L'héritage est **multiniveau** : on peut créer une hiérarchie descendante et toutes les sous-classes de sous-classes hériteront des différents membres supérieurs. Une sous-classe C héritant d'une classe B qui hérite d'une super-classe A pourra accéder aux membres de **B et de A**

La composition est une alternative plus intéressante que l'héritage. Cf. chapitre composition.

## 2-super

Si une classe mère dispose d'un constructeur, par exemple, initialisant une variable, les classes filles devront implémenter aussi un constructeur en appelant ```super()``` dans leur propre constructeur.

Exemple :

```dart
class Mere {
    final int age;
    Mere({required this.age});

    print(age);
}

class Fille {
    Fille({required int age}) : super(age: age);
}
```

Rien n'empêche d'ajouter d'autres arguments au constructeur des classes-filles :

```dart
class Fille extends Mere {
  final String name;
  Fille({
    required int age,
    required String this.name,
  }) : super(age: age);
}
```

[Code](codes/code1.dart)

## 3-Surcharge de méthode

Permet la redéfinition d'une méthode de la classe-mère dans la classe fille.

Pour redéfinir une méthode, il faut **annoter** la méthode dans la classe-fille avec ```@override```.

Syntaxe :

```dart
class Mere {
    void Methode() {
        //code
    }
}

class Fille extends Mere {
    @override
    void Methode() {
        // code propre à la classe fille
    }
}
```

Parfois il peut être intéressant de réutiliser le code de la méthode de la classe mère dans la méthode surchargée de la classe fille : il faudra pour cela utiliser ```super``` qui pointe vers la classe mère.

Syntaxe

```dart
class Mere {
    void Methode() {
        //code
    }
}

class Fille extends Mere {
    @override
    void Methode() {
        // appel de la méthode la classe mère
        super.Methode()
        // code propre à la classe fille
    }
}
```

[Code](codes/code2.dart)

## 4-Classes abstraites

### 1-extends

Ne peuvent pas être instanciées. On utilise le mot-clé ```abstract```. La classe dite **concrète** qui implémente la classe abstraite fera appel à ```extends``` :

```dart
abstract class ClasseAbstraite {
    //code
}

class ClasseConcrete extends ClasseAbstraite {
    // code
}
```

Le principe est le même qu'en Java, on peut définir :

- une signature de méthode
- une méthode complète

Pour implémenter des méthodes abstraites, il faut recourir à ```@override``` dans la classe concrète.

Exemple :

```dart
abstract class Forme {
  double get aire;
}

class Carre extends Forme {
  double long;
  Carre({required this.long});

  @override
  double get aire => this.long * this.long;
}

main(List<String> args) {
  final carre = Carre(long: 12);
  print(carre.aire);
}
```

L'avantage est l'abstraction : on peut utiliser une classe abstraite comme n'importe quel autre type.

Exemple :

```dart
void PrintAire(Forme f) {
  print(f.aire);
}

main(List<String> args) {
  final carre = Carre(long: 12);
  PrintAire(carre);
}
```

[Code](codes/code3.dart)

Une classe abstraite peut très bien implémenter ses propres membres avec son code propre :

```dart
abstract class Shape {
  double get area;
  double get perimeter;

  void printValues() {
    print("Area = $area - Perimeter = $perimeter");
  }
}
```

Ici la méthode ```printValues()``` peut être appelée depuis ses classes concrètes.

[Code](codes/exo1.dart)

### 2-implements

Une alternative à l'appel de ```extends``` et d'utiliser ```implements```.

```dart
class Carre implements Shape {
  ...
}
```

Avec ```implements```, et contrairement à ```extends```, on peut utiliser plusieurs classes abstraites avec une seule classe appelante :

```dart
abstract class ClasseAbstraite1 {
  void a();
}

abstract class ClasseAbstraite2 {
  void b();
}

class ClasseConcrete implements ClasseAbstraite1, ClasseAbstraite2 {
  @override
  void a() {
    // code
  }

  @override
  void b() {
    // code
  }
}
```

### 3-extends vs implements

Avec ```extends```, on ne peut appeler qu'une classe mais toute méthode ayant une implémentation (méthode concrète) dans la classe abstraite n'a pas à être réécrite, sauf si besoin est.

Avec ```implements```, une classe abstraite disposant d'une méthode avec une implémentation (méthode concrète) devra être réécrite ! Il y a **redéfinition de toutes les méthodes (concrètes ET abstraites) de la classe abstraite**.

## 5-Interfaces

Avec la plupart des langages objets ou manipulant des interfaces et contrairement aux classes abstraites, une classe peut plusieurs interfaces. Pour cela, on utilise le mot-clé *interface*. Mais avec Dart, **il n'y a pas de structure, de type propre à l'interface**, comme on peut voir avec Java ou C#. **Les interfaces en Dart sont des classes abstraites**.

Donc pour utiliser des interfaces, il suffit d'appeler ```abstract class```.

Les classes concrètes ayant besoin d'une interface utiliseront ```implements``` et devront implémenter **toutes** les méthodes quelles soient concrètes ou abstraites.

[Revoir la partie ```implements```](2-implements)

## 6-La classe Object?

Toute classe Dart dérive de la super-classe ```Object?```. De celle-ci dérivent deux classes, ```Object``` et ```Null```. Et le type le plus bas dans la hiérarchie est ```Never```.

Lorsque l'on crée une classe, nous n'utilisons jamais l'appel d'héritage ```extends Object``` ou  ```extends Object?```.

Même sans cet appel, on a accès aux méthodes de la super-classe ```Object?``` :

- ```hashCode``` (propriété)
- ```runtimeType``` (propriété)
- ```noSuchMethod(...)``` (méthode)
- ```toString()``` (méthode)

```dart
class Truc {
  void machin(){
    // on peut appeler la méthode toString() de la super-classe Object?
    super.toString()

  }
}
```

### 1-Méthode toString()

Cette méthode est très utile pour le débogage.

Surcharger cette méthode permet de préciser comme créer une représentation sous forme de chaîne de sa propre classe.

Les types pré-établis avec Dart disposent de leur propre implémentation de ```toString()```.

Redéfinition de toString() :

```dart
class Truc {

  // code

  @override
  String toString() {
    // ré-implémentation de la méthode
  }
}
```

[Code](codes/code4.dart)

### 2-Opérateur d'égalité

Objet : chercher à savoir si deux objets sont égaux.

Si le code ```print(5 == 5)``` retournera ```true```, le code suivant retournera ```false``` :

```dart
class Point {
  int X;
  int Y;

  Point(this.X, this.Y);

}

main(List<String> args) {
  print(Point(0, 0) == Point(0, 0));
}
```

Il faut redéfinir l'opérateur d'égalité. Dart permet la redéfinition des opérateurs et notamment d'égalité, selon la syntaxe suivante :

```dart
@override
bool operator ==(Object autreObjet) {
  // réimplémentation
}
```

Ici l'argument **sera TOUJOURS Object** car on surcharge la méthode d'origine donc le cast sera de rigueur.

Dans un premier temps, nous allons vérifier que le type d'```autreObjet``` est un ```Point``` à l'aide de l'opérateur spécial ```is```. Puis on effectue la comparaison sur les valeurs désirées. Si le type de ```autreObjet``` n'est pas le même que celui de la classe alors on retourne ```false``` :

```dart
class Point {
  int X;
  int Y;

  Point(this.X, this.Y);

  @override
  bool operator ==(Object o) {
    if (o is Point) return ((o.X == X) && (o.Y == Y));
    return false;
  }
}
```

[Code](codes/code5.dart)

#### Remarque sur l'opérateur ```is```

Cet opérateur permet en cas de succès de caster le type Object vers le type attendu. Il est **promu** au moment de l'exécution et non à la compilation.

#### Alternative ```covariant```

Pour éviter la promotion, le mot-clé ```covariant``` précédant le nom et le type de l'argument de l'opérateur ```==``` permet d'éviter tout recours au super-type ```Object``` :

```dart
class Classe {
  
  // code
  
  @override
  bool operator ==(covariant Classe   autreClasse) {
    // réimplémentation
  }
}
```

Reprise de l'exemple précédent :

```dart
class Point {
  int X;
  int Y;

  Point(this.X, this.Y);

  @override
  bool operator ==(covariant Point p) => ((p.X == X) && (p.Y == Y));
}
```

[Code](codes/code6.dart)

### 3-Redéfinition des opérateurs

Bien évidemment, la surcharge peut s'appliquer à d'autres opérateurs mathématiques tels que "+", le "*", etc. Même avec des types différents. Mais **attention, il ne s'agit pas surcharge**  : donc pas d'annotation avec ```@override``` puisque ces opérateurs ne sont pas implémentés dans la classe ```Object``` !

```dart
class Point {
  int X;
  int Y;

  Point(this.X, this.Y);

  @override
  bool operator ==(covariant Point p) => ((p.X == this.X) && (p.Y == this.Y));

  Point operator *(int i) {
    return Point(this.X * i, this.Y * i);
  }
}
```

[Code](codes/code7.dart)

### 4-hashCode et l'import de nouveaux packages

#### 1-hashCode et l'opérateur ==

```hashCode``` est un entier qui représente l'état d'un objet. Et cet état est utilisé pour les comparaisons. Deux objets sont dits *égaux* si et seulement leurs hashCodes respectifs est le même.

Cela signifie que lors de la redéfinition de l'opérateur ```==```, il convient de redéfinir ```hashCode``` dans le même temps.

Le problème est de trouver une méthode pour générer cet entier. Il existe des packages tels que ```quiver``` ou ```equitable``` proposant leur propre solution.

#### 2-Importation de packages

Il existe un dépôt centralisé pour les packages indépendants de la bibliothèque standard. Pour cela, il convient de connaître le site centralisant ces packages : [pub.dev](https://pub.dev/) et dans la barre de recherche il suffit de taper ```equatable``` et de cliquer sur le premier lien (redirigeant vers la  page [https://pub.dev/packages/equatable](https://pub.dev/packages/equatable)). Puis on clique sur l'onglet ```installing``` ([URL](https://pub.dev/packages/equatable/install)).

```shell
$ dart pub add equatable
```

Mais AVANT de pouvoir utiliser la ligne de commande, il faut créer un projet Dart :

```sh
dart create ./mon-projet
```

Dart va créer le répertoire et un ensemble de fichier. Il faudra se déplacer dans le répertoire ```./mon-projet/bin/``` pour trouver le code source. Et pour installer des package avec la commande ```dart pub add```, il faut se situer à la racine du projet soit, pour notre cas, ```mon-projet```. Cette commande nous évite de modifier le fichier ```pubspec.yaml``` : elle le fera à notre place.

Dans le fichier ```pubspec.yaml```, on trouvera l'ajout de notre nouvelle dépendance :

```yaml
dependencies: 
  equatable: ^2.0.3
```

#### 3-Utilisation d'equatable

Notre classe ```Point``` doit hériter de la classe  ```Equatable``` et donc nous devons importer comme suit le package :

```dart
import 'package:equatable/equatable.dart';

class Point extends Equatable {
  //Code
}
```

A noter que VSCode se chargera pour nous de faire les écritures d'importation.

Il nous faut corriger une erreur :

```txt
Missing concrete implementation of 'getter Equatable.props'.
Try implementing the missing method, or make the class abstract
```

Que nous est-il demandé ? nous devons implémenter ceci :

```dart
  @override
  List<Object?> get props => throw UnimplementedError();
```

Avec VSCode, il suffit de taper ```props``` et Intellisense nous proposera un code snippet ```props=>...``` qu'il faudra sélectionner. Le code créé nous dit que l'accesseur ```props``` est une liste... mais de quoi ? en fait, il s'agit de passer une liste des **propriétés de la classe** : ici nos coordonnées X et Y. Donc nous allons implémenter le code suivant :

```dart
  @override
  List<Object?> get props => [X, Y];
```

Et nous pouvons supprimer la redéfinition de hashCode car c'est Equatable qui s'en charge pour nous. Plus fort, nous n'avons plus à redéfinir l'opérateur ```==```, ```equatable``` s'en charge pour nous. Quant à ```toString()```, ```equatable``` nous propose un mutateur ```stringify``` qui attend un booléen pour autoriser un affichage par défaut, à notre place :

```dart
class Point extends Equatable {
  final int X;
  final int Y;

  const Point(this.X, this.Y);
  
  @override
  List<Object?> get props => [X, Y];

  @override
  bool? get stringify => true;
}
```

[Code](codes/console-app/bin/console_app.dart)

#### 4-Avantage et résumé

Cela deviendra très intéressant lorsque l'on devra gérer plusieurs classes avec la redéfinition de l'opérateur ```==``` avec ```hashCode``` et ```toString()```

Le package ```equatable``` permet de résoudre ces problèmes en 4 étapes

- appel de ```dart pud add equatable```
- ajout à la classe visée par la redéfinition de ```extends Equatable``` (VSCode se chargeant de l'import)
- redéfinir les getters ```props```
- redéfinir ```stringify``` si l'on souhaite une redéfinition de ```toString()``` à notre place

Très important : la classe ```Equatable``` ne peut s'utiliser que si la classe héritante est non-mutable ou *immutable* : cela signifie que les propriétés de la classe héritantes doivent être ```final``` ! C'est bien le cas ici puisque nos propriétés ```X``` et ```Y``` sont ```final```.

## 7-Classes et génériques

Pour présenter l'utilisation des génériques avec des classes, nous allons implémenter une pile (la structure de données dénommée aussi *stack*).

Une pile est une structure de type LIFO (Last In First Out) : dernier arrivé, premier servi. Une pile dispose de méthodes telles que ```push()``` qui placera un élément sur le reste de la pile et ```pop()``` qui supprimera l'élément le plus haut de la pile (autrement dit le dernier élément stocké sur notre pile).

Nous utiliserons une liste comme structure interne.

Cette pile prendra en charge un type et un seul. Pour palier à ce problème, on utilisera donc les génériques. La signature de la classe ressemblera à :

```dart
class Stack<T> {}
```

Puis on crée une propriété ```final``` pour notre liste de stockage qui sera aussi privée.

La méthode ```push()``` sera très simple : il suffit d'appeler la méthode ```add``` sur notre liste privée. L'item a ajouté sera passé en argument.

La méthode ``` pop()``` ne prend aucun argument mais retournera l'élément retiré de la liste.

Code :

```dart
class Stack<T> {
  final List<T> _lst = [];

  void push(T item) {
    _lst.add(item);
  }

  T pop() => _lst.removeLast();
}
```

Nous pouvons maintenant utiliser cette classe avec n'importe quel type :

```dart
final s1 = Stack<int>();
final s2 = Stack<String>();
final s3 = Stack<Point>();
```

[Code complet avec tests](codes/code8.dart)

## 8-Composition vs héritage

Le cas de notre pile est un représentant du pattern de la composition. Flutter utilise massivement la composition et héritage. D'une manière générale, il convient de privilégier la composition à l'héritage.

Un exemple : nous allons recréer un bouton.

Tout d'abord nous créons une classe abstraite très simple qui englobera tous les widgets :

```dart
abstract class Widget {}
```

Puis nous allons créer deux widgets : un pour du texte et un pour représenter un bouton.

```dart
class Text extends Widget {
  final String text;
  Text(this.text);
}
```

Dans ce premier exemple, nous faisons hériter ```Text``` de ```Widget```. Nulle composition ici.

En revanche pour le ```Button``` nous allons utiliser la composition pour intégrer un widget enfant (ici ce sera du texte) et c'est là qu'intervient la composition :

```dart
class Button extends Widget {
  final Widget child;
  final void Function()? onPressed;

  Button({
    required this.child,
    this.onPressed,
  });
}
```

Nous laissons le soin aux développeurs de créer leur événement propre ```onPressed``` ainsi que le widget-enfant. La signature indique une fonction *nullable* ne prenant aucun argument et ne retournant rien.

Voici comment le tout fonctionne :

```dart
main(List<String> args) {
  String txt = "hello, world";
  final btn = Button(
    child: Text(txt),
    onPressed: () => print(txt),
  );
  // appel si onPressed est non-null
  btn.onPressed?.call();
}
```

La composition fait que le widget ```Button``` ne sait pas à quoi ressemble la propriété ```child```, hormis que son type est ```Widget```. Grâce à ce système, il devient aisé de créer des interfaces très complexes.

L'héritage est une relation dite "is-a" : ici ```Button``` et ```Text``` **sont** des ```Widget```s. La composition est une relation dite "has-a" : ici Button **a** un propriété de type ```Widget```.

Si l'on reprend l'exemple de la pile : Stack **a** une liste mais n'hérite pas de ```List``` dont Stack **n'est pas** un type ```List```.

## 7-Factory constructor

Ce type de constructeur repose sur le pattern Factory.

Il est particulièrement utilisé pour le traitement des données au format JSON.

Nous avons vu deux types de constructeurs dans le [chapitre 10](../Chapitre-10/chapitre10.md#3-constructeurs)

- les constructeurs par défaut
- les constructeurs nommés ou *named constructors*

Les constructeurs Factory sont utiles dans deux cas :

- pour implémenter un constructeur qui n'a pas besoin de toujours créer une instance de sa classe
- pour initialiser une variable ```final``` utilisant d'abord une logique puis initialisé (un constructeur traditionnel doit être initialisé immédiatement).

Et le cas le plus patent est celui où l'on effectue une requête distante auprès d'une API et en retour on attend une réponse au format JSON.

Pour notre exemple nous allons reprendre nos classes ```Forme```, ```Carre``` et ```Cercle``` :

```dart
abstract class Forme {
  double get aire;
}

class Carre extends Forme {
  double long;
  Carre({required this.long});

  @override
  double get aire => this.long * this.long;
}

class Cercle extends Forme {
  double rayon;
  Cercle({required this.rayon});

  @override
  double get aire => pi * (rayon * rayon);
}

void PrintAire(Forme f) {
  print(f.aire);
}
```

Nous allons créer une Map comme suit :

```dart
main(List<String> args) {
  final formes = [
    {
      'type': 'carre',
      'cote': 10.0,
    },
    {
      'type': 'cercle',
      'rayon': 10.0,
    },
  ];
}
```

Pour créer un constructeur Factory, Dart nous met à disposition le mot-clé : ```factory``` précédent le nom du constructeur (le nom de la classe). Et comme nous devons traiter du JSON, nous allons l'appeler ```fromJson()```. Où placer ce constructeur ? dans la classe abstraite :

```dart
abstract class Forme {
  double get aire;

  factory Forme.fromJson();
}
```

Mais ce code doit être précisé. Tout d'abord, il doit prendre un argument notre JSON : si l'on analyse on a affaire à un type ```Map<String, Object>```. Pourquoi ne pas utiliser ```dynamic``` ? c'est totalement valable mais on ne profitera pas du système de typage  :

```dart
abstract class Forme {
  double get aire;

  factory Forme.fromJson(Map<String, Object> json) {};
}
```

Nous devons rédiger le corps de la fonction.

En fonction de la valeur de ```type``` présent dans le JSON, on initialisera/retournera un type ```Carre``` ou ```Cercle``` et on lui transmettra la bonne valeur :

```dart
abstract class Forme {
  double get aire;

  factory Forme.fromJson(Map<String, Object> json) {
    final type = json['type'];
    switch (type) {
      case 'carre':
        final long = json['cote'];
        // cast et vérification que long est double
        if (long is double) {
          return Carre(long: long);
        }
        // sinon il faut retourner quelque chose
        // on ne peut pas retourner une Forme
        // ni un null donc la seule possibilité
        // et de lever une erreur
        // ce sera étudié plus tard
        throw UnsupportedError("propriété cote est manquante ou non valide");
      case 'cercle':
        final rayon = json['rayon'];
        if (rayon is double) {
          return Cercle(rayon: rayon);
        }
        throw UnsupportedError("Le rayon est manquant ou non-valide");
      default:
        throw UnimplementedError("Forme inconnue");
    }
  }
}
```

Quelques précisions :

- nous devons vérifier si les propriétés sont bien des types ```double```
- si c'est le cas alors on crée une instance de ```Cercle``` ou de ```Carre```
- sinon soit nous retournons une Forme (ce qui n'est pas possible puisque notre classe ne peut pas lire la valeur de ```json['cote']```) soit un null (impossible car le factory **doit** retourner un type ```Forme```) donc il ne nous reste qu'une exception à lever !

Notre code n'est TOUJOURS PAS valide ! Les classes ```Cercle``` et ```Carre``` ne peuvent pas *étendre* la classe abstraite ```Forme```. La cause vient de la création d'un constructeur Factory. Le message d'erreur de Dart n'est pas clair. Mais cela signifie que l'on n'a pas créé de constructeur pour la classe abstraite.

Par défaut, lorsqu'une classe est créée sans constructeur, Dart en crée une implicitement sans argument. Mais si nous créons un constructeur explicitement, Dart ne génère plus de constructeur implicite.

Or c'est cela que pointe l'erreur : nous n'avons pas crée de constructeur explicite pour la classe abstraite.

Dans notre cas, il n'y a pas de gestion de variables mutables, on créer un constructeur vide constant :

```dart
abstract class Forme {
  // nécessité de créer un constructeur explicite
  const Forme();

  double get aire;

  factory Forme.fromJson(Map<String, Object> json) {
    ...
  }
}
```

On peut tester notre code en rajoutant une forme qui n'a pas été créée : le triangle.

```dart
main(List<String> args) {
  final formes = [
    {
      'type': 'carre',
      'cote': 10.0,
    },
    {
      'type': 'cercle',
      'rayon': 10.0,
    },
    {
      'type': 'triangle',
    }
  ];

  final f = Forme.fromJson(formes[0]);
  PrintAire(f);

  formes.map((forme) => Forme.fromJson(forme)).forEach(PrintAire);
```

[Code](codes/code10.dart)

### Résumé : différences entre les constructeurs et le constructeur factory

Un constructeur Factory retourne une instance d'une sous-classe. Alors qu'un constructeur traditionnel retourne une instance de sa classe.

Avec un factory, nous pouvons exécuter du code logique en premier puis retourner une instance. Avec un constructeur traditionnel, toutes les variables ```final``` doivent être initialisées immédiatement

Enfin il existe une solution moins difficile à manipuler : remplacer le constructeur Factory par une méthode statique. Ici notre méthode statique prendra en argument le type de Map et retournera une ```Forme``` :

```dart
abstract class Forme {
  double get aire;

  static Forme fromJson(Map<String, Object> json) {
    final type = json['type'];
    switch (type) {
      case 'carre':
        final long = json['cote'];
        // cast et vérification que long est double
        if (long is double) {
          return Carre(long: long);
        }
        // sinon il faut retourner quelque chose
        // on ne peut pas retourner une Forme
        // ni un null donc la seule possibilité
        // et de lever une erreur
        // ce sera étudié plus tard
        throw UnsupportedError("propriété cote est manquante ou non valide");
      case 'cercle':
        final rayon = json['rayon'];
        if (rayon is double) {
          return Cercle(rayon: rayon);
        }
        throw UnsupportedError("Le rayon est manquant ou non-valide");
      default:
        throw UnimplementedError("Forme inconnue");
    }
  }
}
```

Observons que le code n'a que très peu changé : la signature de la méthode, et la suppression du constructeur de ```Forme```.

Mais le résultat est le même.

[Code](codes/code11.dart)

[Autre exemple](codes/exo2.dart)

**Par convention, il convient de recourir à ```factory``` pour intialiser des instances de classe.**

Pour aller plus loin : [article](https://dash-overflow.net/articles/factory/) sur la différence entre le constructeur Factory et la méthode statique.

## 8-Copie d'objets

La plupart du temps il est bien plus pertinent de créer des classes non-mutables avec des variables final afin de s'assurer un comportement cohérent.

Mais dans la réalité, les choses sont plus complexes et la nécessité d'un changement d'état peut s'avérer essentiel.

Dans ce cas, il est possible de créer une classe non-mutable tout en simulant une modification : il suffit de créer une méthode d'instance retournant une nouvelle instance avec une copie partielle ou complète des propriétés (suivant ce que l'on veut modifier).

Partons d'une classe :

```dart
class Credentials {
  final String email;
  final String mdp;
  
  const Credentials({
    this.email = '',
    this.mdp = '',
  });
}
```

Maintenant si nous créons une instance de cette classe, il nous est interdit de modifier les propriétés. Le code suivant est interdit : 

```dart
final c = Credentials()
c.email = 'truc@bidule.com';
```

Pour palier à ce problème, **sans** modifier la structure non-mutable de la classe, nous allons lui adjoindre une méthode :

```dart
Credentials copie({
  String? email,
  String? mdp,
}) {
  return Credentials(email: email ?? this.email, mdp: mdp ?? this.mdp);
}
```

Cette méthode retourne une nouvelle instance en permettant à l'utilisateur d'affecter de nouvelles valeurs en recréant une nouvelle instance de façon implicite :

```dart
final c = new Credentials(email: 'mon-mail@mail.com', mdp: '1234');

final copie = c.copie(mdp: 'azerty');
```

[Code](codes/code12.dart)

## 9-Opérateur cascade

Cet opérateur agit sur des classes avec états mutables. Cet opérateur est le double point ```..``` : il permet de chaîner les appels de méthodes sur une instance.

Pour notre exemple, reprenons notre classe Point :

```dart
class Point {
  final int X;
  final int Y;

  Point(this.X, this.Y);

  @override
  String toString() => "Point (x: $X, y: $Y)";

  @override
  bool operator ==(covariant Point p) => ((p.X == this.X) && (p.Y == this.Y));

  Point operator *(int i) {
    return Point(this.X * i, this.Y * i);
  }
}
```

Et créons une classe nous permettant de créer un chemin sous forme d'une liste de type ```Point```s.

```dart
class Chemin {
  List<Point> _points = [];

  void departDe(Point p) {
    _points = [p];
  }

  void traceLigneVers(Point p) {
    _points.add(p);
  }
}
```

Pour tracer un carré, on va créer une instance de ```Chemin``` et appeler une fois la méthode ```departDe``` et **quatre fois** la méthode ```traceLigneVers```. Au lieu d'écrire le code comme suit :

```dart
final c = Chemin();
c.departDe(Point(0, 0));
c.traceLigneVers(Point(2, 0));
c.traceLigneVers(Point(2, 2));
c.traceLigneVers(Point(0, 2));
c.traceLigneVers(Point(0, 0));
```

nous pourrons utiliser l'opérateur cascade :

```dart
final c = Chemin()
  ..departDe(Point(0, 0))
  ..traceLigneVers(Point(2, 0))
  ..traceLigneVers(Point(2, 2))
  ..traceLigneVers(Point(0, 2))
  ..traceLigneVers(Point(0, 0));
```

**NB** il existe la version nullable de cet opérateur : ```?..```

[Code](codes/code13.dart)
