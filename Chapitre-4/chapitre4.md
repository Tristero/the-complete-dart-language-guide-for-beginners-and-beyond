# CLI (TP)

TP : nous allons créer un jeu très simple, le Chifoumi ou Pierre-Papier-Ciseaux en ligne de commande.

## 1-Pré-requis

Le SDK Dart déjà installé (pour son installation, allez sur [dart.dev/get-dart](https://dart.dev/get-dart)). De même que [Visual Studio Code](https://code.visualstudio.com/) et le plugin pour [Dart](https://marketplace.visualstudio.com/items?itemName=Dart-Code.dart-code). Si on veut utiliser Flutter (ce qui n'est pas le cas pour ce cours), il existe une extension officielle pour le support du [framework](https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter).

Si Flutter a été installé, Dart a été aussi installé.

Pour vérifier son installation : à la console on tape ```dart --version```.

## 2-Règles

Pour jouer à Chifoumi, il faut savoir que :

- Le ciseaux battent le papier
- Le papier bat la pierre
- La pierre bat les ciseaux

Ici un joueur joue contre l'ordinateur.

Donc notre jeu, la CLI va proproser trois choix au joueur. Le joueur disposera de 4 possibilités :

- pierre (l'utilisateur sélectionnera la touche **"p"**)
- ciseaux (l'utilisateur tapera la touche **"c"** de son clavier)
- papier (**"f"** pour feuille)
- **"q"** servira à quitter le jeu.

Pour toute autre entrée, on affichera un message.

Et le résultat sera affiché immédiatement après le choix aléatoire de l'ordinateur.

Il y a trois résultats possibles :

- l'humain gagne
- l'ordinateur gagne
- égalité entre les joueurs

## 3-Objectifs

- Réutiliser les éléments vus dans les chapitres 1 à 3
- Apprendre à lire une entrée à la console
- Choisir un nombre aléatoire

## 4-Pseudocode

```dart
main(List<String> args) {
  
  //création d'une boucle infinie
  //  affiche le prompt avec les choix possibles
  //  lecture de l'entrée de l'utilisateur
  //  si le choix est un mouvement 'f', 'p', 'c'
  //    randomisation du choix de l'ordinateur
  //    comparaison des résultats
  //    affiche le résultat
  //  si le choix est 'q'
  //    on quitte le programme
  //  pour les autres choix
  //    on affiche un message d'entrée invalide

}
```

## 5-Lecture d'une entrée console

Pour lire une entrée d'un utilisateur en ligne de commande, Dart dispose d'une méthode issue de la bibliothèque ```dart:io```.

Donc il faut importer cette bibliothèque en utilisant le mot-clé ```import``` suivi du nom entre guillemets de la bibliothèque :

```dart
import 'dart:io';

main(List<String> args) {
  ...
}
```

Pour récupérer l'entrée utilisateur, Dart fournit un objet : ```stdin``` qui va se charger de lire le flux en entrée. Et sur cet objet nous appelons la méthode ```readLineSync()```. Cette méthode nous retourne soit un Null soit un String : Dart nous indique que le type est ```String?```. Cela signifie que la valeur retournée peut être nulle ou être une chaîne de caractères. Pour l'instant nous ne nous en occuperons pas.

Implémentons notre code de lecture dans la boucle infinie :

```dart
import 'dart:io';

main(List<String> args) {
  //création d'une boucle infinie
  while (true) {
    //  affiche le prompt avec les choix possibles
    print(
        "pierre(p)/papier(f)/ciseaux(c).\nChoix possibles sont p, f ou c et pour quitter q :");
    //  lecture de l'entrée de l'utilisateur
    var rslt = stdin.readLineSync();

    //  si le choix est un mouvement 'f', 'p', 'c'
    //    randomisation du choix de l'ordinateur

    //    comparaison des résultats

    //    affiche le résultat

    //  si le choix est 'q'
      //    on quitte le programme
    //  pour les autres choix
    //    on affiche un message d'entrée invalide
  }
}
```

Si l'on teste notre code, nous ne disposons que du Contrôle-C pour stopper notre programme. Modifions légèrement notre programme : nous ne souhaitons pas voir le choix de l'utilisateur à la ligne suivante. Donc nous devons faire appel à nouveau à ```dart:io```. Il existe ```stdout``` et sa méthode ```write``` qui n'effectue pas de retour à la ligne :

```dart
import 'dart:io';

main(List<String> args) {
  //création d'une boucle infinie
  while (true) {
    //  affiche le prompt avec les choix possibles
    stdout.write(
        "pierre(p)/papier(f)/ciseaux(c).\nChoix possibles sont p, f ou c et pour quitter q : ");
    //  lecture de l'entrée de l'utilisateur
    var rslt = stdin.readLineSync();

    //  si le choix est un mouvement 'f', 'p', 'c'
    //    randomisation du choix de l'ordinateur

    //    comparaison des résultats

    //    affiche le résultat

    //  si le choix est 'q'
      //    on quitte le programme
    //  pour les autres choix
    //    on affiche un message d'entrée invalide
  }
}
```

## 6-Conditions

Nous allons implémenter notre boucle et les conditions. Rien de compliquer ici.

```dart
import 'dart:io';

main(List<String> args) {
  //création d'une boucle infinie
  while (true) {
    //  affiche le prompt avec les choix possibles
    stdout.write(
        "pierre(p)/papier(f)/ciseaux(c). Choix possibles sont p, f ou c et pour quitter q : ");
    //  lecture de l'entrée de l'utilisateur
    var rslt = stdin.readLineSync();

    //  si le choix est un mouvement 'f', 'p', 'c'
    if (rslt == "f" || rslt == "p" || rslt == "f") {
      print("Votre choix : $rslt");

      //    randomisation du choix de l'ordinateur

      //    comparaison des résultats

      //    affiche le résultat

      //  si le choix est 'q'
    } else if (rslt == "q") {
      //    on quitte le programme
      break;
      //  pour les autres choix
    } else {
      //    on affiche un message d'entrée invalide
      print("Vous ne pouvez choisir que p f c ou q.");
    }
  }
}
```

## 7-Gestion de l'aléatoire

L'aléatoire est géré par la bibliothèque ```dart:math```. Contrairement à ```stdin```, il nous faut créer une instance de ```Random```. Puis appeler une méthode ```nextInt()``` qui prend en paramètre la valeur limite à générer. Or ici nous avons trois possibilités. Ces trois possibilités peuvent être codées simplement via une énumération où chaque valeur est un index commençant à .

### 1-Codage de l'énumération

```dart
enum Jeu {
  Papier,
  Pierre,
  Ciseaux,
}
```

La valeur de ```Papier``` vaut 0, celle de ```Pierre``` vaut 1 et ainsi de suite. Cela va nous permettre d'utiliser et d'associer notre générateur de nombres pseudo-aléatoires.

### 2-Convertion de l'entrée utilisateur

Mais avant toute chose il nous faut convertir le choix de l'utilisateur en une valeur de l'énumération :

```dart
import 'dart:io';

import 'dart:math';

enum Jeu {
  Papier,
  Pierre,
  Ciseaux,
}

main(List<String> args) {
  //création d'une boucle infinie
  while (true) {
    //  affiche le prompt avec les choix possibles
    stdout.write(
        "pierre(p)/papier(f)/ciseaux(c). Choix possibles sont p, f ou c et pour quitter q : ");
    //  lecture de l'entrée de l'utilisateur
    var rslt = stdin.readLineSync();

    //  si le choix est un mouvement 'f', 'p', 'c'
    if (rslt == "f" || rslt == "p" || rslt == "f") {
      print("Votre choix : $rslt");

      //    conversion du choix de l'utilisateur en type Jeu
      var choix;
      switch (rslt) {
        case "p":
          choix = Jeu.Pierre;
          break;
        case "f":
          choix = Jeu.Papier;
          break;
        case "c":
          choix = Jeu.Ciseaux;
          break;
      }

      
      //    randomisation du choix de l'ordinateur

      //    comparaison des résultats

      //    affiche le résultat

      //  si le choix est 'q'
    } else if (rslt == "q") {
      //    on quitte le programme
      break;
      //  pour les autres choix
    } else {
      //    on affiche un message d'entrée invalide
      print("Vous ne pouvez choisir que p f c ou q.");
    }
  }
}
```

### 3-Aléatoire

Pour générer de l'aléatoire, Dart dispose de la bibliothèque ```dart:math``` et dans cette bibliothèque, nous trouvons une classe ```Random``` : il nous faut instancier cette classe puis appeler la méthode ```nextInt()```. Cette méthode nécessite un argument : la valeur limite du choix (valeur qui sera **rejetée**). Autrement dit, on demande un choix entre 0 et cette valeur limite **rejetée**. Pour nous, nos choix sont : 0, 1 et 2. Donc la valeur maximum rejetée est 3.

On a deux possibilités pour créer notre générateur : soit hors de la boucle et un seul générateur sera créé, soit dans la boucle et à chaque itération, un générateur sera instancié.

```dart
...
main(List<String> args) {
  //création d'une boucle infinie
  while (true) {
    //  affiche le prompt avec les choix possibles
    stdout.write(
        "pierre(p)/papier(f)/ciseaux(c). Choix possibles sont p, f ou c et pour quitter q : ");
    //  lecture de l'entrée de l'utilisateur
    var rslt = stdin.readLineSync();

    //  si le choix est un mouvement 'f', 'p', 'c'
    if (rslt == "f" || rslt == "p" || rslt == "f") {
      print("Votre choix : $rslt");

      //    conversion du choix de l'utilisateur en type Jeu
      var choix;
      switch (rslt) {
        case "p":
          choix = Jeu.Pierre;
          break;
        case "f":
          choix = Jeu.Papier;
          break;
        case "c":
          choix = Jeu.Ciseaux;
          break;
      }

      //    randomisation du choix de l'ordinateur
      Random r = Random();
      final valeurAleatoire = r.nextInt(3);
      //    comparaison des résultats

      //    affiche le résultat

      //  si le choix est 'q'
    } else if (rslt == "q") {
      //    on quitte le programme
      break;
      //  pour les autres choix
    } else {
      //    on affiche un message d'entrée invalide
      print("Vous ne pouvez choisir que p f c ou q.");
    }
  }
}
```

### 4-Comparaison des choix

Pour récupérer la valeur d'une énumération, nous disposons de la méthode ```values``` qui retourne une liste. Nous allons utiliser le nombre aléatoire généré comme index (nous verrons les collections dans le chapitre suivant) :

```dart
Jeu.values[valeurAleatoire];
```

Maintenant nous allons comparer le choix de l'ordinateur. Commençons par le plus simple l'égalité :

```dart
    final choixOrdi = Jeu.values[valeurAleatoire];
    if ( choixOrdi == choix) {
      print("Egalité !");
    }
```

Maintenant, on va déterminer tous les cas où le joueur humain gagne. Pour cela il va falloir traiter tous les cas puis on traitera en dernier les cas de réussite de l'ordinateur dans le ```else``` final :

```dart
      //    comparaison des résultats
      final choixOrdi = Jeu.values[valeurAleatoire];
      if (choixOrdi == choix) {
        print("Egalité !");
      } else if ((choix == Jeu.Pierre && choixOrdi == Jeu.Ciseaux) ||
          (choix == Jeu.Papier && choixOrdi == Jeu.Pierre) ||
          (choix == Jeu.Ciseaux && choixOrdi == Jeu.Papier)) {
        print("Vous avez gagné !");
      } else {
        print("J'ai gagné !");
      }
```

Notre algorithme codé dans ```main``` ressemblera à ceci :

```dart
main(List<String> args) {
  //création d'une boucle infinie
  while (true) {
    //  affiche le prompt avec les choix possibles
    stdout.write(
        "pierre(p)/papier(f)/ciseaux(c). Choix possibles sont p, f ou c et pour quitter q : ");
    //  lecture de l'entrée de l'utilisateur
    var rslt = stdin.readLineSync();

    //  si le choix est un mouvement 'f', 'p', 'c'
    if (rslt == "f" || rslt == "p" || rslt == "f") {
      //    conversion du choix de l'utilisateur en type Jeu
      var choix;
      switch (rslt) {
        case "p":
          choix = Jeu.Pierre;
          break;
        case "f":
          choix = Jeu.Papier;
          break;
        case "c":
          choix = Jeu.Ciseaux;
          break;
      }

      //    randomisation du choix de l'ordinateur
      Random r = Random();
      final valeurAleatoire = r.nextInt(3);

      //    comparaison des résultats
      //    affiche le résultat
      final choixOrdi = Jeu.values[valeurAleatoire];
      print("J'ai choisi $choixOrdi.");
      print("Vous avez choisi $choix.");

      if (choixOrdi == choix) {
        print("Egalité !");
      } else if ((choix == Jeu.Pierre && choixOrdi == Jeu.Ciseaux) ||
          (choix == Jeu.Papier && choixOrdi == Jeu.Pierre) ||
          (choix == Jeu.Ciseaux && choixOrdi == Jeu.Papier)) {
        print("Vous avez gagné !");
      } else {
        print("J'ai gagné !");
      }

      //  si le choix est 'q'
    } else if (rslt == "q") {
      //    on quitte le programme
      break;
      //  pour les autres choix
    } else {
      //    on affiche un message d'entrée invalide
      print("Vous ne pouvez choisir que p f c ou q.");
    }
  }
}
```

**Remarque** : Nous avons supprimé ```print("Votre choix : $rslt");``` pour le remplacer par ```print("Vous avez choisi $choix.");```.

[Code](codes/code1.dart)
