import 'dart:io';

import 'dart:math';

enum Jeu {
  Papier,
  Pierre,
  Ciseaux,
}

main(List<String> args) {
  //création d'une boucle infinie
  while (true) {
    //  affiche le prompt avec les choix possibles
    stdout.write(
        "pierre(p)/papier(f)/ciseaux(c). Choix possibles sont p, f ou c et pour quitter q : ");
    //  lecture de l'entrée de l'utilisateur
    var rslt = stdin.readLineSync();

    //  si le choix est un mouvement 'f', 'p', 'c'
    if (rslt == "f" || rslt == "p" || rslt == "f") {
      //    conversion du choix de l'utilisateur en type Jeu
      var choix;
      switch (rslt) {
        case "p":
          choix = Jeu.Pierre;
          break;
        case "f":
          choix = Jeu.Papier;
          break;
        case "c":
          choix = Jeu.Ciseaux;
          break;
      }

      //    randomisation du choix de l'ordinateur
      Random r = Random();
      final valeurAleatoire = r.nextInt(3);

      //    comparaison des résultats
      //    affiche le résultat
      final choixOrdi = Jeu.values[valeurAleatoire];
      print("J'ai choisi $choixOrdi.");
      print("Vous avez choisi $choix.");

      if (choixOrdi == choix) {
        print("Egalité !");
      } else if ((choix == Jeu.Pierre && choixOrdi == Jeu.Ciseaux) ||
          (choix == Jeu.Papier && choixOrdi == Jeu.Pierre) ||
          (choix == Jeu.Ciseaux && choixOrdi == Jeu.Papier)) {
        print("Vous avez gagné !");
      } else {
        print("J'ai gagné !");
      }

      //  si le choix est 'q'
    } else if (rslt == "q") {
      //    on quitte le programme
      break;
      //  pour les autres choix
    } else {
      //    on affiche un message d'entrée invalide
      print("Vous ne pouvez choisir que p f c ou q.");
    }
  }
}
