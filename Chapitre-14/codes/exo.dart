class EmailAddress {
  final String email;

  EmailAddress(this.email) {
    if (email.isEmpty) {
      throw FormatException("L'adresse ne peut être vide");
    }
    if (!email.contains('@')) {
      throw FormatException("l'adresse doit contenir le symbole @");
    }
  }

  @override
  String toString() {
    return email;
  }
}

main(List<String> args) {
  try {
    print(EmailAddress('me@example.com'));
    print(EmailAddress('example.com'));
    print(EmailAddress(''));
  } on FormatException catch (e) {
    print(e);
  }
}
