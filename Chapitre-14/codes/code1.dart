main(List<String> args) {
  const i = 9;

  assert(i > 0);
  print("i > 0 : OK");
  assert(i == 9);
  print("i == 9 : OK");
  assert(i < 5, "i n'est pas inférieur à 5");
  print("i < 5 : ERREUR");
}
