class Fraction {
  final int numerateur;
  final int denominateur;

  Fraction({
    required this.numerateur,
    required this.denominateur,
  }) {
    if (denominateur == 0) throw IntegerDivisionByZeroException();
  }

  double get valeur => numerateur / denominateur;
}

main(List<String> args) {
  final f = Fraction(numerateur: 3, denominateur: 0);
  print(f.valeur);
}
