class Fraction {
  final int numerateur;
  final int denominateur;

  Fraction({
    required this.numerateur,
    required this.denominateur,
  }) {
    if (denominateur == 0) throw IntegerDivisionByZeroException();
  }

  double get valeur => numerateur / denominateur;
}

main(List<String> args) {
  try {
    final f = Fraction(numerateur: 3, denominateur: 0);
    print(f.valeur);
  } on IntegerDivisionByZeroException {
    print("Division par zéro");
    rethrow; // <------------ relance de l'exception
  } on Exception {
    print("Erreur inattendue");
  } finally {
    print("le parcours du try/on/finally s'est bien achevé sur finally.");
  }
  print("le code s'est bien achevé.");
}
