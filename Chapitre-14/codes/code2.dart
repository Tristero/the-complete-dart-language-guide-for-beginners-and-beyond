class IntPositif {
  final int val;

  IntPositif(this.val) {
    assert(val >= 0, 'La valeur doit être strictement supérieure à zéro');
  }
}

main(List<String> args) {
  final invalide = IntPositif(-1);
  print(invalide);
}
