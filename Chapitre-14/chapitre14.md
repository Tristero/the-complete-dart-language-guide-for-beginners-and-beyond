# Gestion des erreurs et exceptions

Objectifs :

- la différences entre erreurs et exceptions
- les *assertions*
- utiliser ```throw```
- utiliser ```try/catch/finally/rethrow```

## 1-Exceptions et erreurs

En général, on emploie ces deux termes d'une façon interchangeable. Mais une erreur renvoie à une erreur du développeur : comme par exemple, oublier un argument dans une méthode, dépasser les limites d'index d'une liste, etc.

Quant à l'exception, il s'agit plus d'une chose inattendue, une condition qui a échoué... toute chose qui devient hors de contrôle.

Avec une erreur, le programme s'arrête. Avec une exception, il est possible de la gérer en affichant un message et de continuer le programme en proposant une solution alternative.

## 2-Assertions

### 1-Exemples

C'est le recours à la fonction ```assert()``` qui retournera ou non une exception selon que le prédicat passé en argument est vérifié ou non.

```dart
main(List<String> args) {
  const i = 9;

  assert(i > 0);
  print("i > 0 : OK");
  assert(i == 9);
  print("i == 9 : OK");
  assert(i < 5);
  print("i < 5 : ERREUR");
}
```

Pour fonctionner, **il convient d'activer les assertions** car, par défaut, le compilateur ignore ```assert``` :

```shell
$ dart run --enable-asserts code1.dart
...
```

Pour la dernière assertion, nous aurons une erreur.

Autre exemple :

```dart
class IntPositif {
  final int val;

  IntPositif(this.val) {
    assert(val >= 0);
  }
}
```

A chaque fois, nous aurons une exception non gérée : ```Unhandled exception``` suivi d'un messagge indiquant que l'on a une erreur sur une assertion qui ne vérifie pas le prédicat.

### 2-Message customisé

Avec ```assert()```, il nous est possible d'ajouter un message supplémentaire en second argument qui remplacera le message par défaut :

```dart
assert(i < 5, "i n'est pas inférieur à 5");
```

Et le message de sortie d'erreur pourra ressembler à :

```txt
...
Failed assertion: line 8 pos 10: 'i < 5': i n'est pas inférieur à 5
...
```

### 3-Bénéfice de const sur final

Avec le code suivant :

```dart
class IntPositif {
  final int val;

  IntPositif(this.val) {
    assert(val >= 0, 'La valeur doit être strictement supérieure à zéro');
  }
}

main(List<String> args) {
  final invalide = IntPositif(-1);
  print(invalide);
}
```

Si l'on remplace ```final invalide``` par ```const invalide```, le compilateur réagira immédiatement en indiquant que l'évaluation de cette expression lévera une exception.

### 4-A retenir

Quelques points à retenir

- Dart et Flutter utilisent aussi les assertions dans leurs codes.
- Pour les applications Flutter, en mode *Debug* seulement, les assertions sont pleinement fonctionnelles. Mais en mode *Release*, les assertions sont désactivées.
- Avec Dart, il faut préciser que l'on souhaite activer les assertions avec le flag ```--enable-asserts```
- Les assertions ne sont utiles que pour gérer les erreurs à l'exécution

Les exceptions n'ont pas de restrictions en fonction du mode d'utilisation.

[Code simple](codes/code1.dart)

[Code avec classe](codes/code2.dart)

## 3-Exceptions

### 1-throw

```throw``` va nous permettre de créer une erreur si une action *dangereuse* est engagée par le programmeur.

Prenons le cas de la fraction. On se crée une classe très simple :

```dart
class Fraction {
  final int numerateur;
  final int denominateur;

  Fraction({
    required this.numerateur,
    required this.denominateur,
  });

  double get valeur => numerateur / denominateur;
}
```

Nous avons un problème avec ce code : que faire si l'on passe en dénominateur 0 ?

```dart
main(List<String> args) {
  final f = Fraction(numerateur: 3, denominateur: 0);
  print(f.valeur);
}
```

Nous obtenons le résultat ```Infinity``` !

Une division par zéro doit être considérée comme une erreur. Dart nous propose ```throw``` et nous allons utiliser une exception déjà présente dans la bibliothèque standard : ```IntegerDivisionByZeroException()``` :

```dart
class Fraction {

  // Code omis pour simplifier la lecture

  Fraction({
    required this.numerateur,
    required this.denominateur,
  }) {
    if (denominateur == 0) throw IntegerDivisionByZeroException();
  }

  // Code omis pour simplifier la lecteur
```

Et là, l'exécution retournera une erreur :

```txt
Unhandled exception:
IntegerDivisionByZeroException
```

suivi d'une pile de traçage ou *stack trace* qui montre la pile d'appel des méthodes employées pour cette exception.

[Code](codes/code3.dart)

Il est possible d'afficher un texte en lieu et place d'une pile d'appel :

```dart
throw NomDeLException("message particulier à afficher")
```

[Code](codes/exo.dart)

### 2-Récupération

Si une erreur survient, il est possible de récupérer cette erreur et d'engager une action alternative avec ```try```. ```try```va essayer d'exécuter un code placé entre accolade. Il est toujours suivi d'une alternative telle que ```catch``` (mais il existe d'autres actions) :

Syntaxe :

```dart
try {
    // code à tester
} catch (e) {
    // code à exécuter en cas d'erreur
}
```

Si on reprend le code précédent :

```dart
main(List<String> args) {
  try {
    final f = Fraction(numerateur: 3, denominateur: 0);
    print(f.valeur);
  } catch (e) {
    print(e);
  }

  print("le code s'est bien achevé.");
}
```

Le programme s'est bien achevé normalement en affichant non seulement le message d'erreur mais aussi la dernière déclaration de la fonction ```main```.

Il est possible avec le ```catch``` d'afficher la pile de traçage ou *stack trace* en second argument :

```dart
try {
    //code
} catch (e, st) {
    print(st);
    // code
}
```

Et nous obtenons l'erreur : ```IntegerDivisionByZeroException``` en retour.

### 3-Cumul de catch

```try/catch``` n'est pas réservé qu'à une seule erreur, il est tout à fait possible de créer une succession de catch à l'aide de ```try/on```

```dart
try {
    //code
} on IntegerDivisionByZeroException catch (e){
    //code
    print(e);
}
```

Avec ce code, tout autre exception qu'```IntegerDivisionByZeroException``` **ne sera pas** gérée !

**NB**  une remarque importante : le ```catch(e)``` de ```on...catch(e)``` n'est pas utile si on n'utilise pas la variable ```e```. On peut écrire plus simple :

```dart
try {
    //code
} on Exception {
    //code
}
```

On peut rajouter d'autres exceptions :

```dart
try {
    // code
} on IntegerDivisionByZeroException catch(e) {
    //code
} on Exception catch(e) {
    //code
}
```

### 4-finally

Le duo ```try/catch``` et ```try/on ... catch``` peut se trouver un ```finally``` :

```dart
try {
    //code
} on IntegerDivisionByZeroException catch(e) {
    //code
} on Exception catch(e) {
    //code
} finally {
    //Tout code placé ici sera TOUJOURS exécuté qu'une erreur soit levée ou non
}
```

Tout code placé dans un ```finally``` sera **toujours exécuté**.

[Code](codes/code4.dart)

### 5-rethrow

```rethrow``` va relancer l'exception :

```dart
main(List<String> args) {
  try {
    final f = Fraction(numerateur: 3, denominateur: 0);
    print(f.valeur);
  } on IntegerDivisionByZeroException {
    print("Division par zéro");
    rethrow; // <--------- relance de l'erreur
  } on Exception {
    print("Erreur inattendue");
  } finally {
    print("le parcours du try/on/finally s'est bien achevé sur finally.");
  }
  print("le code s'est bien achevé.");
}
```

Que va-t-il se passer ?

Dart va retourner les messages suivants :

```txt
Division par zéro
le parcours du try/on/finally s'est bien achevé sur finally.
Unhandled exception:
IntegerDivisionByZeroException
...
```

Le programme va s'arrêter subitement après l'appel de ```finally``` avec l'affichage du ```IntegerDivisionByZeroException``` et sa pile de traçage.

Donc ```rethrow``` sera utile pour :

- gérer une erreur dans un bloc ```catch```
- propager l'exception dans le code appelant.

[Code](codes/code5.dart)

### 6-Exception

Toute exception dérive de la classe abstraite ```Exception```. Ce qui signifie que l'on peut aussi créer des exceptions qui nous sont propres.
