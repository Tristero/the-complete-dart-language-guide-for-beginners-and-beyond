# Null safety

Comment gérer les variables dont on est sûr qu'elles ne contiennent **aucune** valeur null ?

La sécurisation des nulls ou *null safety* apparaît dès juin 2020 avec Dart 2.9.

Tout le propos ci-dessous ne peut s'appliquer pour les versions antérieures à 2.9 !

## 1-variables nullables

Nous ne pouvons plus écrire ce genre d'initialisation :

```dart
int x = null;
```

Il faut utiliser une syntaxe différente pour pouvoir utiliser null :

```dart
int? x = null;

// ou plus simple

int? y;
```

Avec cette écriture, une déclaration ```x + 2``` génère une erreur à la compilation. Il faut effectuer une vérification préalable avant tout déréférencement !

Une première solution :

```dart
if (x == null) {
    print("impossible car x est null");
} else {
    print(x + 2);
}
```

Si ```x``` est *nullable*, la déclaration suivante est dite *non-nullable* :

```dart
int x;
```

```x``` attend une valeur entière non-*null*.

Une autre solution est l'utilisation du [IF-NULL](#3-condition-if-null)

## 2-Opérateur d'assertion

Si l'on est sûr que la variable *nullable* est non-nulle, Dart dispose d'un opérateur indiquant que cette variable est vraiment non-nulle : ```!``` que l'on postfixe à la variable. On parle aussi de l'opérateur *bang*.

```dart
int x = 42;
int? variableNullable;

if (x > 0) {
    variableNullable = x;
}

int y = variableNullable!;

print(y);
```

Petite précision sur la condition : ici nous introduisons une incertitude, sans ça le compilateur nous dirait que l'utilisation de ```!``` est inutile.

Si on retire l'opérateur ```!```, alors on a une erreur : on peut affecter un type ```int?``` (la variable ```variableNullable```) à un type ```int``` (ce qu'est notre variable ```y```).

[Code](codes/code1.dart)

## 3-Condition if-null

L'opérateur IF-NULL  s'écrit ```??``` et il permet d'attribuer une valeur par défaut si valeur d'une variable est *null*.

Syntaxe :

```dart
Type t = variableNullable ?? valeurParDefaut;
```

Si ```variableNullable``` est nulle alors elle prendra la ```valeurParDefaut``` sinon elle conserve sa valeur.

Exemple :

Transformons le code suivant avec ```??``` :

```dart
int x = 42;
int? variableNullable;

if (x > 0) {
    variableNullable = x;
}

// Ligne à transformer
int y = variableNullable == null ? 0 : variableNullable;

print(y);
```

Au lieu d'utiliser l'opérateur ternaire, nous utiliserons l'opérateur ```??```.

```dart
int x = 42;
int? variableNullable;

if (x > 0) {
  variableNullable = x;
}

int a = variableNullable ?? 0;

print(a);
```

### Affectation conditionnelle

Autre alternative :  déclarer et affecter conditionnellement une valeur avec l'opérateur ```??=``` si la variable est nulle.

Exemple :

```dart
int x = 42;
int? variableNullable;
if (x > 0) {
  variableNullable = x;
}
// affecte O si initialisation null
variableNullable ??= 0;

int b = variableNullable;
print(b);
```

[Code](codes/code2.dart)

## 4-Null safety et les collections

Peut-on utiliser des nulls dans une collection :

```dart
// Ne génère pas d'erreurs :
const v1 = ["Berlin", "Madrid", null];
const v2 = <String?>["Washington", "Montréal", null];
```

Donc nous pouvons typer une collection avec des types nullables.

Si on itère sur ces collections nous aurons un type ```String?``` donc un risque de tomber sur des nulls. Et tout appel de ce genre :

```dart
for (var ville in v1) {
  print(ville.toUpperCase());
}
```

génèrera une erreur car ```ville``` est potentiellement null.

Forcer ```ville!.toUpperCase()``` génèrera une exception à l'exécution du code.

Donc la seule solution est de vérifier si la variable est null ou non :

```dart
for (var ville in v1) {
  if (ville != null) {
    print(ville.toUpperCase());
  }
}
```

Dans ce code tout null est expurgé de tout traitement et d'erreur.

[Code](codes/code3.dart)

## 5-Accès conditionnel

Au lieu d'utiliser un if, nous pouvons postfixer ```?``` à la fin de la variable comme suit :

```dart
const v2 = <String?>["Washington", "Montréal", null];

for (var ville in v2) {
  print(ville?.toUpperCase());
}
```

Le résultat va différer du code précédent car ```null``` sera affiché mais pas traité :

```txt
WASHINGTON
MONTRÉAL
null
```

[Code](codes/code3.dart)

## 6-Première conclusion

Ce sont les premières bases de la sécurisation dans l'utilisation et le traitement de null dans le code. Mais Dart applique cette sécurisation à d'autres niveaux tels les fonctions ou encore les classes.
