main(List<String> args) {
  const v1 = ["Berlin", "Madrid", null];
  const v2 = <String?>["Washington", "Montréal", null];

  for (var ville in v1) {
    if (ville != null) {
      print(ville.toUpperCase());
    }
  }

  // accès conditionnel
  for (var ville in v2) {
    print(ville?.toUpperCase());
  }
}
