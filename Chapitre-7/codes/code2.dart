main(List<String> args) {
  // opérateur d'assertion
  int x = 42;
  int? variableNullable;
  //// on introduit une possible incertitude
  if (x > 0) {
    variableNullable = x;
  }

  // opérateur IF-NULL
  int a = variableNullable ?? 0;
  print(a);

  // affectation conditionnée
  variableNullable ??= 0;
  int b = variableNullable;
  print(b);
}
