main(List<String> args) {
  // Variable dite nullable
  int? z;
  if (z == null) {
    print("impossible de faire un calcul");
  } else {
    print(z + 2);
  }

  // opérateur d'assertion
  int x = 42;
  int? variableNullable;
  //// on introduit une possible incertitude
  if (x > 0) {
    variableNullable = x;
  }
  int y = variableNullable!;
  print(y);
}
