# Système de type en Dart

## 1-Inférence de type

Il est possible de ne pas déclarer le type et laisser le compilateur de déterminer le type en utilisant le mot-clé ```var``` :

On remplacera la déclaration : ```String msg = "hello";``` par ```var msg = "hello";```.

## 2-final

```final``` est un mot réservé du langage. Sa spécificité est de créer une variable en lecture seule (et non une constante [cf. infra](#3-const)) : il faut déclarer un nom de variable et affecter une valeur.

```dart
final msg = "hello";
```

Il n'est pas nécessaire de déclarer le type avec ```final``` :

```dart
// pas nécessaire d'écrire ceci
final String msg = "hello";
```

## 3-const

Dart propose aussi un mot réservé ```const``` qui doit être vu comme un ```final``` encore plus restrictif.

La différence c'est qu'une variable déclarée avec ```const``` devient une **constante à la compilation**. Autrement dit on permet à Dart d'optimiser son code généré.

Attention à son utilisation :

```dart
// pas de soucis avec la déclaration suivante
const msg = "hello";

// Le code suivant va générer une exception
const newMsg = msg.toUpperCase();
```

Il est impossible de déclarer une ```const``` pour ```newMsg``` car pour le compilateur : *toute constante doit être initialisée avec une valeur constante* (sic) ! Le problème ici c'est que l'évaluation de la méthode ```toUpperCase()``` est effectuée à l'exécution du code **pas** à la compilation.

En revanche le code suivant est correct :

```dart
const x = 1;
const y = 2;
const z = x + y;
```

Le compilateur pourra créer sa constante ```z``` à la compilation.

## 4-dynamic

Nous savons que Dart est un langage fortement typé et son typage est statique. Or il est possible de modifier le type d'une variable grâce au mot-clé ```dynamic``` :

```dart
dynamic x = 10;
print(x);
x = true;
print(x);
```

L'utilisation de ```dynamic``` sera à éviter autant que possible pour garantir la sécurité du code. Mais ```dynamic``` sera très utile pour certaines situation comme gérer du JSON.

## 5-Dart 2.9

Avec Dart 2.9 est apparu le principe de *Null Safety* que l'on abordera ultérieurement et qui permet de gérer des variables pouvant soit posséder une valeur non-nulle soit une valeur nulle. La gestion de cette sécurisation est très proche de celle de Kotlin.
