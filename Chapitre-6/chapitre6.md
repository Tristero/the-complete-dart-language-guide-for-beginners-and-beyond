# TP : Traitement de données avec Dart

Nous allons recourir à un CSV comprenant des données de type log. L'objectif est qu'à partir de ces données, on génère un rapport indiquant le temps mis par type d'activité.

Le code devra récupérer en argument le nom du fichier CSV et devra afficher par activité, la somme totale d'heure par activité.

Objectifs :

- lire les arguments de la CLI
- lire un CSV sur le système local
- traiter ligne par ligne les données
- afficher un résultat

Normalement le code est réalisable en une trentaine de lignes.

## 1-Lire les arguments de la CLI

### 1-Paramètre

Pour lire, il suffit de mettre un paramètre à ```main``` :

```dart
main(List<String> args) {
  print(args);
}
```

Si on tape la commande ```dart run code1.dart hello -n world``` on aura le résultat suivant :

```txt
[hello, -n, world]
```

### 2-Vérification

Nous voulons que le programme échoue s'il n'y a pas d'argument passé. Pour cela on affichera un message et on appellera une méthode spéciale (du package ```dart:io```) : ```exit()``` qui prend en argument un entier. 0 = tout s'est bien déroulé. Sinon on y ajoute un chiffre entre 0 et 127 (recommandé par Dart).

```dart
  if (args.isEmpty) {
    print("Usage : le logiciel requiert le nom d'un fichier CSV");
    exit(1);
  }
```

Puis on va récupérer le nom du fichier via l'accès indicé de la liste  ```args[0]```.

```dart
main(List<String> args) {
  //print(args);

  if (args.isEmpty) {
    print("Usage : le logiciel requiert le nom d'un fichier CSV");
    exit(1);
  }

  final nomFichier = args[0];
  print("Fichier récupéré = $nomFichier");
}
```

## 2-Lire ligne par ligne

### 1-Ouvrir un fichier

Le package ```dart:io``` dispose d'une classe ```File``` que l'on instancie en passant en argument le chemin vers le fichier à lire.

```dart
var f = File(nomFichier);
```

### 2-Lire les lignes

Dart propose deux façons de procéder : une synchrone et une asynchrone. Nous n'allons pas nous intéresser pour l'instant à la programmation asynchrone :

```dart
var lignes = f.readAsLinesSync(encoding: utf8);
```

### 3-Vérification de l'existence du fichier

Dart propose ```exists()``` et ```existsSync()``` qui retourne pour le second un booléen :

```dart
  var f = File(nomFichier);
  if (!f.existsSync()) {
    print("Erreur : le fichier '$nomFichier' n'existe pas !");
    exit(2);
  }

  // maintenant on peut lire les lignes :
  var lignes = f.readAsLinesSync(encoding: utf8);
```

## 3-Présentation et codage

### 1-Ce que l'on veut

Notre CSV est composé de champs séparés par une virgule. Chaque champ est entouré par des guillemets. La première ligne est le nom de chaque colonne.

Ce qui nous intéresse c'est la colonne "Duration" et la colonne "Tag". Nous voulons que pour chaque tag/activité on ait la somme totale de durée.

Nous utiliserons une map de type ```String``` et ```double```.

Il nous faudra rejeter la première ligne. On utilisera une méthode d'extraction. Puis nous itèrerons sur notre nouvelle liste.

Chaque ligne sera décomposée en une liste de valeur. On récupèrera les valeurs des deux champs qui nous intéressent aux index 3 et 5.

Il faudra bien penser à gérer les valeurs null et le parsing d'un String en int !

Enfin nous afficherons les résultats.

### 2-Suppression de la première ligne

Dart fournit ```sublist()``` qui prend un argument obligatoire (un entier représentant l'index à partir duquel on commence à extraire la liste) et un argument optionnel qui est la fin de l'extraction (ce qui ne nous intéresse pas) :

```dart
  final lignes = f.readAsLinesSync(encoding: utf8).sublist(1);
```

### 3-Création de la map

```dart
  final mapRslt = Map<String, double>();
```

On peut itérer dans boucle FOR-IN.

### 4-Séparation d'une ligne en liste de valeurs

Avec Dart, on utilisera ```split()``` qui prend un argument de type ```String``` : c'est le séparateur et ce sera, pour nous, la virgule. Et à partir de là, on va pouvoir extraire les valeurs.

```dart
  for (var ligne in lignes) {
    final valeurs = ligne.split(",");
;
}
```

### 5-Extraction des valeurs

**ATTENTION** : les valeurs extraites sont ```"Blogging"``` et ```"1.5"``` et **non** ```Blogging``` ni ```1.5```. Donc nous **devons supprimer** les guillemets à l'aide de la méthode ```replaceAll()``` qui prend deux arguments de type ```String``` : le premier est la chaîne à trouver le second est la chaîne de remplacement.

```dart
  for (var ligne in lignes) {
    final valeurs = ligne.split(",");
    final duration = double.parse(valeurs[3].replaceAll('"', ""));
    final tag = valeurs[5].replaceAll('"', '');
}
```

Sans le ```replaceAll()```, le parsing des durées génèrerait une exception.

### 6-Modification de la Map

Nous devons effectuer pour chaque tag la somme des durées. Tout d'abord on va extraire la valeur d'une clé dans une variable ```somme```. Il faudra vérifier si la valeur par défaut de ```somme``` n'est pas nulle. Si c'est le cas alors on affecte à ```somme``` la valeur ```0.0```.

```dart
  for (var ligne in lignes) {
    final valeurs = ligne.split(",");
    final duration = double.parse(valeurs[3].replaceAll('"', ""));
    final tag = valeurs[5].replaceAll('"', '');
    var somme = mapRslt[tag];
    if (somme == null) {
      somme = 0.0;
    }
    mapRslt[tag] = somme + duration;
  }
```

### 7-Affichage des résultats

Pour finir nous n'allons pas utiliser une boucle FOR-IN mais la méthode ```forEach``` qui permettra d'avoir un code plus simple et clair.

Les méthodes ```forEach``` sont communes aux collections itérables dont font parties les listes, sets et maps. Cette méthode prend en argument une fonction anonyme. Et nous utiliserons la syntaxe que l'on retrouve en JS avec la *fat arrow* :

```dart
  mapRslt.forEach((key, value) => print("$key = ${value.toStringAsFixed(1)}h"));
```

**NB** : nous ne souhaitons afficher qu'un chiffre après la virgule donc nous appelons la méthode ```toStringAsFixed(1)```.

### 8-Une clé vide

La troisième ligne affichée ne montre pas de clé :

```txt
 = 52.4h
```

Nous allons modifier la fonction anonyme avec l'opérateur ternaire conditionnel :

```dart
  mapRslt.forEach((key, value) {
    final cle = key == "" ? "Unallocated" : key;
    print("$cle = ${value.toStringAsFixed(1)}h");
  });
```

Et nous pouvons condenser le code en une seule ligne :

```dart
  mapRslt.forEach((key, value) {
    print("${key == "" ? "Unallocated" : key} = ${value.toStringAsFixed(1)}h");
  });
}
```

### 9-Ajout du nombre total d'heures

On se crée une variable ```var totalHeures = 0.0;```.

Dans la boucle, on lui ajoute la valeur de ```duration``` :

```dart
totalHeures += duration;
```

Puis on affiche le résultat :

```dart
print("Nbre total d'heures = ${totalHeures.toStringAsFixed(1)}h");
```

## 4-Code complet

Voici le code complet, épuré des affichages inutiles de débogage :

```dart
import 'dart:convert';
import 'dart:ffi';
import 'dart:io';

main(List<String> args) {
  if (args.isEmpty) {
    print("Usage : le logiciel requiert le nom d'un fichier CSV");
    exit(1);
  }

  final nomFichier = args[0];

  var f = File(nomFichier);
  if (!f.existsSync()) {
    print("Erreur : le fichier '$nomFichier' n'existe pas !");
    exit(2);
  }

  final lignes = f.readAsLinesSync(encoding: utf8).sublist(1);
  final mapRslt = Map<String, double>();
  var totalHeures = 0.0;

  for (var ligne in lignes) {
    final valeurs = ligne.split(",");
    final duration = double.parse(valeurs[3].replaceAll('"', ""));
    final tag = valeurs[5].replaceAll('"', '');
    var somme = mapRslt[tag];
    if (somme == null) {
      somme = 0.0;
    }
    mapRslt[tag] = somme + duration;
    totalHeures += duration;
  }

  mapRslt.forEach((key, value) {
    print("${key == "" ? "Unallocated" : key} = ${value.toStringAsFixed(1)}h");
  });
  print("Nbre total d'heures = ${totalHeures.toStringAsFixed(1)}h");
}

```

[Code](codes/code1.dart)
