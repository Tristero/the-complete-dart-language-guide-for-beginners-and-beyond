import 'dart:convert';
import 'dart:io';

main(List<String> args) {
  if (args.isEmpty) {
    print("Usage : le logiciel requiert le nom d'un fichier CSV");
    exit(1);
  }

  final nomFichier = args[0];

  var f = File(nomFichier);
  if (!f.existsSync()) {
    print("Erreur : le fichier '$nomFichier' n'existe pas !");
    exit(2);
  }

  final lignes = f.readAsLinesSync(encoding: utf8).sublist(1);
  final mapRslt = Map<String, double>();
  var totalHeures = 0.0;

  for (var ligne in lignes) {
    final valeurs = ligne.split(",");
    final duration = double.parse(valeurs[3].replaceAll('"', ""));
    final tag = valeurs[5].replaceAll('"', '');
    var somme = mapRslt[tag];
    if (somme == null) {
      somme = 0.0;
    }
    mapRslt[tag] = somme + duration;
    totalHeures += duration;
  }

  mapRslt.forEach((key, value) {
    print("${key == "" ? "Unallocated" : key} = ${value.toStringAsFixed(1)}h");
  });
  print("Nbre total d'heures = ${totalHeures.toStringAsFixed(1)}h");
}
