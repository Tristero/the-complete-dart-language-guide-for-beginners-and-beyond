# Mixins et extensions

Ces fonctionnalités vont permettre d'étendre les fonctionnalités des types existants. Les **mixins** permettent de résoudre le problème de l'héritage simple : une classe-fille ne peut hériter que d'une classe-mère. Les **extensions**, quant à elles, résolvent le même problème mais avec une optique différente : on ajoute des fonctionnalités dans **modifier la super-classe**. Cette dernière option permet d'étendre les classes de frameworks et autres SDK tels que Flutter.

## 1-Mixins

### 1-Problèmatique

Imaginons une classe ```Animal``` puis deux autres classes héritantes : ```Poisson``` et ```Humain```. Si tous les animaux peuvent respirer (nous créons une méthode dans ```Animal```, ```respire()```), en revanche, tous les animaux ne peuvent pas nager. Donc on doit créer des méthodes redondantes, partagées pour chaque classe où les animaux peuvent nager :

```dart
class Animal {
  void respire() => print("respire");
}

class Poisson extends Animal {
  void nage() => print("nage");
}

class Humain extends Animal {
  void nage() => print("nage");
}
```

Nous avons deux méthodes redondantes. Comment gérer l'affaire ? Hors de question de placer ce code dans la classe-mère. La solution est le mixin.

### 2-Syntaxe et utilisation du mixin

Pour créer un mixin on appelle le mot-clé ```mixin``` suivi d'un nom (en *UpperCamelCase*) et d'accolades. A l'intérieur des accolades on y place les fonctions que l'on souhaite partager et ajouter :

```dart
mixin Nager {
  void nage() => print("nage");
}
```

Maintenant les classes héritantes peuvent utiliser les mixins comme suit :

```dart
class ClasseFille extends ClasseMere with NouveauMixin1, NouveauMixin2, ... {
    ...
}
```

Il est possible d'ajouter autant de mixins à une classe.

Pour notre code :

```dart
mixin Nager {
  void nage() => print("nage");
}

class Animal {
  void respire() => print("respire");
}

class Poisson extends Animal with Nager {}

class Humain extends Animal with Nager {}
```

Et on peut tester que tout fonctionne :

```dart
main(List<String> args) {
  final p = Poisson();
  final h = Humain();

  p.respire();
  p.nage();

  h.respire();
  h.nage();
}
```

Les mixins sont des outils pour définir des comportements particuliers, des fonctionnalités particulières. Les mixins ont d'autres avantages comme d'alléger les hiérarchies de classe.

### 3-Mixins, classes et choix

Dart permet d'utiliser le mot-clé ```with``` avec une classe :

```dart
class Respiration {
  void respire() => print("respire");
}

class Animal with Respiration {}
```

Et cela n'engendre aucune conséquence sur le reste du code. **ATTENTION** si ```Respiration``` est *instanciable* ce n'est pas du tout le cas pour les *mixins* ! La question est de savoir si on doit utiliser des classes ou des mixins. On utilisera les mixins lorsqu'on souhaite représenter une action.

[Code](codes/code1.dart)

### 4-Problèmes avec les mixins

On pourrait croire que l'on pourrait utiliser les mixins partout. Il y a des limitations :

- impossible d'en créer une instance
- ne possède pas de constructeur (conséquence du point précédent)
- il peut y avoir des *collisions* de noms de méthodes

Dans le cas de la collision, cela peut s'avérer vicieux. Ainsi on peut avoir deux mixins avec des noms différents mais avec des méthodes à la signature **exactement** identique sans que le compilateur ne s'en plaigne :

```dart
mixin Mixin1 {
    int truc = 1;
}

mixin Mixin2 {
    int truc = 2;
}
```

Maintenant si l'on crée une classe qui utilise ses deux mixins, et que sur l'instance de cette classe on appelle la propriété ```truc``` quelle sera la valeur utilisée ?

```dart
mixin Mixin1 {
  int truc = 1;
}

mixin Mixin2 {
  int truc = 2;
}

class Bidule with Mixin1, Mixin2 {}

main(List<String> args) {
  final b = Bidule();
  print(b.truc);
}
```

La réponse sera 2. Et si on inverse l'ordre d'appel des mixins : ```class Bidule with Mixin2, Mixin1 {}```, le résultat affiché sur 1 !

L'ordre des mixins va écraser les mixins homonymes et le plus récent dans les appels sera celui qui sera conserver.

Donc vaut-il mieux utiliser les mixins ou les classes abstraites ? Tout dépend des cas d'utilisation et il faudra bien réfléchir sur leur utilisation. Il n'y ni solution miracle, ni choix évident

[Code](codes/code2.dart)

## 2-Extensions

Face aux mixins, Dart propose les extensions. Ces extensions se greffent sur des types existants et l'on peut récupérer les membres des classes très facilement.

### 1-Syntaxe

Syntaxe : on utilise les mots-clés ```extension on``` suivi de la classe à étendre puis on rédigera le code dans un bloc entre accolades.

```dart
extension on NomDeLaClasse {
    // on crée de nouvelles méthodes
}
```

On peut nommer l'extension (cf. infra pour son utilité) :

```dart
extension NomExtension on NomDeLaClasse {
    //Code
}
```

La meilleure pratique est la seconde syntaxe.

### 2-Exemple

On a déjà vu ```int.tryParse('123')``` mais on ne peut pas écrire ```'123'.toInt()```. Les extensions nous permettent de résoudre ce problème facilement : nous allons étendre la classe String comme suit :

```dart
extension on String {
  int? toInt() => int.tryParse(this);
}
```

Observons que l'on peut accéder à la valeur de l'instance de ```String``` grâce à ```this```, ce qui n'est pas possible avec les mixins.

### 3-Séparation des codes

Attention si on place les extensions dans un autre fichier, il faudra importer le fichier et **nommer** l'extension (avec un nom de son choix) :

```dart
extension ParsingNumber on String {
  int? toInt() => int.tryParse(this);
}
```

Si on a enregistrer les extensions dans un autre fichier : il faudra importer seulement le nom du fichier. Plaçons l'extension dans un fichier ```ext.dart```, par exemple, il nous faut modifier le fichier contenant le main

```dart
import 'ext.dart';

main(List<String> args) {
  final nb = '123'.toInt();
  if (nb is int) {
    print('nb est bien un entier');
  }
}
```

[Code](codes/code3.dart) et son [extension](codes/ext.dart)

### 4-Génériques

Il est tout à fait possible d'utiliser des génériques avec les extensions.

Exemple : création d'une méthode ```sum()``` 

Imaginons que l'on souhaite appliquer cette méthode sur une liste : ```[1,2,3].sum();```. Avec les connaissances nous pourrions écrire un code :

```dart
extension Utilities on Iterable<int> {
  int sum() => this.reduce((value, element) => value + element);
}
```

Ce code ne fonctionnera que sur les types entiers. On pourrait transformer le type ```int``` en ```num``` sauf que le type de retour sera ```num``` et non ```int``` ou ```double```.

Donc nous allons recourir à la généricité et nous allons **contraindre** à ce que le type appartiennent à ```num``` et à toutes ses classes-filles. Pour cela on utilisera le mot-clé ```extends``` sur le générique :

```dart
extension Utilities<T extends num> on Iterable<T> {
  T sum() => this.reduce((value, element) => (value + element) as T);
}
```

```<T extends num>``` impose une contrainte de subordination à un type et à ses sous-types : ```num```, ```int``` et ```double```. Le code ```(value + element) as T``` est nécessaire car c'est le compilateur qui, ici, pose problème : il n'est pas assez intelligent pour déterminer que les types de ```value``` et ```element``` peuvent être ```num```, ```int``` ou ```double```.

Si la constrainte avait été ignorée, Dart aurait utilisé le type ```Object``` par défaut. Cette contrainte peut être appliquée sur n'importe quel sous-classe d'```Object```.

[Code](codes/code4.dart)

### package dartx

Il existe un package qui évite de réinventer la roue avec ce type d'extensions, il s'agit de [dartx](https://pub.dev/packages/dartx). Ce package rajoute un ensemble de fonctionnalités aux classes num, String et autres structures de données. Le dépôt [Github](https://github.com/leisim/dartx) est une source intéressante pour approfondir ses connaissances sur l'utilisation de Dart et des extensions.
