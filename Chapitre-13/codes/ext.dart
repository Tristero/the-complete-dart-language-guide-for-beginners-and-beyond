extension ParsingNumber on String {
  int? toInt() => int.tryParse(this);
}
