extension Utils on int {
  List<int> rangeTo(int other) {
    List<int> lst = [];

    if (other < this) {
      return lst;
    }

    for (int i = this; i <= other; i++) {
      lst.add(i);
    }
    return lst;
  }
}

main(List<String> args) {
  final l = 1.rangeTo(5);
  print(l);

  final lvide = 5.rangeTo(3);
  print(lvide);
}
