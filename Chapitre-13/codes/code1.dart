mixin Nager {
  void nage() => print("nage");
}

class Respiration {
  void respire() => print("respire");
}

class Animal extends Respiration {}

class Poisson extends Animal with Nager {}

class Humain extends Animal with Nager {}

main(List<String> args) {
  final p = Poisson();
  final h = Humain();

  p.respire();
  p.nage();

  h.respire();
  h.nage();
}
