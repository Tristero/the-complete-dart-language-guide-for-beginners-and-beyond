mixin Mixin1 {
  int truc = 1;
}

mixin Mixin2 {
  int truc = 2;
}

class Bidule with Mixin1, Mixin2 {}

class BiduleDifferent with Mixin2, Mixin1 {}

main(List<String> args) {
  final b = Bidule();
  print('Avec Bidule with Mixin1, Mixin2 b.truc vaut ${b.truc} ');

  final b2 = BiduleDifferent();
  print('Avec BiduleDifferent with Mixin2, Mixin1 b2.truc vaut ${b2.truc} ');
}
