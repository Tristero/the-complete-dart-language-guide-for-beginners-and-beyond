extension Utilities<T extends num> on Iterable<T> {
  T sum() => this.reduce((value, element) => (value + element) as T);
}

main(List<String> args) {
  final s = [1, 2, 3].sum();
  print(s);

  final s1 = [1.0, 2.0, 3.0].sum();
  print(s1);
}
