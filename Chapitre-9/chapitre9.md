# Fonctions : notions avancées et opérateurs fonctionnels

- fonctions anonymes et les closures/fermetures
- fonctions comme argument
- types fonction et alias
- opérateurs fonctionnels : map, where, reduce
- génériques

## 1-Fonctions anonymes

Ne prennent pas de nom et dispose de la syntaxe suivante :

```dart
// Création
var nomVariable = (arg1, ..., argN) => {
    ...
};

// Utilisation
nomVariable(valeur);
```

Exemple :

```dart
void main() {
    final hello = (nom) => "Hello, my name is ${nom}.";
    print(hello("John"));
}
```

**NB**  l'argument de la fonction anonyme peut être typé :

```dart
final hello = (String nom) => "Hello, my name is ${nom}.";
```

## 2-Fonctions comme arguments

On peut utiliser en argument d'une fonction, d'une méthode une fonction.

Le typage de la fonction passée en argument, en Dart, s'effectue comme suit :

```dart
TypeDeRetour Function(TypeArgument1, ..., TypeArgumentN) nomFonctionArgument
```

La signature est la même que celle d'une fonction traditionnelle sauf que l'on précise le mot-clé ```Function```.

### Exemple

#### 1-Création

```dart
void welcome(String nom, String Function(String) greet) {
    print(greet(nom));
}
```

Donc la fonction prend deux arguments positionnels :

- une chaîne de caractères
- une fonction

#### 2-Utilisation

On peut procéder deux manières :

- avec une fonction anonyme directement placée en argument

```dart
welcome("Jane", (str) => str.toUpperCase());
```

- en créant une fonction déportée (imbriquée ou non) :

```dart
main(List<String> args) {
  final fun = (String str) {
    final lng = str.length;
    return "Le mot comporte ${lng} caractères";
  };

  welcome("bibule", fun);
}
```

[Code](codes/code1.dart)

## 3-Type alias

Le problème avec le passage d'une fonction en argument est l'écriture du type : ```TypeDeRetour Function(TypeArgument...) nomArgumentFonction```.

Une solution est d'utiliser un type alias
à l'aide du mot-clé ```typedef```.

Syntaxes :

```dart
typedef NomFonction = TypeDeRetour Function(TypeArg, ..., TypeArgN);
```

Ou plus concis :

```dart
typedef TypeDeRetour NomDeLaFonction(TypeArg, ..., TypeArgN);
```

### Exemple de création d'un type-alias

Réécriture de notre code précédent :

```dart
void welcome(String nom, String Function(String) greet) {
  print(greet(nom));
}
```

On va extraire le type fonction vers un type alias :

```dart
typedef Greet = String Function(String);
```

Ou :

```dart
typedef String Greet(String);
```

Puis on utilisera seulement le type ```Greet``` pour typer l'argument :

```dart
void welcome(String nom, Greet greet) {
  print(greet(nom));
}
```

On va pouvoir décliner ce type-alias pour créer un nombre X de fonctions obéissant à ce type : on pourra passer à la fonction ```welcome()``` autant de fonctions ayant des comportements totalement différents **mais** respectant la signature.

[Code](codes/code2.dart)

## 4-Closures

ou fermetures en français.

Comme n'importe quel autre langage, il est possible d'appliquer une closure avec Dart :

```dart
main(List<String> args) {
  const multiplicateur = 10;
  const lst = [2, 4, 6];
  final rslt = lst.map((e) => e * multiplicateur);
  print(rslt);
}
```

```x``` a une portée locale à la lambda mais la fonction anonyme utilise la constante ```multiplicateur``` dont la portée est plus large.

Tous ces éléments sont très largement utilisés avec Dart.

[Code](codes/code3.dart)

## 5-Collections itérables

Une collection est dite itérable si celle-ci implémente la class ```Iterable``` ! Une collection ```Iterable``` permet un accès séquentiel. Avec Dart nous possédons déjà trois grandes familles de collections : les ```List```s, les ```Map```s et les ```Set```s.

Ces collections dépendantes d'```Iterable``` disposent donc de méthodes communes pour être manipulées de la "même" manière.

### Génériques

De même, la plupart des méthodes partagées par les ```Iterable```s recourt aux génériques pour typer leurs arguments.

Avec les langages implémentant les génériques, il est possible d'abstraire un code en le rendant plus sûr.

#### Premier exemple

Nous souhaitons créer une liste à partir d'une autre liste : mais nous souhaitons que le type de chaque élément en entrée soit de même type qu'en sortie. Un entier restera un entier. De même que pour un double, etc.

```dart
  const lstInt = [1, 2, 3];
  const lstDbl = [1.0, 2.0, 3.0];

  List<int> transforme(List<int> lst, int Function(int) fonction) {
    final rslt = <int>[];
    for (var elem in lst) {
      rslt.add(fonction(elem));
    }
    return rslt;
  }
```

Si l'on veut faire la même opération pour les doubles, nous devrons la réécrire... mais les génériques nous permettent un plus grande abstraction. On utilise plus un type connu mais une lettre majuscule seule (nomenclature recommandée) en lieu et place du type int, double, etc.

Voyons comment la signature change avec cette nouvelle syntaxe :

```dart
/*
Avec un type connu :
List<int> transforme(List<int> lst, int Function(int) fonction) {}

Réécriture générique
*/
List<T> transforme<T>(List<T> lst, T Function(T) fonction) {}
```

On a précisé que la fonction ```transforme``` sera générique avec l'ajout de ```<T>``` juste après son nom.

Maintenant l'implémentation du code : il suffira de remplacer les types par ```T``` :

```dart
List<T> transforme<T>(List<T> lst, T Function(T) fonction) {
  final rslt = <T>[];
  for (var elem in lst) {
    rslt.add(fonction(elem));
  }
  return rslt;
}
```

Et maintenant nous pouvons utiliser n'importe quel type avec ```transforme``` :

```dart
  var newLstInt = transforme<int>(lstInt, (x) => x * x);

  print("Ancienne liste d'entier = $lstInt et la nouvelle liste = $newLstInt");

  var newLstDbl = transforme<double>(lstDbl, (x) => x * x);
  print("Ancienne liste d'entier = $lstDbl et la nouvelle liste = $newLstDbl");
```

Important : ne pas oublier de préciser le type entre les ```<>``` !!!

[Code](codes/generique1.dart)

#### Second exemple

Comment procéder pour avoir un certain type en entrée et, possiblement, un autre ? Comment faire si à partir d'une liste de valeurs double on veut obtenir une liste de valeurs entières ?

```dart
List<R> transforme<T, R>(List<T> liste, R Function(T) fonction) {
  List<R> rslt = <R>[];

  for (var element in liste) {
    rslt.add(fonction(element));
  }

  return rslt;
}
```

Pour être utilisée, cette fonction demande deux types T et R. T est le type en entrée et R en retour : donc ```transforme``` va utiliser une valeur de type ```T``` et retourner une valeur de type ```R``` sous forme de ```List```. (pour le retour de la fonction ```transforme```) et comme valeur pour la fonction passée en argument.

Donc lors de l'utilisation effective de cette fonction, nous allons substituer les types génériques en types "normaux" :

```dart
main(List<String> args) {
  const lstInt = [1, 2, 3];
  const lstDbl = [1.0, 2.0, 3.0];

  // on appelle transforme pour créer une liste d'entiers en une nouvelle liste d'entiers
  final nvLstInt = transforme<int, int>(lstInt, (x) => x * x);
  print("Liste de départ = $lstInt // liste transformée  = $nvLstInt");

  // on appelle transforme pour créer une liste d'entier (retour) depuis une liste de double (entrée)
  final nvLstDbl = transforme<double, int>(lstDbl, (x) => x.round());
  print("Liste de départ = $lstDbl // Liste transformée = $nvLstDbl");
}
```

[Code](codes/generique2.dart)

## 6-Méthode forEach()

```forEach()``` est une méthode utilisable sur les collections. On appelle cette méthode sur une instance d'une collection qui implémente la class ```Iterable``` et l'on passe en argument une fonction. ```forEach()``` **ne retourne aucune valeur**.

Syntaxe :

```dart
collection.forEach((element) {
  expression;
  ...
});

// S'il n'y a qu'une seule expression
// on peut utiliser la fat arrow :
collection.forEach((element) => expressionUnique);

// si on utilise une seule fonction
collection.forEach(fonction);
```

Exemples :

```dart
main(List<String> args) {
  const lst = [1, 2, 3, 4];

  lst.forEach((element) {
    print(element);
  });

  // Réécriture avec la fat arrow
  lst.forEach((element) => print(element));

  // Réécriture simplifiée
  lst.forEach(print);
}
```

L'utilisation de la dernière écriture n'est possible que si le type du paramètre de la fonction passée en argument de ```forEach()``` a une position équivalente (types équivalents) ou suffisamment élevée dans la hiérarchie des types. Ici, ```print``` prend en paramètre un type ```Object``` or, notre liste est de type ```int``` et ```int``` est un sous-type d'```Object```.

[Code](codes/code4.dart)

## 7-Méthode map

Cette méthode permet d'appliquer une fonction à une collection et retourne une nouvelle collection modifiée par la fonction passée en argument.

Les syntaxes possibles sont :

```dart
const lst = [1, 2, 3, 4];
final lst2 = lst.map((e) {
  return e * e;
});
```

Cette version peut se condenser avec la version *fat arrow* :

```dart
// version fat arrow
final lst2 = lst.map((e) => e * e);
print(lst2);
```

Ou

```dart
int carre(int valeur) {
  return valeur * valeur;
}

// fonction encore plus concise respectant le type de l'argument de map()
// à savoir que le type de l'argument de la fonction passé à map retourne le même type !
// ici notre liste prend des entiers. Notre fonction carre prend en argument 
// un int et retourne un int
final lst2 = lst.map(carre);
```

### Quelques remarques importante

```map()``` est fonction dite paresseuse.

Le type de la collection retournée par ```map()``` n'est pas ```List``` mais ```Iterable``` ! Donc pour convertir le type ```Iterable```, il faudra appeler la méthode ```toList()```. Dans notre code ci-dessus, ```lst2``` est de type ```Iterable<int>``` donc pour la convertir directement en ```List<int>``` :

```dart
lst2 = lst.map(carre).toList();
```

[Code](codes/code5.dart)

## 8-Méthodes where et firstWhere

### 1-where()

Cette méthode permet de filtrer une collection et de retourner une nouvelle collection, **attention** de type **```Iterable```** ! Donc ```where()``` ne retournent pas une collection du même type que la collection d'origine mais le type supérieur dans la hiérarchie : ```Iterable```.

Pour être utilisée, ```where()``` prend en argument une fonction de type ```bool Function(T)``` où T est de même type que les valeurs de la collection.

Exemple : filtrage sur des nombres pairs.

```dart
main(List<String> args) {
  const lst = [1, 2, 3, 4, 5, 6];

  final newList = lst.where((element) => element % 2 == 0);
  
  print(lst);
  // Retourne : [1, 2, 3, 4, 5, 6]
  print(newList);
  // Retourne : (2, 4, 6)
}
```

### 2-firstWhere()

La différence avec la méthode précédente est qu'elle ne retourne pas de collection ```Iterable``` mais une valeur.

Cette méthode prend au plus deux arguments de type fonction :

- La première va vérifier un prédicat et retournera un booléen (comme pour ```where()``` : ```bool Function(T)```
- La seconde est une fonction optionnelle mais elle permet de gérer le cas où aucune solution n'a été trouvée : ```int Function()?``` .Si cette fonction est omise alors une erreur sera retournée de type ```stateError```

Si une valeur au moins satisfait le prédicat (la valeur trouvée correspond à la condition), alors elle est retournée et il n'y a plus d'évaluation du reste de la collection. Sinon on a deux cas :

#### 1-erreur non gérée

Exemple : nous reprenons notre liste ci-dessus et on recherche la valeur 10 (qui n'existe pas) :

```dart
final elem = lst.firstWhere((element) => element == 10);
print(elem);
```

Et on obtient une erreur :

```txt
Unhandled exception:
Bad state: No element
```

On pourrait gérer ce problème avec un ```try/catch``` mais on peut aussi gérer ce problème en fournissant une seconde fonction.

#### 2-gestion avec une seconde fonction

Pour cela on dispose d'un argument nommé ```orElse``` de type ```int Function()?```.

Exemple :

```dart
final elem = lst.firstWhere(((element) => element == 10), orElse: () => -1);
print(elem);
```

Nous n'avons plus d'erreurs ; la méthode ```whereFirst()``` nous aura retourné ```-1```.

[Code](codes/code6.dart)

Exercice 1 : recréer la méthode ```where()``` sous forme de fonction pour les listes uniquement. [Solution](codes/exo1.dart)

Exercice 2 : réimplémentation de ```whereFirst()```. Comme précédemment ce sera une fonction applicable uniquement sur des listes. Attention, il faudra deux arguments dont le ```orElse``` qui sera nommé. ***Ce dernier ne sera pas un argument optionnel mais nommé et obligatoire (required)*** (cf. révision du [chapitre8](../Chapitre-8/chapitre8.md#4-arguments-nommés-et-required)). [Solution](codes/code2.dart)

## 9-Méthode reduce()

Si l'on vient des langages fonctionnels comme Haskell, OCaml, F# et consort, nous avons l'habitude des fonctions de type Fold, FoldLeft ou FoldRight. Ici, nous disposons de la méthode ```reduce()```.

Objet de cette méthode : on prend une collection, on combine tous les éléments de cette collection et la méthode nous retourne une seule valeur.

Cette méthode prend un seul argument : une fonction de type ```T Function(T, T)``` et cette fonction anonyme doit prendre deux arguments du même type. Le premier est un **accumulateur**. Cet accumulateur c'est une variable qui va se charger de stocker la valeur à retourner à la fin des opérations. Cette variable se modifie à chaque itération puisqu'elle résulte de l'opération de combinaison. La combinaison à réaliser est fournie par le code à implémenter de la fonction anonyme. Cet accumulateur est initialisé avec la première valeur de la collection. Le second argument est la valeur de l'item suivant, s'il existe. S'il n'existe pas alors, l'accumulateur est retourné.

Exemple : faire la somme des éléments d'une liste d'entiers.

```dart
const lst = [1, 2, 3];
final somme = lst.reduce((value, element) => value + element);
print(somme);
```

Le résultat est ```6``` : c'est la somme de ```1 + 2 + 3```.

[Code incluant un code "plus complexe"](codes/code7.dart)

## 10-Combinaisons de méthodes

La combinaison de ces méthodes issues de la programmation permet d'effectuer des traitements évolués en peu de temps.

Exemple : extraire d'une collection d'adresses mails selon une autre collection de nom de domaine. La règle est de récupérer les noms de domaine inconnus.

```dart
Iterable<String> RechercheDomainesInconnus(
    List<String> emails, List<String> domaines) {
  final rslt = emails
      .map((email) => email.split('@').last)
      .where((dns) => !domaines.contains(dns));

  return rslt;
}
```

Dans un premier temps, on va appliquer (```map```) une fonction de séparation de strings en récupérant seulement le dernier élément. Puis on utilise ```where``` pour rechercher si le nom de domaine récupéré avec ```map``` existe dans la collection des noms de domaine. Le résultat de cette extraction sera une collection de type ```Iterable```.

L'avantage de la combinaison de ces méthodes issues de la programmation fonctionnelle permettent :

- un code plus léger, plus facile à lire
- une code plus facilement maintenable

Il existe en Dart de nombreuses autres méthodes fonctionnelles pour les collections : ```skip```, ```take```, ```any```, etc.

[Code](codes/code8.dart)
