main(List<String> args) {
  const lst = [1, 2, 3, 4];

  // map
  var lst2 = lst.map((e) => e * e);
  print("Ceci n'est pas une liste : $lst2");
  int carre(int valeur) {
    return valeur * valeur;
  }

  lst2 = lst.map(carre).toList();
  print("Conversion de l'Iterable en List  = $lst2");
}
