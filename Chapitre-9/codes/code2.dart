//typedef Greet = String Function(String);
// version concise
typedef String Greet(String);

// Déclinaison de la signature
String salut(String nom) => "Salut, ${nom}";

String agur(String nom) => "Agur, ${nom}";

String ciao(String nom) => "Ciao, ${nom}";

main(List<String> args) {
  welcome("Alice", (n) => "Hello, ${n}");
}

void welcome(String nom, Greet greet) {
  print(greet(nom));
}
