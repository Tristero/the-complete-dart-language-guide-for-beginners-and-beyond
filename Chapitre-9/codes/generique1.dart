main(List<String> args) {
  const lstInt = [1, 2, 3];
  const lstDbl = [1.0, 2.0, 3.0];

  /*
  List<int> transforme(List<int> lst, int Function(int) fonction) {
    final rslt = <int>[];
    for (var elem in lst) {
      rslt.add(fonction(elem));
    }
    return rslt;
  }
  */

  // Réécriture générique
  List<T> transforme<T>(List<T> lst, T Function(T) fonction) {
    final rslt = <T>[];
    for (var elem in lst) {
      rslt.add(fonction(elem));
    }
    return rslt;
  }

  // utilisation
  var newLstInt = transforme<int>(lstInt, (x) => x * x);

  print("Ancienne liste d'entier = $lstInt et la nouvelle liste = $newLstInt");

  var newLstDbl = transforme<double>(lstDbl, (x) => x * x);
  print("Ancienne liste d'entier = $lstDbl et la nouvelle liste = $newLstDbl");
}
