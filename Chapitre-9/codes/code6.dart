main(List<String> args) {
  const lst = [1, 2, 3, 4, 5, 6];

  // utilisation de where()
  final newList = lst.where((element) => element % 2 == 0);

  print(lst);
  print(newList);

  // utilisation de firstWhere
  final elem = lst.firstWhere(((element) => element == 10), orElse: () => -1);
  print(elem);
}
