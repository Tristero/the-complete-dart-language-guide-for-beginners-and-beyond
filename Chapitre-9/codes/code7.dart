main(List<String> args) {
  /*const lst = [1, 2, 3];
  final somme = lst.reduce((value, element) => value + element);
  print(somme);*/

  // somme des nombres pairs
  const lst2 = [1, 2, 3, 4, 5, 6];
  final sommePairs = lst2.reduce((value, element) {
    print("V=$value et E=$element");
    // suppression de la première itération où valeur = 1
    if (value == 1) {
      return element;
    }
    if (element % 2 == 0) {
      print("!! V=$value et E=$element");
      return element + value;
    }
    return value;
  });

  print(sommePairs);
}
