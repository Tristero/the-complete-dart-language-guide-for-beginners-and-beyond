main(List<String> args) {
  const emails = [
    'abc@abc.org',
    'me@example.com',
    'johndoe@gmail.com',
    'alice@yahoo.com',
    'bob@bobinc.com',
  ];
  const domaines = [
    'gmail.com',
    'yahoo.com',
  ];

  final domainesInconnus = RechercheDomainesInconnus(emails, domaines);
  print(domainesInconnus);
}

Iterable<String> RechercheDomainesInconnus(
        List<String> emails, List<String> domaines) =>
    emails
        .map((email) => email.split('@').last)
        .where((dns) => !domaines.contains(dns));
