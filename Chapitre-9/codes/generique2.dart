main(List<String> args) {
  const lstInt = [1, 2, 3];
  const lstDbl = [1.0, 2.0, 3.0];

  // on appelle transforme pour créer une liste d'entiers en une nouvelle liste d'entiers
  final nvLstInt = transforme<int, int>(lstInt, (x) => x * x);
  print("Liste de départ = $lstInt // liste transformée  = $nvLstInt");

  // on appelle transforme pour créer une liste d'entier (retour) depuis une liste de double (entrée)
  final nvLstDbl = transforme<double, int>(lstDbl, (x) => x.round());
  print("Liste de départ = $lstDbl // Liste transformée = $nvLstDbl");
}

List<R> transforme<T, R>(List<T> liste, R Function(T) fonction) {
  List<R> rslt = <R>[];

  for (var element in liste) {
    rslt.add(fonction(element));
  }

  return rslt;
}
