main(List<String> args) {
  const lst = [1, 2, 3, 4];

  // Différentes écritures de forEach
  lst.forEach((element) {
    print(element);
  });

  lst.forEach((element) => print(element));

  lst.forEach(print);
}
