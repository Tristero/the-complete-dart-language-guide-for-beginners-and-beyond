main(List<String> args) {
  final hello = (String nom) => "Hello, my name is ${nom}";

  print(hello("John"));

  // fonctions comme argument
  // avec une fonction lambda
  welcome("Jane", (str) => str.toUpperCase());

  // avec une fonction "déportée"
  final fun = (String str) {
    final lng = str.length;
    return "Le mot comporte ${lng} caractères";
  };

  welcome("bibule", fun);
}

void welcome(String nom, String Function(String) greet) {
  print(greet(nom));
}
