main(List<String> args) {
  const liste = [1, 2, 3, 4, 5, 6];

  int t1 = FirstWhere(liste, (element) => element == 2, orElse: () => -1);
  assert(t1 == 2);

  // gestion de l'erreur
  int t2 = FirstWhere(liste, (elem) => elem == 10, orElse: () => -1);
  assert(t2 == -1);
}

typedef T DefaultValue<T>();

T FirstWhere<T>(List<T> liste, bool Function(T) predicat,
    {required T Function() orElse}) {
  // on évite forEach qui itère sur chaque élément.
  // ici on se concentre uniquement sur la première valeur trouvée
  // et on arrête
  for (var element in liste) {
    if (predicat(element)) {
      return element;
    }
  }

  return orElse();
}
