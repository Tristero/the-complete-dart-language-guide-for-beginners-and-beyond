// implémentation d'un where sous forme d'une fonction
// doit fonctionner avec une liste de n'importe quel type
// doit retourner une nouvelle liste du même type
main(List<String> args) {
  const lst = <int>[1, 2, 3, 4, 5];
  final rslt = Where(lst, (element) => element == 10);
  print(rslt);
}

List<T> Where<T>(List<T> liste, bool Function(T) predicat) {
  List<T> rslt = [];
  liste.forEach((element) {
    if (predicat(element)) {
      rslt.add(element);
    }
  });
  return rslt;
}
