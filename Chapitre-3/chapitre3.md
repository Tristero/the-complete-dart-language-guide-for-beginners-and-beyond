# Structures de contrôle

Nous verrons les structures conditionnelles ```if```/```else```, les boucles ```while``` et ```for```, les déclarations ```switch``` et les énumérations.

## 1-Conditions IF/ELSE

La syntaxe est identique à ce que l'on trouve traditionnellement dans les langages issus du C :

```dart
if (condition) {
    ...
} else if (condition) {
    ...
} else {
    ...
}
```

## 2-Boucle WHILE

Syntaxe simple :

```dart
while (condition) {
    ...
}
```

[Code](codes/code1.dart)

## 3-Boucles FOR

Syntaxe exactement la même que pour C#, Java et consort :

```dart
for (initialisation; condition limite; pas) {
    ...
}
```

Ne pas oublier à l'initialisation soit de typer la variable, soit d'indiquer ```var``` !

Comme en C ou Go, nous pouvons *oublier* des éléments dans la création du for :

```dart
main(List<String> args) {
  // la condition limite a été déplacée
  // dans le corps du code de la boucle
  for (int i = 1;; i++) {
    if (i == 6) {
      break;
    }
    print("*" * i);
  }
}
```

La boucle for peut s'écrire un peu à la manière d'un Kotlin, Python même Go sur le parcours de collections :

```dart
for (var i in [1, 2, 3]){
    print(i);
}
```

[Code](codes/code2.dart)

## 4-break et continue

Comme en C :

- ```break``` sort immédiatement de toute boucle,
- ```continue``` stoppe l'itération en cours et passe à la suivante mais ne sort pas de la boucle.

## 5-SWITCH

### 1-Syntaxe

```dart
switch(constante) {
    case VALEUR:
        ...
        break;
    case VALEUR2:
        ...
        break;
    default:
        ...
        break; //optionnel
}
```

### 2-Cas vides

Mais Dart supporte les **cas vides** :

```dart
var command = 'CLOSED';
switch (command) {
  case 'CLOSED': // Cas vide et le suivant est exécuté
  case 'NOW_CLOSED':
    // fonctionne à la fois pour CLOSED et NOW_CLOSED
    executeNowClosed();
    break;
}
```

### 3-Labels

La notion de **label** permet de sauter d'un cas vers un autre :

```dart
var command = 'CLOSED';
switch (command) {
  case 'CLOSED':
    executeClosed();
    continue nowClosed;
  // continue vers le label nowClosed

  nowClosed:
  case 'NOW_CLOSED':
    // fonctionne à la fois pour CLOSED et NOW_CLOSED
    executeNowClosed();
    break;
}
```

### 4-Terminer un cas

Il existe d'autres manières d'achever un *case* : on a vu ```continue``` suivi d'un label, on peut utiliser les mots-clés ```throw```, ou ```return```.

**RAPPEL** : le cas ```default``` est le seul où ```break``` est **optionnel**.

[Documentation sur switch](https://dart.dev/guides/language/language-tour#switch-and-case)

## 6-Enumérations

Nombre de langages supporte les énumérations.

### 1-Deux syntaxes

```dart
enum NomEnumeration {
    valeur1
    valeur2
    ...
    dernièreValeur,
}
```

ou

```dart
enum NomEnumeration{ valeur1, valeur2,..., derniereValeur}
```

**Attention** la dernière virgule pour le formatage !

### 2-Initialisation, utilisation et indexation

Une énumération ne peut se créer dans une fonction ou une méthode : c'est un type (particulier).

Son utilisation est comme une classe statique : on appelle le nom de l'énumération suivi d'un point puis de la valeur.

Chaque élément de l'énumération possède un index débutant à 0. On peut voir ces éléments comme des accesseurs/getters. Nous verrons dans le prochain chapitre comment s'en servir.

[Code](codes/code3.dart)
