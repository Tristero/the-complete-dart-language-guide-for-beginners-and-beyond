main(List<String> args) {
  var rslt = "*";

  while (rslt.length < 6) {
    print(rslt);
    rslt += "*";
  }

  // Alternative
  var i = 1;
  while (i < 6) {
    print("*" * i);
    i++;
  }
}
