// reprise du code1.dart

main(List<String> args) {
  for (int i = 1; i < 6; i++) {
    print("*" * i);
  }

  for (int i = 1;; i++) {
    if (i == 6) {
      break;
    }
    print("*" * i);
  }

  for (var i in [1, 2, 3]) {
    print(i);
  }
}
