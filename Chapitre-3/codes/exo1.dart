main(List<String> args) {
  const netSalary = 1500;
  const expenses = 1500;

  if (netSalary > expenses) {
    print("You have saved \$${netSalary - expenses} this month");
  } else if (expenses > netSalary) {
    print("You have lost \$${expenses - netSalary} this month");
  } else {
    print("Your balance hasn't changed");
  }
}
