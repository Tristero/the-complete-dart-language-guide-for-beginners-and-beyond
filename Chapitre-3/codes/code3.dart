enum Couleur {
  rouge,
  vert,
  bleu,
}

main(List<String> args) {
  const c = Couleur.bleu;

  switch (c) {
    case Couleur.rouge:
      print("La couleur est rouge");
      break;
    case Couleur.bleu:
      print("La couleur est bleue");
      break;
    default:
      print("La couleur est verte");
  }
}
