# POO - les bases

Objectifs :

- créer une classe
- les constructeurs
- les membres : variables et méthodes
- accesseurs/mutateurs
- mot-clé *static*
- mot-clé *private*

## 1-Syntaxe

### 1-Création

```dart
class NomDeLaClasse {}
```

Nomenclature : un nom de classe utilise la convention en *upper CamelCase*.

### 2-Instanciation

```dart
NomDeLaClasse classe = new NomDeLaClasse();

// Version alternative concise
final classe = NomDeLaClasse();
```

### 3-Membres

Sans utilisation de mots limitant la portée des membres, une variable crée dans une classe est publique :

```dart
class Compte {
  double solde = 0;
}

main(List<String> args) {
  final compte = Compte();
  print(compte.solde);
  compte.solde += 500;
  print(compte.solde);
}
/*
Le résultat affiché sera
0.0
500.0
*/
```

[Code](codes/code1.dart)

## 2-Méthodes d'instance

La syntaxe d'une méthode d'instance est identique à celle d'une fonction Dart : un type de retour suivi du nom de la méthode et ses arguments typés si besoin.

Exemple :

```dart
class Compte {
  double solde = 0;
  double limite = 250;

  void Deposer(double montant) {
    solde += montant;
  }

  bool Retirer(double montant) {
    if (montant >= limite || montant >= solde) {
      return false;
    }
    solde -= montant;
    return true;
  }
}
```

**Remarques** :

- Par défaut les méthodes sont publiques.
- Le retour par des booléens pour gérer des erreurs n'est pas très pertinent : il conviendra d'utiliser les exceptions

[Code](codes/code2.dart)

## 3-Constructeurs

### 1-Syntaxe pour un constructeur

Un constructeur a pour syntaxe :

```dart
class NomDeLaClasse {

    NomDeLaClasse(Type args) {
        this.args = args;
    }

    Type args;
}
```

Il existe une version plus concise de créer un constructeur en utilisant une ***liste d'initialisateurs*** :

```dart
class NomDeLaClasse {
    Type args;

    NomDeLaClasse(Type args) : args = args;
}
```

Exemple :

```dart
class Compte {
  double solde = 0;
  double limite = 250;

  Compte(double solde, double limite)
      : solde = solde,
        limite = limite;

  /*
  Version "classique" :

  Compte(double solde, double limite) {
    this.solde = solde;
    this.limite = limite;
  }
  */

  // ...
  // reprise du code
}
```

### 2-Pas de surcharge directe

Mais contrairement à d'autres langages, on ne peut pas surcharger directement un constructeur. Le code suivant génère une erreur :

```dart
class Compte {

  Compte(double solde) {
    this.solde = solde;
  }

  Compte(double solde, double limite) {
    Compte(solde);
    this.limite = limite;
  }
  ...
}
```

Il existe des solutions paliant à ce déficit de surcharge du constructeur : [named constructor](#1-named-constructors) (que l'on introduira dans la partie 7 ci-dessous) et les [factory constructor](../Chapitre-11/chapitre11.md#7-factory-constructors) (dans le prochain chapitre).

### 3-Arguments nommés avec valeurs par défaut

On entoure l'argument d'accolades et on lui affecte une valeur par défaut :

```dart
class NomDeLaClasse {
    Type arg

    NomDeLaClasse({Type arg = X}) : arg = arg;
}

// Instanciation
final classe = NomDeLaClasse(arg: Y);
```

Exemple :

```dart
class Compte {
  double solde;
  double limite;

  Compte(double solde, {double limite = 100})
      : solde = solde,
        limite = limite;
  
 
  ...
}

// Instanciation et utilisation de limite
final c = Compte(limite: 250);
```

### 4-required

On peut utiliser le mot-clé ```required``` si on veut nommer **sans** valeur par défaut :

```dart
class NomDeLaClasse {
    Type arg

    NomDeLaClasse({required Type arg}) : arg = arg;
}

// Instanciation
final classe = NomDeLaClasse(arg: Y);
```

Exemple :

```dart
class Compte {
  double solde;
  double limite;

  Compte({required double solde, double limite = 100})
      : solde = solde,
        limite = limite;
  
  ...
}

// instanciation
final c = Compte(solde: 500, limite: 250);
```

[Code](codes/code3.dart)

### 5-Constructeur concis

Il est possible d'aller plus loin dans la concision :

```dart
class NomDeLaClasse {
    Type arg;
    Type arg2;

    NomDeLaClasse({
        required this.arg, 
        this.arg2 = X,
    });
}
```

Exemple :

```dart
class Compte {
  double solde;
  double limite;

  Compte({required this.solde, this.limite = 100});

  ...
}

// instanciation : ne change pas
final c = Compte(solde: 500, limite: 250);
```

### 6-const : classe constante

Il est possible de créer des instance de classe comme des constantes. Pour cela il faut respecter certains critères :

- ```const``` doit précéder le constructeur
- toutes les propriétés de la classe doivent être ```final``` (lecture seule).

Exemple :

```dart
class Complexe {
  final int reel;
  final int imaginaire;

  const Complexe(this.reel, this.imaginaire);
}

void main() {
  const nbComplexe = Complexe(1, 2);
}
```

Avec un constructeur sous la forme d'une constante, cela permet au compilateur d'optimiser le code en retour

### 7-Surcharges de constructeurs indirectes

En fait, il existe deux alternatives pour créer des surcharges de constructeurs :

- les constructeurs nommés ou *named constructors*.
- les constructeurs dits *factory* (il s'agit du design pattern éponyme)

#### 1-Named constructors

Nous allons reprendre l'exemple des nombres complexes. Il existe 4 types de complexes spéciaux : 0, l'identité où la partie imaginaire vaut 0 et le réel vaut 1, le réel (la partie imaginaire vaut 0) et l'imaginaire (la partie réelle vaut 0).

Donc il serait utile de pouvoir créer des instances de Complexe plus simplement en ayant un accès direct à l'une de ces quatre possibilités.

Premier cas : zéro. On appelle le constructeur dans la classe suivi d'un point suivi d'un nom avec des parenthèses. Dans ces parenthèses, on va pouvoir enlever ou non, rajouter ou non des arguments et, hors de parenthèses, initialiser les arguments du constructeur si l'on souhaite. Mais **attention** l'ajout d'arguments ne pourra pas se faire avec des constructeurs-constantes :

Exemple :

```dart
class Complexe {
  final int reel;
  final int imaginaire;

  const Complexe(this.reel, this.imaginaire);
  // constructeurs nommés
  const Complexe.zero()
      : reel = 0,
        imaginaire = 0;
  const Complexe.identite()
      : reel = 1,
        imaginaire = 0;
  const Complexe.reel(this.reel) : this.imaginaire = 0;
  const Complexe.imaginaire(this.imaginaire) : this.reel = 0;

  void PrintComplexe() {
    print("${this.reel}+i${this.imaginaire}");
  }
}
```

**ATTENTION** ici notre constructeur est une constante. Donc tous les autres constructeurs nommés seront **aussi** ```const``` !

**NB** : la nomenclature d'écriture pour les constructeurs nommés n'est pas en *upper* CamelCase mais en ***lower* camelCase**.

Et les appels sont simples :

```dart
const id = Complexe.identite();
id.PrintComplexe();
```

[Code](codes/code7.dart)

#### Named constructors et opérations

les constructeurs nommés ne servent pas qu'à faire de la surcharge de constructeurs. Ils peuvent être utilisés comme tels sans la création d'un constructeur particulier.

De même les constructeurs nommés peuvent effectuer des opérations.

Nous allons prendre le cas des températures. Nous souhaitons manipuler deux types d'échelles : les degrés Celsius et Fahrenheit.

Nous allons créer dans un premier temps une variable d'instance stockant une valeur en Celsius et un constructeur nommé qui prendra en paramètre une valeur Celsius :

```dart
class Temperature {
  double celsius;

  Temperature.celsius(this.celsius);
}
```

Maintenant, nous allons créer un constructeur qui va prendre une valeur Fahrenheit et effectuera une convertion :

```dart
class Temperature {
  double celsius;

  Temperature.celsius(this.celsius);

  Temperature.fahrenheit(double fahrenheit) : celsius = (fahrenheit - 32) / 1.8;
}
```

[Code](codes/code8.dart)

## 4-Etats d'une classe

Il est souvent demander à ce que certains membres ne soient pas modifiables.

Dans nos exemples, nous n'avons pas associé un compte à une personne. Or ce champ ne doit pas être modifié a posteriori :

```dart
class Compte {
  Compte({
    required this.proprietaire,
    required this.solde,
    this.limite = 100,
  });

  String proprietaire;
  double solde;
  double limite;

  ...
}

// utilisation
final c = Compte(proprietaire: "Alice", solde: 500, limite: 200);
c.proprietaire = "Bob";
// aucune erreur
```

Pour empêcher ce type de situation, on utilisera le mot-clé ```final``` lors de la création de la variable d'instance ```proprietaire``` :

```dart
class Compte {
  Compte({
    required this.proprietaire,
    required this.solde,
    this.limite = 100,
  });

  final String proprietaire;
  double solde;
  double limite;

  ...
}

final compte = Compte(
  proprietaire: "Alice",
  solde: 500,
  limite: 250,
);

// Le code lèvera une erreur :
compte.proprietaire = "Bob";
```

```final``` permet de créer facilement un accesseur (getter) non mutateur.

[Code](codes/code5.dart)

## 5-Accesseurs et Mutateurs

Intérêt : la protection en lecture et/ou en écriture d'une variable locale.

Dans les présentations ci-dessous, nous effectuerons des opérations dans ces accesseurs/mutateurs.

### 1-Accesseurs ou getters

le mot-clé ```get``` est utilisé avec une variable suivi d'un corps de fonction (on peut utiliser les accolades ou la fat arrow).

Nous allons transformer le code suivant avec un getter :

```dart
class Temperature {
  double celsius;

  Temperature.celsius(this.celsius);
  Temperature.fahrenheit(double fahrenheit) : celsius = (fahrenheit - 32) / 1.8;
}
```

On crée une variable ```fahrenheit``` et elle devient un accesseur avec le mot-clé ```get``` et on lui affecte un code :

```dart
double get fahrenheit => celsius = (fahrenheit - 32) / 1.8;
```

Puis on applique cette méthode à notre constructeur nommé :

```dart
class Temperature {
  double celsius;
  double get fahrenheit => celsius = (fahrenheit - 32) / 1.8;

  Temperature.celsius(this.celsius);
  Temperature.fahrenheit(double fahrenheit) : celsius = fahrenheit;
}
```

**NB** tout getter est une méthode d'instance ! Donc la présence d'un bloc de code est obligatoire. Sans sa présence le compilateur lévera une erreur.

[Code](codes/code9.dart)

### 2-Mutateurs ou setters

Un setter est une fonction débutant avec le mot-clé ```set``` suivi du nom de la variable devenant un mutateur suivi de parenthèse avec un argument et, enfin, suivi d'un bloc d'instructions.

```dart
set fahrenheit(double fahrenheit) =>
      celsius = FahrenheitToCelsius(fahrenheit);
```

Nous modifions le code de conversion :

```dart
class Temperature {
  double celsius;
  double get fahrenheit => this.celsius * 1.8 + 32;

  set fahrenheit(double fahrenheit) =>
      celsius = FahrenheitToCelsius(fahrenheit);

  Temperature.celsius(this.celsius);
  Temperature.fahrenheit(double f) : celsius = FahrenheitToCelsius(f);

  static double FahrenheitToCelsius(double valeurFahrenheit) =>
      (valeurFahrenheit - 32) / 1.8;
}
```

Son utilisation :

```dart
final temp = Temperature.celsius(30);

print(temp.celsius);
print(temp.fahrenheit);
temp.fahrenheit = 451;
print(temp.celsius);
```

[Code](codes/code9.dart)

## 6-Méthodes et variables statiques

### 1-Méthodes statiques

Dans l'exemple précédent nous avons introduit une méthode statique ou méthode de classe (rappel : c'est une méthode accessible hors instanciation).

On utilise le mot-clé ```static``` suivi d'une méthode traditionnelle.

Exemple : la gestion de convertion de température 

```dart
class Temperature {
  static double FahrenheitToCelsius(double fahrenheit) =>
      (fahrenheit - 32) / 1.8;

  static double CelsiusToFahrenheit(double celsius) => celsius * 1.8 + 32;
}

main(List<String> args) {
  print("${Temperature.CelsiusToFahrenheit(25)}°F");

  print("${Temperature.FahrenheitToCelsius(452)}°C");
}
```

[Code](codes/code10.dart)

### 2-Variables statiques

Tout comme les méthodes, nous pouvons créer des variables statiques. En général on associe les mot-clés ```static``` et ```const``` :

```dart
static const PI = 3.14159;
```

Une méthode d'instance peut accéder à des variables statiques de sa propre classe :

```dart
class Hello {
  static const hello = "Hello";
  String nom;

  Hello(this.nom);

  void afficheHello() {
    print("$hello, ${nom}");
  }
}

main(List<String> args) {
  final hello = Hello("Alice");
  hello.afficheHello();
}
```

Version plus alambiquée (et inutile avec des accesseurs et mutateurs) :

```dart
class Hello {
  // Valeur accessible pour une instance aussi
  static const hello = "Hello";

  // confère le chapitre 7 - variables privées
  String? _nom;
  
  set nom(String valeur) => _nom = valeur;
  String get nom => this._nom ?? "Unknown";

  void afficheHello() {
    print("$hello, ${nom}");
  }
}
```

[Code](codes/code11.dart)

## 7-Variables et méthodes privées

### 1-Variables privées

Exemple : compte bancaire.

Le code suivant n'empêche pas l'accès et surtout la modification de la variable ```solde``` :

```dart
class Compte {
  double solde;

  Compte(this.solde);

  void Deposer(double montant) {
    this.solde += montant;
  }

  void Retirer(double montant) {
    if (solde > montant) {
      this.solde -= montant;
    }
  }
}

void Main() {
  final c = Compte(100);
  c.solde = 10000;
}
```

En Dart, il n'y a pas de mot-clé ```private``` comme en C# ou Java (de même qu'il n'existe pas de ```protected```).

La seule manière est celle de Python : on utilise le underscore. Mais comme en Python, la variable reste visible en-dehors de la classe SI ET SEULEMENT SI la classe n'est pas séparé de son appel : autrement dit, il faut un fichier pour la classe intégrant une variable privée et un autre pour une fonction ou une classe appelant notre première classe :

Si on place dans un même fichier [code12.dart](codes/code12.dart) :

```dart
class Compte {
  double _solde;

  Compte(this._solde);

  void Deposer(double montant) {
    this._solde += montant;
  }

  void Retirer(double montant) {
    if (_solde > montant) {
      this._solde -= montant;
    }
  }
}

main(List<String> args) {
  final compte = Compte(100);
  compte._solde = 10000;
  print(compte._solde);
}
```

La modification est possible. En revanche, si on place ```Compte``` dans un fichier (par exemple, compte.dart) à part et notre point d'entrée ```Main``` (par exemple, prog.dart), alors notre variable n'est plus accessible. **Attention**, il faut importer notre classe ```Compte``` : dans notre cas, nous avons placé les deux codes au même endroit.

Code de [compte.dart](codes/compte.dart)

```dart
class Compte {
  double _solde;

  Compte(this._solde);

  void Deposer(double montant) {
    this._solde += montant;
  }

  void Retirer(double montant) {
    if (_solde > montant) {
      this._solde -= montant;
    }
  }
}
```

Le code de [prog.dart](codes/prog.dart) :

```dart
import 'compte.dart';

main(List<String> args) {
  final compte = Compte(100);
  // impossible de faire 
  // compte._solde = 10000;
  compte.Deposer(10000);
}
```

Tout l'intérêt des getters/setters est ici : on va pouvoir utiliser notamment un getter pour retourner la valeur de ```_solde```.

```dart
class Compte {
  double _solde;
  double get solde => this._solde;

  ...
}
```

Et nous pouvons afficher le solde directement dans ```Main``` :

```dart
main(List<String> args) {
  final compte = Compte(100);
  // Dorénavant il est impossible de taper :
  // compte._solde = 10000;
  compte.Deposer(1000);
  print(compte.solde);
}
```

### 2-Classes, méthodes privées

Il est possible avec Dart de créer des classes et des méthodes privées juste en plaçant devant le nom un underscore !
