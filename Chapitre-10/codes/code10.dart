class Temperature {
  static double FahrenheitToCelsius(double fahrenheit) =>
      (fahrenheit - 32) / 1.8;

  static double CelsiusToFahrenheit(double celsius) => celsius * 1.8 + 32;
}

main(List<String> args) {
  print("${Temperature.CelsiusToFahrenheit(25)}°F");

  print("${Temperature.FahrenheitToCelsius(452)}°C");
}
