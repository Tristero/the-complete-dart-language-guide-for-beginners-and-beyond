class Complexe {
  const Complexe(this.reel, this.imaginaire);
  final int reel;
  final int imaginaire;

  void PrintComplexe() {
    print("${this.reel}+i${this.imaginaire}");
  }
}

main(List<String> args) {
  const nb = Complexe(1, 2);
  nb.PrintComplexe();
}
