main(List<String> args) {
  final p1 = Person(name: "Alice", age: 42, height: 1.64);
  p1.printDescription();

  final p2 = Person(name: "Bob", age: 24, height: 1.80);
  p2.printDescription();
}

class Person {
  final String name;
  int age;
  double height;

  Person({
    required this.name,
    required this.age,
    required this.height,
  });

  void printDescription() {
    print(
        "My name is ${this.name}. I'am ${this.age} years old, I'm ${this.height} meters tall.");
  }
}
