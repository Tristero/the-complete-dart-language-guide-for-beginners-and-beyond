class Compte {
  double _solde;
  double get solde => this._solde;

  Compte(this._solde);

  void Deposer(double montant) {
    this._solde += montant;
  }

  void Retirer(double montant) {
    if (_solde > montant) {
      this._solde -= montant;
    }
  }
}
