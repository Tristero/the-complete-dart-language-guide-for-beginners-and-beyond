class Compte {
  double _solde;

  Compte(this._solde);

  void Deposer(double montant) {
    this._solde += montant;
  }

  void Retirer(double montant) {
    if (_solde > montant) {
      this._solde -= montant;
    }
  }
}

main(List<String> args) {
  final compte = Compte(100);
  compte._solde = 10000;
  print(compte._solde);
}
