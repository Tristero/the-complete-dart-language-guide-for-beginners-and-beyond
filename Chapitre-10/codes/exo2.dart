main(List<String> args) {}

class Restaurant {
  // get the total number of ratings
  // calculate the average rating (loop or reduce)

  final String name;
  final String cuisine;
  final List<double> ratings;

  const Restaurant({
    required this.name,
    required this.cuisine,
    required this.ratings,
  });

  int get sumRatings => this.ratings.length;

  double? AverageRatings() =>
      this.ratings.reduce((value, element) => value + element) / sumRatings;
}
