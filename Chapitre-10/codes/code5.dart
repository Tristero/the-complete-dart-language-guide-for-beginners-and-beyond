class Compte {
  Compte({
    required this.proprietaire,
    required this.solde,
    this.limite = 100,
  });

  final String proprietaire;
  double solde;
  double limite;

  void Deposer(double montant) {
    solde += montant;
  }

  bool Retirer(double montant) {
    if (montant >= limite || montant >= solde) {
      return false;
    }
    solde -= montant;
    return true;
  }
}

main(List<String> args) {
  final compte = Compte(
    proprietaire: "Alice",
    solde: 500,
    limite: 250,
  );

  // impossible d'effectuer :
  // compte.proprietaire = "Bob";
  final rslt = compte.Retirer(500);
  print("Retrait accordé ? $rslt - Solde après opération = ${compte.solde}");
}
