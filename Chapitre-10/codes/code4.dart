class Compte {
  Compte({required this.solde, this.limite = 100});

  double solde;
  double limite;

  void Deposer(double montant) {
    solde += montant;
  }

  bool Retirer(double montant) {
    if (montant >= limite || montant >= solde) {
      return false;
    }
    solde -= montant;
    return true;
  }
}

main(List<String> args) {
  final compte = Compte(solde: 500, limite: 250);
  final rslt = compte.Retirer(500);
  print("Retrait accordé ? $rslt - Solde après opération = ${compte.solde}");
}
