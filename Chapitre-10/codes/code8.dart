main(List<String> args) {
  final temp = Temperature.celsius(30);
  final temp2 = Temperature.fahrenheit(451);
  print(temp.celsius);
  print(temp2.celsius);
}

class Temperature {
  double celsius;

  Temperature.celsius(this.celsius);
  Temperature.fahrenheit(double fahrenheit) : celsius = (fahrenheit - 32) / 1.8;
}
