class Complexe {
  final int reel;
  final int imaginaire;

  const Complexe(this.reel, this.imaginaire);
  // constructeurs nommés
  const Complexe.zero()
      : reel = 0,
        imaginaire = 0;
  const Complexe.identite()
      : reel = 1,
        imaginaire = 0;
  const Complexe.reel(this.reel) : this.imaginaire = 0;
  const Complexe.imaginaire(this.imaginaire) : this.reel = 0;

  void PrintComplexe() {
    print("${this.reel}+i${this.imaginaire}");
  }
}

main(List<String> args) {
  const nb = Complexe(1, 2);
  nb.PrintComplexe();
  const id = Complexe.identite();
  id.PrintComplexe();
}
