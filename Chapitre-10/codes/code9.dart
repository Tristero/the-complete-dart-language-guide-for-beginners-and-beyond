main(List<String> args) {
  final temp = Temperature.celsius(30);

  print(temp.celsius);
  print(temp.fahrenheit);
  temp.fahrenheit = 451;
  print(temp.celsius);
}

class Temperature {
  double celsius;
  
  double get fahrenheit => this.celsius * 1.8 + 32;

  set fahrenheit(double fahrenheit) =>
      celsius = FahrenheitToCelsius(fahrenheit);

  Temperature.celsius(this.celsius);
  Temperature.fahrenheit(double f) : celsius = FahrenheitToCelsius(f);

  static double FahrenheitToCelsius(double valeurFahrenheit) =>
      (valeurFahrenheit - 32) / 1.8;
}
