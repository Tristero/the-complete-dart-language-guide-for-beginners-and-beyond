class Hello {
  static const hello = "Hello";
  String? _nom;

  set nom(String valeur) => _nom = valeur;
  String get nom => this._nom ?? "Unknown";

  void afficheHello() {
    print("$hello, ${nom}");
  }
}

class Hello2 {
  static const hello = "Hello";
  String nom;

  Hello2(this.nom);

  void afficheHello() {
    print("$hello, ${nom}");
  }
}

main(List<String> args) {
  final h2 = Hello2("Alice");
  h2.afficheHello();
}
