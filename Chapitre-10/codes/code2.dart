class Compte {
  double solde = 0;
  double limite = 250;

  void Deposer(double montant) {
    solde += montant;
  }

  bool Retirer(double montant) {
    if (montant >= limite || montant >= solde) {
      return false;
    }
    solde -= montant;
    return true;
  }
}

main(List<String> args) {
  final compte = Compte();
  print(compte.solde);
  compte.Deposer(500.0);
  print(compte.solde);
  final rslt1 = compte.Retirer(125);
  print("Autorisation ? $rslt1 : solde = ${compte.solde}");

  final rslt2 = compte.Retirer(250);
  print("Autorisation ? $rslt2 : solde = ${compte.solde}");

  final rslt3 = compte.Retirer(125);
  print("Autorisation ? $rslt3 : solde = ${compte.solde}");
}
