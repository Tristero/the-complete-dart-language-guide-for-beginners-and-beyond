main(List<String> args) {
  const cmd = ['margherita', 'pepperoni', "pineapple"];
  print("Total = ${calculTotal(cmd)}");
}

double calculTotal(List<String> cmd) {
  const pizzaPrices = {
    'margherita': 5.5,
    'pepperoni': 7.5,
    'veggie': 6.5,
  };

  var total = 0.0;
  cmd.forEach((element) {
    var prix = pizzaPrices[element];
    if (prix == null) {
      print("La pizza ${element} n'existe pas");
      return;
    }
    total += prix;
  });

  return total;
}
