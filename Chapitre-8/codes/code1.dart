main(List<String> args) {
  print(addOption(1));
  print(addOption(1, 10));

  print(sum([]));
  print(sum([1, 2]));
  print(sum([1, 2.0, 3]));
  print(sum([1, 2, 3, 4]));

  print(presentation(nom: "Jane"));
  print(presentation(age: 42, nom: "John"));

  print(presentation2(age: 24, nom: "Jane Jr"));

  print(add(1, 2));
}

num sum(List<num> lst) {
  num total = 0;
  lst.forEach((element) {
    total += element;
  });
  return total;
}

// arguments positionnels optionnels
int addOption(int a, [int b = 2]) {
  return a + b;
}

// arguments nommés
String presentation({String nom = "", int? age}) {
  String msg = "Bonjour, ${nom}";
  if (age != null) {
    msg = "${msg}, vous avez ${age} ans.";
  }
  return msg;
}

// arguments REQUIRED
String presentation2({required String nom, required int age}) {
  return "Bonjour, ${nom}, , vous avez ${age} ans.";
}

// Fat Arrow
int add(int a, int b) => a + b;

void printAdd(int a, int b) => print(a + b);
