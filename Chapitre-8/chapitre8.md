# Fonctions

## 1-Base syntaxique

En Dart comme en C, on place en premier le type de retour (qui peut être vide : *void*) puis le nom de la fonction suivi de parenthèses regroupant, optionnellement, des paramètres typés :

```dart
Type nomFonction(Type arg1, ... Type argN){
    ...
    // return si le type de retour n'est pas void
}
```

Exemple :

```dart
num sum(List<num> lst) {
  num total = 0;
  lst.forEach((element) {
    total += element;
  });
  return total;
}

// Ce code parfaitement avec des int ET des
// double :
// print(sum([1, 2.0, 3]));
// retournera la valeur 6.0
```

Comme dans tous les langages, on peut passer en argument d'une fonction, des littéraux, des variables et des expressions.

## 2-Arguments

Les arguments peuvent être positionnels, nommés.

### 1-Arguments positionnels

Les arguments **positionnels** sont ceux que l'on trouve "par défaut" dans la très grande majorité des fonctions et méthodes : ```int add( int a, int b){}``` où les deux arguments sont dits positionnels.

### 2-Arguments positionnels optionnels

Il est possible de rendre un argument optionnel.

Syntaxe :

```dart
Type fonction(Type arg1, ..., [Type argOptionnel = valeur]){
    ...
}
```

Exemple :

```dart
int addOption(int a, [int b = 2]) {
  return a + b;
}
```

Il est possible d'appeler la fonction soit avec l'option soit sans :

```dart
print(addOption(1));
// retournera 3
print(addOption(1, 10));
// retournera 11
```

### 3-Arguments nommés

Syntaxe pour **Dart 2.10+** :

```dart
Type fonction ({Type arg1=valeur1, ..., Type? argN}) {
    ...
}

// Appel de la fonction
fonction({arg1: valeur1, ..., argN: valeurN});

// l'ordre n'est plus important dans l'appel entre les accolades :
fonction(argN: valeurN, ..., arg1: valeur1);
```

#### Caractéristiques à retenir

Dans une fonction, les arguments nommés sont toujours placés **APRES** les arguments positionnels.

Les arguments nommés sont **optionnels**.

Les arguments nommés sont regroupés entre accolades.

Avec Dart 2.10+, les types des paramètres s'ils sont typés **doivent posséder une valeur par défaut** sinon ils doivent être possiblement null. Pour cela on donnera le type suivi d'un ```?``` :par exemple,  ```int? arg``` sinon ```int arg=0```.

L'appel d'une fonction avec des arguments nommés sans accolade et l'ordre des paramètres n'est plus important car les arguments sont nommés.

Exemple :

```dart
// Ici age peut être null
String presentation({String nom = "", int? age}) {
  String msg = "Bonjour, ${nom}";
  if (age != null) {
    msg = "${msg}, vous avez ${age} ans.";
  }
  return msg;
}

main(List<String> args){
  print(presentation(nom: "Jane"));
  print(presentation(age: 42, nom: "John"));
}
```

### 4-Arguments nommés et required

Le mot-clé ```required``` pallie à l'optionalité des arguments.

Ainsi on peut réécrire le code suivant, en supprimant la nullabilité et la valeur par défaut :

```dart
String presentation({String nom = "", int? age}) {
  String msg = "Bonjour, ${nom}";
  if (age != null) {
    msg = "${msg}, vous avez ${age} ans.";
  }
  return msg;
}
```

```dart
String presentation2({required String nom, required int age}) {
  return "Bonjour, ${nom}, , vous avez ${age} ans.";
}

// Appel : l'ordre n'importe toujours pas :
void main() {
    print(presentation2(age: 24, nom: "Jane Jr"));
}
```

## 3-Fat arrow

Permet une syntaxe légère identique à celle de JS :

```dart
TypeDeRetour fonction(TypeArgument nomArg) => expression;
```

Exemple :

```dart
int add(int a, int b) => a + b;
```

Il n'y a pas besoin du mot-clé ```return```.

Avec cette syntaxe, on peut ne rien retourner :

```dart
void printAdd(int a, int b) => print(a + b);
```

[Code](codes/code1.dart)
